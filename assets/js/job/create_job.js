function initialize_map() {

      var lat_cur = parseFloat('13.736279');
      var lng_cur = parseFloat('100.6384563');

      console.log('lat lon');
      
      console.log(lat_cur);
      console.log(lng_cur);

      var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:lat_cur,
                        lng:lng_cur  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:lat_cur,
                        lng:lng_cur                 
                },
                map:map,
                draggable:false
        });

        marker.setMap(map);  

        var iw = new google.maps.InfoWindow({
            content: "Customer Location"
          });
        google.maps.event.addListener(marker, "click", function(e) {
            iw.open(map, this);
          });


        // textfield search
        //var searchBox = new google.maps.places.SearchBox(document.getElementById('find_location'));
        // seach change event
        // google.maps.event.addListener(searchBox,'places_changed',function(){

        //         var places = searchBox.getPlaces();
        //         console.log(places);

        //         var bounds = new google.maps.LatLngBounds();
        //         var jsonbounds = JSON.stringify(bounds);
        //         var i,place;
                
                
        //         for(i=0;place=places[i];i++){   
        //                 try       
        //                 {
        //                         bounds.extend(place.geometry.location);
        //                         marker.setPosition(place.geometry.location);
        //                         console.log(places);
        //                         var latitude = places[i].geometry.location.lat();
        //                         var longitude  =  places[i].geometry.location.lng();
        //                         console.log("LAT::"+latitude+"LNG"+longitude);
        //                         jQuery("#lat").val(latitude);
        //                         jQuery("#lng").val(longitude);
        //                 }
        //                 catch(e)    
        //                 {
        //                         console.log(e);    
        //                 }
        //         map.setZoom(1);       
        //         }
                
                
        //         map.fitBounds(bounds);


        //         initialize_map();
        // });
        
        google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng;
        jQuery("#lat").val(latLng.lat());
        jQuery("#lng").val(latLng.lng());
        
        });
        
       google.maps.event.addDomListener(window, 'load', initialize_map);

}


var base_url = jQuery('input[name="base_url"]').val();
var $input_customer = jQuery("input[name='customer']");
$input_customer.typeahead({
  source:  function (query, process) {
        return jQuery.get(base_url+'job/ajaxGetMyOwnCustomer', { query: query}, function (data) {
              data = jQuery.parseJSON(data);
              console.log('data');
              console.log(data);
              return process(data);
          });


  },
  updater: function(item){
    // console.log(item);
    jQuery('input[name="customer_id"]').val(item.id);
    setMapMarker(item);

    jQuery('input[name="customer_lat"]').val(item.lat);
    jQuery('input[name="customer_lon"').val(item.lng);
    return jQuery.trim(item.name);
  },
  autoSelect: false

});

function setMapMarker(data){
    var latitude = parseFloat(data.lat);
    var longitude = parseFloat(data.lng);

    var lat_long = {
        'latitude':latitude,
        'longitude':longitude
    };

        var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:latitude,
                        lng:longitude  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:latitude,
                        lng:longitude                 
                },
                map:map,
                draggable:false
        });

        marker.setMap(map);

        var iw = new google.maps.InfoWindow({
            content: '<b>[ลูกค้า]</b>'+data.name
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });

    jQuery('select[name="technicial_skill"]').removeAttr('readonly');  






}

function changeTechnicialSkill(tech_skill_id){
    console.log(tech_skill_id);
    var tech_skill_id = parseInt(tech_skill_id);
    var customer_id = parseInt(jQuery('input[name="customer_id"]').val());

    if(!customer_id){
        jQuery('input[name="customer"]').focus();
        return false;
    }

    /* get technicial by skill and map for customer */
    var post_data = {
        'customer_id':customer_id,
        'tech_skill_id':tech_skill_id
    };
    techTechnicialForCustomer(post_data);

    /* eof get technical by skill and map for customer*/
}

function techTechnicialForCustomer(post_data){
    var base_url = jQuery('input[name="base_url"]').val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxGetTechnicialForCustomer",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  jQuery('table.technicial_row > tbody').html('').html('<tr><td colspan="3" class="text-center"><i class="fa fa-spin fa-spinner"></i></td></tr>');
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('table.technicial_row > tbody').html('');

                                var closest_tech_lat = "";
                                var closest_tech_lng = "";
                                jQuery(response.technicial_row).each(function(index,value){
                                    var tbody_text = "";
                                    var checked = (index === 0)?'checked="checked"':'';
                                    closest_tech_lat = (index === 0)?value.tech_lat:closest_tech_lat;
                                    closest_tech_lng = (index === 0)?value.tech_lon:closest_tech_lng;
                                    tbody_text += '<tr>';
                                        tbody_text += '<td>'+value.technicial_firstname_lastname+'</td>';
                                        tbody_text += '<td>'+value.distance+'</td>';
                                        tbody_text += '<td><input type="radio" name="select_technicial" value="'+value.tech_id+'" '+checked+' data-lat="'+value.tech_lat+'" data-lng="'+value.tech_lon+'" onclick="clickSelectTechnicial(this)"></td>';
                                    tbody_text += '</tr>';
                                    jQuery('table.technicial_row > tbody').append(tbody_text);
                                });

                                /* render marker two point */
                                var customer_lat_lng = {
                                    'lat':parseFloat(jQuery('input[name="customer_lat"]').val()),
                                    'lng':parseFloat(jQuery('input[name="customer_lon"]').val())
                                };
                                var technicial_lat_lng = {  
                                    'lat':parseFloat(closest_tech_lat),
                                    'lng':parseFloat(closest_tech_lng)
                                };


                                renderMapMarkerTwoPoint(customer_lat_lng,technicial_lat_lng);
                                /* eof render marker two point*/



                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });


}

function renderMapMarkerTwoPoint(point1,point2){

        var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:point1.lat,
                        lng:point1.lng  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:point1.lat,
                        lng:point1.lng                 
                },
                map:map,
                draggable:false
        });

        var marker2 = new google.maps.Marker({   
                position:{
                        lat:point2.lat,
                        lng:point2.lng                 
                },
                map:map,
                draggable:false
        });


        marker.setMap(map);
        marker2.setMap(map);

        var iw = new google.maps.InfoWindow({
            content: 'ลูกค้า'
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });

        var iw2 = new google.maps.InfoWindow({
            content: 'ช่าง'
        });
        google.maps.event.addListener(marker2, "mouseover", function(e) {
            iw2.open(map, this);
        });

                        var flightPlanCoordinates = [
                              {lat: point1.lat, lng: point1.lng},
                              {lat: point2.lat, lng: point2.lng}
                        ];
                        var flightPath = new google.maps.Polyline({
                          path: flightPlanCoordinates,
                          geodesic: true,
                          strokeColor: '#FF0000',
                          strokeOpacity: 1.0,
                          strokeWeight: 2
                        });

                        flightPath.setMap(map);


}


function clickSelectTechnicial(element){
    var element = jQuery(element);

    var customer_lat_lng = {
        'lat':parseFloat(jQuery('input[name="customer_lat"]').val()),
        'lng':parseFloat(jQuery('input[name="customer_lon"]').val())
    };

    var technicial_lat_lng = {  
        'lat':parseFloat(element.data('lat')),
        'lng':parseFloat(element.data('lng'))
    };

    renderMapMarkerTwoPoint(customer_lat_lng,technicial_lat_lng);


}

jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="create-job-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                customer: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                product_id:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                product_serial_number:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });

});


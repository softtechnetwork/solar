baguetteBox.run('.tz-gallery');



function initialize_map(){
		var customer_lat_lon = {
			lat:parseFloat(jQuery('input[name="customer_latitude"]').val()),
			lon:parseFloat(jQuery('input[name="customer_longitude"]').val())
		};

		console.log('customer_lat_lon');
		console.log(customer_lat_lon);
		    var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var mapcus = new google.maps.Map(document.getElementById('map-assign-customer'),{
                center:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon         
                },
                map:map,
                draggable:false
        });

        var markercus = new google.maps.Marker({   
                position:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon         
                },
                map:map,
                draggable:false
        });

        marker.setMap(map);  
        markercus.setMap(mapcus);

        var iw = new google.maps.InfoWindow({
            content: "<strong>ที่อยู่ลูกค้า</strong> <br><small>"+jQuery('input[name="customer_address"]').val()+"</small>"
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });

        google.maps.event.addListener(markercus, "mouseover", function(e) {
            iw.open(mapcus, this);
        });

}

function changeApproveStatus(element){
    var element = jQuery(element);

    var post_data = {
        'approve_status':element.val(),
        'technicial_job_id':element.data('jobid')
    };
    //console.log(post_data);

                    var base_url = jQuery('input[name="base_url"]').val();
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxChangeApproveStatus",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: jQuery('input[name="change_approve_success"]').val(),
                                  text: jQuery('input[name="change_approve_success_txt"]').val(),
                                  icon: "success",
                                });
                            }else{
                                swal({
                                  title: jQuery('input[name="change_approve_error"]').val(),
                                  text: jQuery('input[name="change_approve_error_txt"]').val(),
                                  icon: "error",
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}

jQuery(document).ready(function(){

  renderTechnicialImage();
  assignTechnicialForJob();
  validateCancelAssignTechnicialForm();

  
});
function assignTechnicialForJob(){
  jQuery('form[name="form-assign-technicial"]').submit(function(e){
    e.preventDefault();
    //console.log('aaaa');
    var form_data = jQuery(this).serialize();
    //  console.log(form_data);

                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxUpdateAssignTechnicial",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               swal({
                                  title: 'ทำการอัพเดทข้อมูลสำเร็จ!!!',
                                  text: 'คุณได้ทำการกำหนดช่างเรียบร้อยแล้ว',
                                  icon: "success",
                                }).then(function(){
                                  jQuery('#AssignTechnicialModal').modal('hide');
                                  location.reload();
                                });

                               
                            }else{
                                swal({
                                  title: 'เกิดข้อผิดพลาด!!!',
                                  text: 'กรุณาลองใหม่อีกครั้ง',
                                  icon: "error",
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });



  });

}
function renderTechnicialImage(){
  var agent_code = jQuery('input[name="hide_agent_code"]').val();
  //console.log(agent_code)

                    var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"technicial/ajaxGetTechnicialImageProfile",
                          async:true,
                          dataType:'json',
                          data:{'agent_code':agent_code},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               //element.parent().parent().fadeOut('slow');
                               jQuery('div#tech_image_profile').find('i.fa').remove();
                              if(response.data.Image){
                                jQuery("#agent_image").attr('src','data:image/png;base64,'+response.data.Image);
                              }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}

function validateCancelAssignTechnicialForm(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="form-cancel-assign-technicial"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cancel_reason: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
              e.preventDefault();
              var post_data = {
                'cancel_reason':jQuery('form[name="form-cancel-assign-technicial"]').find('input[name="cancel_reason"]').val(),
                'tech_job_id':jQuery('input[name="tech_job_id"]').val()

              };

                    var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxCancelAssignTechnicial",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            if(response.status){
                                swal({
                                  title: 'ทำการอัพเดทข้อมูลสำเร็จ!!!',
                                  text: 'คุณได้ทำการยกเลิกกำหนดช่างเรียบร้อยแล้ว',
                                  icon: "success",
                                }).then(function(){
                                    jQuery('#CancelAssignTechnicial').modal('hide');
                                    location.reload();
                                });

                               
                            }else{

                                swal({
                                  title: 'เกิดข้อผิดพลาด!!!',
                                  text: 'กรุณาลองใหม่อีกครั้ง',
                                  icon: "error",
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

            },
            onError:function(e){

            }
    });
}


function changePaymentStatus(element){
    //console.log(element);
    var element = jQuery(element);
    // console.log(element);
    var payment_status = element.val();
    // console.log(payment_status);

    var post_data = {
      'payment_status':payment_status,
      'technicial_job_id':element.data('jobid')
    };

    //console.log(post_data);

                  var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxSetJobPayment",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: 'สำเร็จ!!',
                                  text: 'คุณได้ทำการเปลี่ยนสถานะการชำระเงินแล้ว',
                                  icon: "success",
                                }).then(function(){
                                  location.reload();
                                });
                              
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

}
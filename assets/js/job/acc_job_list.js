

function changePaymentStatus(element){
  var element = jQuery(element);
  var technicial_job_id = element.data('jobid');
  var payment_status = element.val();

  //console.log(element.parent().parent()); return false;

  if(payment_status === 'paid'){
      //console.log('aaaa'); return false;

      jQuery('#payDateModal').modal('show');
      jQuery('input[name="hide_paydate_payment_type"]').val(payment_status);
      jQuery('input[name="hide_paydate_job_id"]').val(technicial_job_id);

      return false;

  }

  var post_data = {
    'technicial_job_id':technicial_job_id,
    'payment_status':payment_status
  };


  

                  var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxSetJobPayment",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: 'สำเร็จ!!',
                                  text: 'คุณได้ทำการเปลี่ยนสถานะการชำระเงินแล้ว',
                                  icon: "success",
                                }).then(function(){
                                  //location.reload();
                                  element.parent().parent().remove();
                                });
                              
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}

function checkAll(element){
  var element = jQuery(element);
    if(element.is(':checked')){
      
      jQuery('input[name="check_job[]"]').each(function(index,value){
        this.checked = true;
      });
    }else{
      //console.log('unchecked')
      jQuery('input[name="check_job[]"]').each(function(index,value){
        this.checked = false;
      });
    }
}


function checkListAction(form_element){
  var element = jQuery(form_element);
  var form = element.closest('form');




  /* check checked job */
  if(jQuery('input[name="check_job[]"]:checked').length <= 0){
            swal({
              title: 'แจ้งเตือน!!',
              text: 'กรุณาเลือกงานอย่างน้อย 1 รายการเพื่อกระทำ',
              icon: "warning",
            }).then(function(){
              //location.reload();
              //element.parent().parent().remove();
            });
            return false;
      
  }else if(jQuery('select[name="select_action"]').val() === ''){
            swal({
              title: 'แจ้งเตือน!!',
              text: 'กรุณาเลือกรูปแบบกระบวนการกระทำ',
              icon: "warning",
            }).then(function(){
              //location.reload();
              //element.parent().parent().remove();
            });
            return false;
  }else if(jQuery('select[name="select_action"]').val() === 'export_excel'){
      checkListActionExportExcel(form_element);
  }else{

      /* update all record checked into database */
      var all_checked_record = [];

      jQuery('input[name="check_job[]"]:checked').each(function(index,value){
        //console.log(value);
        all_checked_record.push({
          'payment_status':jQuery('select[name="select_action"]').val(),
          'technicial_job_id':this.value
        });
      });


      if(jQuery('select[name="select_action"]').val() === 'paid'){
        //console.log(JSON.stringify(all_checked_record));
        jQuery('input[name="hide_paydate_multirecord"]').val(JSON.stringify(all_checked_record));
        jQuery('#payDateMultiRecordModal').modal('show');
        return false;
      }

                    var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxSetMultiJobPayment",
                          async:true,
                          dataType:'json',
                          data:{'data':all_checked_record},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: 'สำเร็จ!!',
                                  text: 'คุณได้ทำการเปลี่ยนสถานะการชำระเงินแล้ว',
                                  icon: "success",
                                }).then(function(){
                                  //location.reload();
                                  jQuery('input[name="check_job[]"]:checked').each(function(index,value){
                                      jQuery(this).closest('tr').remove();
                                  });

                                  if(jQuery('input[name="check_all"]').is(':checked')){
                                      jQuery('input[name="check_all"]').removeAttr('checked');
                                  }
                                });
                                
                              
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });



      /* eof update all record checked into database */



  }
  /* */
  
}

function checkListActionExportExcel(element){

  var element = jQuery(element);
  var form = element.closest('form');

  /* check checked job */
  if(jQuery('input[name="check_job[]"]:checked').length <= 0){
            swal({
              title: 'แจ้งเตือน!!',
              text: 'กรุณาเลือกงานอย่างน้อย 1 รายการเพื่อกระทำ',
              icon: "warning",
            }).then(function(){
              //location.reload();
              //element.parent().parent().remove();
            });
            return false;
      
  }else if(jQuery('select[name="select_action"]').val() === ''){
            swal({
              title: 'แจ้งเตือน!!',
              text: 'กรุณาเลือกรูปแบบกระบวนการกระทำ',
              icon: "warning",
            }).then(function(){
              //location.reload();
              //element.parent().parent().remove();
            });
            return false;
  }else{
    
    /* update all record checked into database */
      var all_checked_record = [];

      jQuery('input[name="check_job[]"]:checked').each(function(index,value){
        //console.log(value);
        all_checked_record.push({
          'payment_status':jQuery('select[name="select_action"]').val(),
          'technicial_job_id':this.value
        });
      });

      //console.log(all_checked_record);
      var form_txt = '<form action="" method="post">';

          form_txt += '<input type="hidden" name="action_type" value="export_to_excel" />';
      jQuery(all_checked_record).each(function(index,value){
          form_txt += '<input type="hidden" name="checked_job[]" value="'+value.technicial_job_id+'" />';
      });
            
      form_txt += '</form>';

      //console.log(form_txt); return false;

      jQuery(form_txt).appendTo('body').submit().remove();

      // var newForm = jQuery('<form>', {
      //   'action': '',
      //   'target': '_top'
      // }).append(jQuery('<input>', {
      //     'name': 'all_checked_record',
      //     'value': JSON.stringify(all_checked_record),
      //     'type': 'hidden'
      // }));
      // newForm.submit();

  }

}


function changePaymentStatusWithPayDate(element){
  var element = jQuery(element);
  var form_data = element.closest('form').serialize();
  //console.log(form_data);
                  var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxSetJobPaymentWithPayDate",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('#payDateModal').modal('hide');
                                swal({
                                  title: 'สำเร็จ!!',
                                  text: 'คุณได้ทำการเปลี่ยนสถานะการชำระเงินแล้ว',
                                  icon: "success",
                                }).then(function(){
                                  location.reload();
                                  //element.parent().parent().remove();
                                });
                              
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

}


function changePaymentStatusMultirecordWithPayDate(element){
  var element = jQuery(element);
  var form_data = element.closest('form').serialize();
  // console.log(form_data);
  // return false;

                  var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxSetJobPaymentMultirecordWithPayDate",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('#payDateModal').modal('hide');
                                swal({
                                  title: 'สำเร็จ!!',
                                  text: 'คุณได้ทำการเปลี่ยนสถานะการชำระเงินแล้ว',
                                  icon: "success",
                                }).then(function(){
                                  jQuery('#payDateMultiRecordModal').modal('hide');
                                  location.reload();
                                  //element.parent().parent().remove();
                                });
                              
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}
jQuery(document).ready(function(){

  jQuery('input[name="pay_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
      format: 'DD-MM-YYYY'
    },
  }, function(start, end, label) {
    
  });

  jQuery('input[name="pay_date_multirecord"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
      format: 'DD-MM-YYYY'
    },
  }, function(start, end, label) {
    
  });
  

});

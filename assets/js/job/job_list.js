jQuery("#input_excel").fileinput({ 
    language: "th",
    uploadUrl: '',
    uploadAsync: false,
    maxFileSize: 10000,
    allowedFileExtensions: ["xls", "xlsx"],
    uploadExtraData: {
        gallary_id: ""        
    },
    dropZoneEnabled:false,    
    showUpload: false,
    showClose: false,
    showCaption: true,
    showBrowse: true,
    showUpload:false,
    showUploadedThumbs: true,
    showPreview: true
}).on('filebatchuploaderror', function(event, data, msg) {
	console.log('filebatch uploade rror event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchpreupload',function(event,data,msg){
    console.log('file batchpreupload event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchuploadcomplete',function(event,data,msg){
    console.log('filebatch upload complete = '+event+' data = '+data+' msg = '+msg);
});


jQuery('#input_excel').on('fileselect', function(event, numFiles, label) {
    jQuery('button.fileinput-upload-button').attr('disabled','disabled');
});

function approveAllJobDone(element){

    if(confirm('ต้องการอนุมัติงานทั้งหมดใช่หรือไม่?') == true){

    var element = jQuery(element);
    var btn_txt = element.html();
    btn_txt += ' <i class="fa fa-spin fa-spinner"></i>';
    element.html('').html(btn_txt);
    element.attr('disabled','disabled');
    //return false;
    //console.log(btn_txt);return false;
    //console.log(element); return false;
    //console.log('abcd');
                    var base_url = jQuery('input[name="base_url"]').val();
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxApproveAllJobDone",
                          async:true,
                          dataType:'json',
                          data:{},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                element.removeAttr('disabled');
                                element.find('i.fa').remove();
                                
                                swal({
                                  title: jQuery('input[name="change_approve_success"]').val(),
                                  text: jQuery('input[name="change_approve_all_success"]').val(),
                                  icon: "success",
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
      }

}


function openModalCancelJob(element){
    var element = jQuery(element);
    var jobid = element.data('jobid');
    jQuery('input[name="hide_job_id"]').val(jobid);
}

function cancelJob(element){
  var element = jQuery(element);
  var jobid = element.data('jobid');
  
                  var base_url = jQuery('input[name="base_url"]').val();
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxCancelJob",
                          async:true,
                          dataType:'json',
                          data:{'jobid':jobid},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: 'ยกเลิกงานเรียบร้อย!!',
                                  text: 'งานดังกล่าวได้ถูกยกเลิกแล้ว',
                                  icon: "success",
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}


jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="cancel-job-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cancel_reason: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
                e.preventDefault();
                var form_data = jQuery('form[name="cancel-job-form"]').serialize();
                  var base_url = jQuery('input[name="base_url"]').val();
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxCancelJob",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: 'ยกเลิกงานเรียบร้อย!!',
                                  text: 'งานดังกล่าวได้ถูกยกเลิกแล้ว',
                                  icon: "success",
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
            }
    });


    /* check query string and open modal for create job by excel file */
    var url_parameter = urlParameter();
    //console.log(url_parameter);
    if(url_parameter !== null && url_parameter.hasOwnProperty('modaltoggle')){
      //console.log(url_parameter);
      jQuery('#importExcelFile').modal('show');
    }

    /* eof check query*/



    /* check has error before import by excel file */
    jQuery('form[name="import-excel-form"]').submit(function(e){
      /* check div file input */
      //e.preventDefault();
      var div_file_input = jQuery('div.file-input');
      if(document.getElementById("input_excel").files.length == 0){
        document.getElementById("input_excel").focus();
        e.preventDefault();
      }

      if(div_file_input.length >= 1){
        if(div_file_input.hasClass('has-error')){
          e.preventDefault();
        }
      }
      //console.log(div_file_input);
    });

});

function urlParameter() {
    var url = window.location.href,
    retObject = {},
    parameters;

    if (url.indexOf('?') === -1) {
        return null;
    }

    url = url.split('?')[1];

    parameters = url.split('&');

    for (var i = 0; i < parameters.length; i++) {
        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
    }

    return retObject;
}
var dataTable_;
var base_url = $('input[name="base_url"]').val();
var radio_periodselected;
var old_euheight = 0;
$(document).ready(function () {

	// drawtable(); // event : draw table

	set_overflowclass();

	setting_duration(); // div : report set duration
	setting_duration_modal(); // div : report set duration
	onaction_trigger_click(); // event : first load trigger click
	// ajax draw table ..
	setting_period_show();


	old_euheight = $(".panel_energyused").height();

	



});

function setting_period_show(period_text = null) {

	var period_value = "";

	if (period_text == null) {
		//var period_value = $('input[type=radio][name=radio_period]').val();
		var period_value = $("input[type='radio']:checked").val();
	}

	if (period_value == "month" || period_text == "month") {
		$("#duration_year").hide();
		$("#duration_month").show();
		$("#angle_down_month").show();
		$("#angle_down_year").hide();

		$("#duration_year_modal").hide();
		$("#duration_month_modal").show();

		$("#angle_down_month_modal").show();
		$("#angle_down_year_modal").hide();
		radio_periodselected = period_value;
		$("#text_modal_period").text("เลือกเดือนที่ต้องนำข้อมูลออก");
	} else {
		$("#duration_month").hide();
		$("#duration_year").show();
		$("#angle_down_month").hide();
		$("#angle_down_year").show();

		$("#duration_month_modal").hide();
		$("#duration_year_modal").show();
		$("#angle_down_month_modal").hide();
		$("#angle_down_year_modal").show();
		radio_periodselected = period_value;
		$("#text_modal_period").text("เลือกปีที่ต้องนำข้อมูลออก");

	}
}

function onaction_trigger_click() {
	$('.size_tag_class:first-child').click();
}

function setting_duration() {
	var currentTime = new Date();


	// returns the year (four digits)
	var current_year = currentTime.getFullYear()
	$('input[name="duration_month"]').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'mm/yy',
		showButtonPanel: true,
		yearRange: "2020:" + current_year,

		daysOfWeek: [
			"Su",
			"Mo",
			"Tu",
			"We",
			"Th",
			"Fr",
			"Sa"
		],
		monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
			'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
		],
		monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
			'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
		],

		onClose: function () {
			var oldvalue = $("#duration_month").val();
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));

			var newvalue =$("#duration_month").val();
			if(oldvalue != newvalue){
				main_controller_event();
			}
		},

		beforeShow: function () {
			if ((selDate = $(this).val()).length > 0) {


				var selDate_split = $(this).val().split('/');
				var iMonth = selDate_split[0];
				iMonth = iMonth > 0 ? iMonth - 1 : 0;
				var iYear = selDate_split[1];
				$(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
				$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
			}
		}
	});



	$("#duration_year").datepicker({

		format: "yyyy",
		viewMode: "years",
		minViewMode: "years"
	});

}

function setting_duration_modal() {
	var currentTime = new Date();


	// returns the year (four digits)
	var current_year = currentTime.getFullYear()
	$('input[name="duration_month_modal"]').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'mm/yy',
		showButtonPanel: true,
		yearRange: "2020:" + current_year,

		daysOfWeek: [
			"Su",
			"Mo",
			"Tu",
			"We",
			"Th",
			"Fr",
			"Sa"
		],
		monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
			'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
		],
		monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
			'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
		],

		onClose: function () {
			var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(iYear, iMonth, 1));

			// main_controller_event();
		},

		beforeShow: function () {
			if ((selDate = $(this).val()).length > 0) {


				var selDate_split = $(this).val().split('/');
				var iMonth = selDate_split[0];
				iMonth = iMonth > 0 ? iMonth - 1 : 0;
				var iYear = selDate_split[1];
				$(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
				$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
			}
		}
	});



	$("#duration_year_modal").datepicker({

		format: "yyyy",
		viewMode: "years",
		minViewMode: "years"
	});

}

$("#search_input").keyup(function () {

	// Retrieve the input field text and reset the count to zero
	var filter = $(this).val(),
		count = 0;

	// Loop through the comment list
	$('#site_div div').each(function () {
		// draw barchart
		if (dtbarchart_plan != 0 && dtbarchart_plan != null) {
			//alert('5');
			google.charts.load('current', {
				'packages': ['bar']
			});
			google.charts.setOnLoadCallback(drawChart);

		}


		// If the list item does not contain the text phrase fade it out
		if ($(this).text().search(new RegExp(filter, "i")) < 0) {
			$(this).hide(); // MY CHANGE

			// Show the list item if the phrase matches and increase the count by 1
		} else {
			$(this).show(); // MY CHANGE
			count++;
		}

	});

});

$("#search_input_modal").keyup(function () {

	// Retrieve the input field text and reset the count to zero
	var filter = $(this).val(),
		count = 0;

	// Loop through the comment list
	$('#site_modaldiv div').each(function () {



		// If the list item does not contain the text phrase fade it out
		if ($(this).text().search(new RegExp(filter, "i")) < 0) {
			$(this).hide(); // MY CHANGE

			// Show the list item if the phrase matches and increase the count by 1
		} else {
			$(this).show(); // MY CHANGE
			count++;
		}

	});

});

/* set default : overflow on site div*/
function set_overflowclass() {
	$panel_description_height = $(".panel_description").height();

	$panel_description_height = $("#description_div").height() + 100;

	var site_div_height = $("#site_div").height();
	

	// $("#site_div").addClass("over-flow-container");
	// var vh_thisscreen = vhToPixels(72);
	// $("#site_div").height(vh_thisscreen);

	// ถ้า site div สูงกว่าให้ใส่ class overflow
	if (site_div_height > $panel_description_height) {

		// $("#site_div").addClass("over-flow-container");
		//  $("#site_div").height($panel_description_height);
		


		$("#site_modaldiv").addClass("over-flow-container");
		$("#site_modaldiv").height($panel_description_height - 100);
	} else {
		$("#site_modaldiv").height($panel_description_height - 100);
	}


	// $("#tablestructure").height(table_height)




}

function set_active_class(stationId) {
	$(".size_tag_class ").removeClass("site_div_class_active");
	$(".text_tag").removeClass("text_tag_onactive");
	$(".text_tag_wat").removeClass("text_tag_onactive");

	$(".ar_").removeClass("arrow-righ");
	$("#tag_" + stationId).addClass("site_div_class_active");
	$("#ar_" + stationId).addClass("arrow-right");

	$("#textsubjecttag_" + stationId).addClass("text_tag_onactive");
	$("#textdigittag_" + stationId).addClass("text_tag_onactive");
	$("#textdigittag_" + stationId + " > .text_font_subject ").addClass("text_tag_onactive");


	// event listener
	// $('input:radio[name="radio_period"]').filter('[value="month"]').attr('checked', true);

	$('input[name="radio_period"]:radio:first').click();


	main_controller(stationId);

}



function initialize_map(stationId) {
	// set defult by first object value
	//console.log(planlist_data)



	var marker;
	setTimeout(function () {
		if (stationId != null) {
			$.each(planlist_data, function (index, value) {
				if (value.stationId == stationId && stationId != null) {
					var locationLat = parseFloat(value.locationLat);
					var locationLng = parseFloat(value.locationLng);

					var map = new google.maps.Map(document.getElementById('map-canvas'), {
						center: {
							lat: locationLat,
							lng: locationLng
						},
						zoom: 16,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});




					// google map marker
					marker = new google.maps.Marker({
						position: {
							lat: locationLat,
							lng: locationLng
						},
						map: map,
						draggable: false
					});

					// set google map info
					var iw = new google.maps.InfoWindow({
						content: "<strong>สถานที่ตั้ง</strong> <br><small>" + value.name_solarapp + "</small>"
					});
					google.maps.event.addListener(marker, "mouseover", function (e) {
						iw.open(map, this);
					});
				}
			});
		}
	}, 2000);

}






function showdatepicker() {
	$("#duration_month").datepicker('show');
}

function showmodaldatepicker() {
	$("#duration_month_modal").datepicker('show');
}

function vhToPixels (vh) {
	return Math.round(window.innerHeight / (100 / vh));
  }

function drawtable(timeType) {


	var eu_height = old_euheight;
	
	var cd_height = $("#chart_div").height();
	
	if (timeType != "month") {
		
		var screen = eu_height > 500 ? 395 : 115;
	} else {
		
		var screen = eu_height > 500 ? 365 : 115;
		
	}
	var vh_thisscreen = vhToPixels(100);
	
	
	// var scrollY = (eu_height - cd_height) - screen;
	var scrollY = (eu_height ) - screen;
	var columns = [{
			"width": "25%"
		},
		{
			"width": "25%",
			"class": "text-center"
		},
		{
			"width": "50%",
			"class": "text-center"
		}
	]

	var dataTable_ = $('#tablestructure').DataTable({
		"processing": false,
		"bDestroy": true,
		"bPaginate": false,
		"bLengthChange": false,
		// "pageLength" : 5,
		"bFilter": false,
		"bInfo": false,
		"searching": false,
		"ordering": false,
		"pageResize": true,
		"scrollY": scrollY + 'px',
		"scrollCollapse": true,


		initComplete: function () {

		},
		createdRow: function (row, data, dataIndex) {
			// Set the data-status attribute, and add a class
			$(row).addClass("text_font_tabletd");
		},
		"columns": columns
	});

	return dataTable_;
}




function main_controller(stationId) {
	$("#active_stationId").val(stationId);
	onaction_setting_paneldescription(stationId); // set panel description value
	onaction_setting_panelenergyused(stationId); // set panel energy used  value


	main_controller_event($('input[type=radio][name=radio_period]').val());

	initialize_map(stationId)
}


function onaction_setting_paneldescription(stationId) {

	//console.log(customer_rel_plan);

	var key = Object.keys(customer_rel_plan).find(key => customer_rel_plan[key].stationId == stationId); // find object key by value

	var allPowerGeneration = customer_rel_plan[key].allPowerGeneration;

	if (allPowerGeneration === 'set default' || allPowerGeneration.length == 0 || isNaN(allPowerGeneration)) {
		allPowerGeneration = 0;
	}
	if (allPowerGeneration > 999) {
		allPowerGeneration = commawithtwodecimal(allPowerGeneration);
	}
	var html = "<span id='' class=' text_font_subject'>วัตต์</span>";
	$("#text_solar").text(allPowerGeneration + " ");
	$("#text_solar").append(html);


	var model = customer_rel_plan[key].model + " " + customer_rel_plan[key].model_pw;
	if (model == " ") {
		model = "-";
	}
	$("#text_hybrid").text(model);

	var electric_meter_type = customer_rel_plan[key].electric_meter_type;
	$("#text_typebusiness").text(electric_meter_type);

	var intallation_date = customer_rel_plan[key].intallation_date;
	var date = new Date(intallation_date * 1000);
	var datecreate = date.getUTCDate() + '/' + (date.getUTCMonth() + 1) + '/' + date.getUTCFullYear();
	$("#text_datecreate").text(datecreate);
}


function onaction_setting_panelenergyused(stationId) {

	event_setdata_datatable(null);

}



$('input[type=radio][name=radio_period]').change(function () {

	main_controller_event(this.value);

});

function onchange_draw_data_year() {

	main_controller_event("year");

}

function main_controller_event(period = null) {
	var stationId = $("#active_stationId").val();
	if (period == null) {
		period = $('input[type=radio][name=radio_period]').val();
	}
	if (period == 'month') {
		var timeType = 2;
		var duration_month = $("#duration_month").val();
		setting_period_show("month"); // event : dropdown visible disable
		ajax_curlgetsolarreport(timeType, duration_month, stationId, "month");

	} else if (period == 'year') {

		var timeType = 3;
		var duration_year = $("#duration_year").val();
		setting_period_show("year"); // event : dropdown visible disable
		ajax_curlgetsolarreport(timeType, duration_year, stationId, "year");
	}
}

function ajax_curlgetsolarreport(timeType, duration, stationId, period) {
	$(".loader").show();
	$.ajax({
		type: "POST",
		url: base_url + "Main/ajax_curlgetsolarreport",
		data: {
			timeType: timeType,
			duration: duration,
			stationId: stationId,
			period: period
		},
		success: function (data, status, xhr) {
			var data = JSON.parse(data);
			// if(data.status != false){

			event_setdata_drawchart(data, period, timeType); // event : draw line chart

			//}
		}
	});
}

function event_setdata_drawchart(data, period, timeType) {



	google.charts.load('current', {
		'packages': ['bar']
	});

	google.charts.setOnLoadCallback(function () {
		draw_candlestick_chart(data, period, timeType);

		event_setdata_datatable(data, period); // event : draw datatable
	});


}


function draw_candlestick_chart(chartdata = null, period = null, timeType = null) {



	var data = new google.visualization.DataTable();
	var data_push = new Array();


	if (chartdata != null) {
		if (typeof chartdata.data !== 'undefined') {
			// your code here

			var chartdata = JSON.parse(chartdata.data);

			if (chartdata.stationDataItems.length > 0) {
				$.each(chartdata.stationDataItems, function (index, value) {

					if (index == 0) {
						// case : ถ้าดูวันเดียวกัน column แสดงคอลัมน์ตามเงื่อนไข


						if (timeType == 1) {
							data.addColumn('datetime', 'วัน / เวลา');

							data.addColumn('number', 'พลังงานที่ผลิตได้ (w)');
						} else if (timeType == 3) {
							data.addColumn('string', '');
							data.addColumn('number', 'พลังงานที่ผลิตได้ (kWh)');
							data.addColumn({
								type: 'string',
								role: 'tooltip'
							});
						} else {
							data.addColumn('date', '');
							data.addColumn('number', 'พลังงานที่ผลิตได้ (kWh)');
						}
					}

					value.m = value.m > 0 ? value.m - 1 : 0;

					if (timeType == 1) {

						var generationPower = value.generationPower > 0 ? value.generationPower : 0;

						data.addRows([
							[new Date(value.y, value.m, value.d, value.h, value.i, value.s), generationPower]
						]);
					} else if (timeType == 3) {

						var generationPower = value.generationValue > 0 ? value.generationValue : 0;

						value.month = value.month > 0 ? value.month - 1 : 0;

						data.addRows([
							[GetMonthName(value.month + 1), generationPower, GetMonthName(value.month)]
						]);


					} else {
						value.month = value.month > 0 ? value.month - 1 : 0;
						var generationPower = value.generationValue > 0 ? value.generationValue : 0;

						data.addRows([
							[new Date(value.year, value.month, value.day), value.generationValue]
						]);

					}

				});
			}
		} else {
			if (timeType == 1) {
				data.addColumn('datetime', 'วัน / เวลา');
				data.addColumn('number', 'พลังงานที่ผลิตได้ (w)');
			} else if (timeType == 3) {
				// data.addColumn('string', 'เดือน');
				data.addColumn('date', '');
				data.addColumn('number', 'พลังงานที่ผลิตได้ (kw)');
				data.addColumn({
					type: 'string',
					role: 'tooltip'
				});
			} else {
				data.addColumn('date', 'วัน / เวลา');
				data.addColumn('number', 'พลังงานที่ผลิตได้ (kw)');
			}
		}

		// Create DateFormat with a timezone offset of -4
		if (timeType == 1) {


			var dateFormat = new google.visualization.DateFormat({
				formatType: 'long',
				timeZone: +7
			});
			dateFormat.format(data, 0);
		} else if (timeType == 2) {
			var dateFormat = new google.visualization.DateFormat({
				formatType: 'long',
				timeZone: +7
			});
			dateFormat.format(data, 0);
		} else if (timeType == 3) {


			var dateFormat = new google.visualization.DateFormat({
				pattern: "MMM",

				timeZone: +7

			});
			dateFormat.format(data, 0);
		} else {
			var dateFormat = new google.visualization.DateFormat({
				pattern: "yyyy"
			});
			dateFormat.format(data, 0);
		}





	}

	var xTicks = [];
	for (var i = 0; i < data.getNumberOfRows(); i++) {
		if (timeType == 3) {
			//console.log(data.getValue(i, 2));

			xTicks.push({

				v: data.getValue(i, 2),
				f: data.getFormattedValue(i, 2)
			});
		} else {


			xTicks.push({

				v: data.getValue(i, 0),
				f: data.getFormattedValue(i, 0)
			});
		}

	}
	console.log(data);
	var $format = "";
	if (timeType == 1) {
		$format = "HH:mm";
	} else if (timeType == 2) {
		$format = "d";
	} else if (timeType == 3) {
		$format = "MMM";
	} else {
		$format = "YYYY-MM-dd";
	}


	if (timeType > 2) {
		var options = {

			title: '',
			height: 150,
			bar: {
				groupWidth: "40%"
			},
			legend: {
				position: 'none'
			},
			hAxis: {
				slantedText: true,
				showTextEvery: 0,

				ticks: xTicks,
				format: $format,
				fontSize: 3 // or the number you want



			}
		};


		var chart = new google.charts.Bar(document.getElementById('chart_div'));
		chart.draw(data, google.charts.Bar.convertOptions(options));

	} else {
		set_columnchartdate(data, timeType);
	}

	// set modal event
	set_modalevent(data , timeType , xTicks , $format);







}
function set_columnchartdate(data , timeType){
	google.charts.load('current', {
		packages: ['corechart']
	}

).then(function () {
	var xTicks = data.getDistinctValues(0);
	var options = {

		title: '',
		height: 150,
		bar: {
			groupWidth: "40%"
		},
		legend: {
			position: 'none'
		},
		chartArea: {
			// leave room for y-axis labels
			width: '94%'
		  },

		hAxis: {
			slantedText: true,
			showTextEvery: 1,
			slantedTextAngle: 45,
			textStyle: {
				fontSize: 7 // or the number you want
			},
			format: 'd',
			ticks: xTicks,





		}
	};


	var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
	chart.draw(data, options);
});
}
function set_modalevent(data, timeType , xTicks , $format) {
	google.charts.load('current', {
			packages: ['corechart']
		}

	).then(function () {
		$("#modal_googlechart").on('shown.bs.modal', function () {


		

			if (timeType > 2) {
				var options = {

					title: '',
					height: 350,
					bar: {
						groupWidth: "50%"
					},
					legend: {
						position: 'none'
					},
					chartArea: {
						// leave room for y-axis labels
						width: '94%'
					  },
	
	
					hAxis: {
						slantedText: true,
						showTextEvery: 1,
						ticks: xTicks,
						format: $format,
	
	
					}
				};
				var chart = new google.charts.Bar(document.getElementById('chart_div_modal'));

				chart.draw(data, google.charts.Bar.convertOptions(options));

			} else {
				var xTicks = data.getDistinctValues(0);
				var options = {

					title: '',
					height: 350,
					bar: {
						groupWidth: "50%"
					},
					legend: {
						position: 'none'
					},
					chartArea: {
						// leave room for y-axis labels
						width: '94%'
					  },
	
	
					hAxis: {
						slantedText: true,
						showTextEvery: 1,
						slantedTextAngle: 45,
						ticks: xTicks,
						format: $format,
						textStyle: {
							fontSize: 8 // or the number you want
						},
	
					}
				};
				var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_modal'));
				chart.draw(data, options);
			}





		});
	});
}


function event_setdata_datatable(data, timeType) {

	var dataTable_ = drawtable(timeType);

	if (data != null && data.status != false) {
		stationdata = JSON.parse(data.data);
		console.log(stationdata.stationDataItems);
		//console.log(stationdata);
		dataTable_.clear().draw(); // clear : data on datatable
		$.each(stationdata.stationDataItems, function (index, value) {
			var month = timeType == "year" ? thaimonthname[value.month - 1].long : value.day;
			var generationValue = value.generationValue.toFixed(2);
			var eco_price = value.eco_price;
			generationValue = commawithtwodecimal(generationValue);
			eco_price = commawithtwodecimal(eco_price);
			dataTable_.row.add([
				month, "<span style='text-align:center;'>" + generationValue + "</span>", eco_price
			]).node().id = 'td_' + 1;

		});
	} else {
		dataTable_.clear().draw(); // clear : data on datatable
	}
	dataTable_.draw(false);
	$(".loader").hide();

}




function showdiagram() {
	$('.site_div_class_active').css('opacity', '0.2');



	var stationId = $("#active_stationId").val();
	var key = Object.keys(customer_rel_plan).find(key => customer_rel_plan[key].stationId == stationId); // find object key by value
	// var generationPower = customer_rel_plan[key].generationPower;
	// $("#diagramtext_generationpower").text(generationPower);

	// text reverse power
	$("#text_reverse_pw").text(customer_rel_plan[key].gridPower);

	console.log(customer_rel_plan);

	var intallation_date = customer_rel_plan[key].intallation_date;
	var date = new Date(intallation_date * 1000);
	date = convertTZ(date, "Asia/Jakarta");
	var datecreate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
	$("#diagramtext_datecreate").text(datecreate);




	$(".loader").show();
	$.ajax({
		type: "POST",
		url: base_url + "Main/ajax_call_history",
		data: {
			stationId: customer_rel_plan[key].stationId,
			access_token: customer_rel_plan[key].access_token,
			unit_price: customer_rel_plan[key].unit_price
		},
		success: function (data, status, xhr) {
			var response = JSON.parse(data);
			$(".loader").hide();
			if (response.status == true) {
				// last update time
				var lastUpdateTime = response.data.lastUpdateTime;
				var date = new Date(lastUpdateTime * 1000);
				var time = prettyDate2(lastUpdateTime * 1000);
				date = convertTZ(date, "Asia/Jakarta");
				var dateupdate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + " " + time;
				$("#diagramtext_dateupdate").text(dateupdate);

				// today generation value
				var todaygeneration = response.data.today_generationpower > 0 ? response.data.today_generationpower : 0;
				todaygeneration = commawithtwodecimal(todaygeneration);
				$("#today_generationpower").text(todaygeneration);
				// ผลตอบแทนวันนี้
				var today_revenue = response.data.today_revenue > 0 ? response.data.today_revenue : 0;
				today_revenue = commawithtwodecimal(today_revenue);
				$("#today_revenue").text(today_revenue);

				// generationPower
				var generationPower = response.data.generationPower > 0 ? response.data.generationPower : 0;
				generationPower = commawithtwodecimal(generationPower);
				$("#diagramtext_generationpower").text(generationPower);


				$("#generationPower_" + stationId).text(generationPower);
				$("#modalgenerationPower_" + stationId).text(generationPower);


				// month_revenue
				var month_revenue = response.data.month_revenue > 0 ? response.data.month_revenue : 0;
				month_revenue = commawithtwodecimal(month_revenue);
				$("#month_revenue").text(month_revenue);
				// kgco
				var kgco = response.data.kgco > 0 ? response.data.kgco : 0;
				kgco = commawithtwodecimal(kgco);
				$("#kgco").text(kgco);
			}
		}
	});



	$("#modal_main").modal("show");
}


function prettyDate2(time) {
	var date = new Date(parseInt(time));
	return date.toLocaleTimeString(navigator.language, {
		timeZone: "Asia/Jakarta",
		hour: '2-digit',
		minute: '2-digit'
	});
}

function convertTZ(date, tzString) {
	return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-us", {
		timeZone: tzString
	}));
}

$('#modal_main').on('hidden.bs.modal', function () {
	// do something…
	$('.site_div_class_active').css('opacity', '1');
})

function showfullscreen() {

	$('.site_div_class_active').css('opacity', '0.2');
	$("#modal_googlechart").modal("show");
}

function onclosemodal() {
	// do something…


}



$('#modal_googlechart').on('hidden.bs.modal', function () {
	// do something…
	$('.site_div_class_active').css('opacity', '1');
})


function exportdata() {
	$('.site_div_class_active').css('opacity', '0.2');
	$(".checkbox_modal").prop('checked', false);
	$("#modal_exportdata").modal("show");
}

function onaction_exportdata() {
	var arr = [];

	var this_peridvalue;
	if ($("#duration_year_modal").is(":hidden")) {
		this_peridvalue = "month";
	} else {
		this_peridvalue = "year";
	}
	var filter_date = this_peridvalue == "month" ? $("#duration_month_modal").val() : $("#duration_year_modal").val();
	$('input.checkbox_modal:checkbox:checked').each(function () {
		arr.push($(this).val());
	});
	if (arr.length > 0) {
		window.location = base_url + "Main/ajax_exportdata?station_arr=" + JSON.stringify(arr) + "&type=" + this_peridvalue + "&filter_date=" + filter_date;

	} else {
		alert('กรุณาเลือกไซต์');
	}
}


$('#modal_exportdata').on('hidden.bs.modal', function () {
	// do something…
	$('.site_div_class_active').css('opacity', '1');
})


function set_active_checkbox(stationId) {
	if ($('#checkbox_' + stationId).is(':checked')) {
		$('#checkbox_' + stationId).prop('checked', false);
	} else {
		$('#checkbox_' + stationId).prop('checked', true);
	}
}

function dropdownangleyear_show(type){
	if(type == 1){
		
		$("#duration_year").attr('size', 1);
		
	}else{
		$( "#duration_year_modal" ).click();
	}
}

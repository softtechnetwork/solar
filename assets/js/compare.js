var dataTable_;
var base_url = $('input[name="base_url"]').val();
var radio_periodselected;
var old_euheight = 0;
$(document).ready(function () {


	
	setting_duration(); // div : report set duration

	onaction_firstloadofpage();




	


});

function onaction_drawing_chart(data, timeType) {
	// google chart : new version
	// google.charts.load('current', {
	// 	'packages': ['line']
	// });


	// google.charts.setOnLoadCallback(function () {
	// 	draw_line_chart(data, timeType);

	// });
	// google chart : old version
	// google.load("visualization", "1", {
	// 	packages: ["linechart"]
	// });
	google.load('visualization', '1', {
		packages: ['corechart']
	});
	google.setOnLoadCallback(function () {
		draw_line_chart(data, timeType);
	});


}

function draw_line_chart(ajax_data, timeType) {

	var data = new google.visualization.DataTable();




	var comparedata_1_dataitem = get_actual_data(ajax_data, 0);
	var comparedata_2_dataitem = get_actual_data(ajax_data, 1);
	var comparedata_3_dataitem = get_actual_data(ajax_data, 2);
	var comparedata_4_dataitem = get_actual_data(ajax_data, 3);
	var comparedata_5_dataitem = get_actual_data(ajax_data, 4);
	var comparedata_6_dataitem = get_actual_data(ajax_data, 5);

	var dd_1_text = $("#dropdown_planlist_1 option:selected").text();
	var dd_2_text = $("#dropdown_planlist_2 option:selected").text();
	var dd_3_text = $("#dropdown_planlist_3 option:selected").text();
	var dd_4_text = $("#dropdown_planlist_4 option:selected").text();
	var dd_5_text = $("#dropdown_planlist_5 option:selected").text();
	var dd_6_text = $("#dropdown_planlist_6 option:selected").text();

	set_chartlabel(dd_1_text, dd_2_text, dd_3_text, dd_4_text, dd_5_text, dd_6_text); // set chart label
	if (timeType == 1) {
		data.addColumn('datetime', '');
	} else if (timeType == 3) {
		data.addColumn('string', '');

	} else {
		data.addColumn('date', '');
	}

	data.addColumn('number', dd_1_text);
	data.addColumn('number', dd_2_text);
	data.addColumn('number', dd_3_text);
	data.addColumn('number', dd_4_text);
	data.addColumn('number', dd_5_text);
	data.addColumn('number', dd_6_text);


	//	dateFormat = set_dateformat_googlechart(timeType, data);

	var arr_data = [];
	var $count = 0;
	if (timeType == 3) { // case : year period show every month
		for ($month = 0; $month < 12; $month++) {


			var new_row = add_new_arr(comparedata_1_dataitem, comparedata_2_dataitem, comparedata_3_dataitem, comparedata_4_dataitem, comparedata_5_dataitem, comparedata_6_dataitem, $month, 3);
			arr_data.push(new_row);

		}
		$count = 12;
	} else { // case :  show every date

		var duration_month = $("#duration_month").val();
		const duration_month_arr = duration_month.split("/");

		var day_in_month = new Date(duration_month_arr[1], parseInt(duration_month_arr[0]), 0).getDate();
		for ($day = 0; $day < day_in_month; $day++) {
			var new_row = add_new_arr(comparedata_1_dataitem, comparedata_2_dataitem, comparedata_3_dataitem, comparedata_4_dataitem, comparedata_5_dataitem, comparedata_6_dataitem, $day, 2);
			arr_data.push(new_row);
		}
		$count = day_in_month;
	}


	data.addRows(arr_data);

	$format = get_format_chart(timeType);
	var options = {
		chart: {
			title: '',
		},
		chartArea: {
			// leave room for y-axis labels
			width: '92%',
			bottom: '30',
			left:'8%'
		},

		height: 200,
		legend: 'none',
		colors: ['#91c5ff', '#b8d719', '#ff8889', '#c078ff', '#ffc324', '#ff8afc'],
		hAxis: {
			showTextEvery: 1,
			format: $format,
			slantedText: true,

			slantedTextAngle: 45,
			textStyle: {

				fontSize: 7 // or the number you want

			},
			gridlines: {
				count: $count
			},
			

		},
		pointSize: 5,
		tooltip: { textStyle: { fontName: 'verdana', fontSize: 18 } },   
		explorer: { 
            actions: ['dragToZoom', 'rightClickToReset'],
            axis: 'horizontal',
            keepInBounds: true,
            maxZoomIn: 4.0},

		



	};
	var chart_div = document.getElementById('line_chart');

	// google chart :  new version 
	// var chart = new google.charts.Line(chart_div);
	// chart.draw(data, google.charts.Line.convertOptions(options));

	// google chart :  old version 
	var chart = new google.visualization.LineChart(chart_div);
	chart.draw(data, options);





}

function set_dateformat_googlechart(timeType, data) {
	if (timeType == 1) {

		var dateFormat = new google.visualization.DateFormat({
			formatType: 'long',
			timeZone: +7
		});
		dateFormat.format(data, 0);
	} else if (timeType == 2) {
		var dateFormat = new google.visualization.DateFormat({
			formatType: 'long',
			timeZone: +7
		});
		dateFormat.format(data, 0);
	} else if (timeType == 3) {

		var dateFormat = new google.visualization.DateFormat({
			pattern: "MMM",
			timeZone: +7
		});
		dateFormat.format(data, 0);
	} else {
		var dateFormat = new google.visualization.DateFormat({
			pattern: "yyyy"
		});
		dateFormat.format(data, 0);
	}
	return dateFormat;
}

function get_format_chart(timeType) {
	var $format = "";
	if (timeType == 1) {
		$format = "HH:mm";
	} else if (timeType == 2) {
		$format = "d";
	} else if (timeType == 3) {
		$format = "MMM";
	} else {
		$format = "YYYY-MM-dd";
	}
	return $format;
}

function onaction_firstloadofpage() {


	$("#duration_year").hide();
	$("#angle_down_year").hide();

	drawtable(); // table :  onloading

		// set selected dropdown
		if (customer_lastselected != 0) {

			var station_arr = JSON.parse(customer_lastselected.compare_data);
			if (station_arr.length == 6) {
				for ($i = 0; $i <= 5; $i++) {
					var dd_no = $i + 1;
					var station_item = parseInt(station_arr[$i]);
						// check value exits on option
						if ($("#dropdown_planlist_" + dd_no + " option[value='" + station_item + "']").val() != undefined) {
							$("#dropdown_planlist_" + dd_no).val(parseInt(station_arr[$i]));
					}
				}
			}

		}


		onchange_data_planlist(); // first loading : call ajax

	}

	function setting_duration() {
		var currentTime = new Date();


		// returns the year (four digits)
		var current_year = currentTime.getFullYear()
		$('input[name="duration_month"]').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'mm/yy',
			showButtonPanel: true,
			yearRange: "2020:" + current_year,

			daysOfWeek: [
				"Su",
				"Mo",
				"Tu",
				"We",
				"Th",
				"Fr",
				"Sa"
			],
			monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
				'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
			],
			monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
				'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
			],

			onClose: function () {
				var oldvalue = $("#duration_month").val();
				var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
				var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
				var newvalue =$("#duration_month").val();
				if(oldvalue != newvalue){
					main_controller_event();
				}
		
			},
			 
		

			beforeShow: function () {
				if ((selDate = $(this).val()).length > 0) {


					var selDate_split = $(this).val().split('/');
					var iMonth = selDate_split[0];
					iMonth = iMonth > 0 ? iMonth - 1 : 0;
					var iYear = selDate_split[1];
					$(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
					$(this).datepicker('setDate', new Date(iYear, iMonth, 1));
				}
			}
		});



		$("#duration_year").datepicker({

			format: "yyyy",
			viewMode: "years",
			minViewMode: "years"
		});

	}



	function showdatepicker() {
		$("#duration_month").datepicker('show');
	}






	$('input[type=radio][name=radio_period]').change(function () {

		main_controller_event(this.value);

	});



	function main_controller_event(period = null) {

		var station_arr = [];
		$('select[name="dropdown_planlist[]"] option:selected').each(function () {
			station_arr.push($(this).val());
		});

		if (period == null) {
			period = $("input[type='radio']:checked").val();

		}


		if (period == 'month') {
			var timeType = 2;
			var duration_month = $("#duration_month").val();
			$(".text_period").text("แสดงเป็นรายเดือน");
			setting_period_show("month"); // event : dropdown visible disable
			ajax_comparedata(timeType, duration_month, "month", station_arr);


		} else if (period == 'year') {

			var timeType = 3;
			var duration_year = $("#duration_year").val();
			$(".text_period").text("แสดงเป็นรายปี");
			setting_period_show("year"); // event : dropdown visible disable
			ajax_comparedata(timeType, duration_year, "year", station_arr);

		}
	}

	function setting_period_show(period_text = null) {
			var period_value = "";
			if (period_text == null) {
				var period_value = $('input[type=radio][name=radio_period]').val();
			}
			if (period_value == "month" || period_text == "month") {
				$("#duration_year").hide();
				$("#duration_month").show();
				$("#angle_down_month").show();
				$("#angle_down_year").hide();
		
			
			} else {
				$("#duration_month").hide();
				$("#duration_year").show();
				$("#angle_down_month").hide();
				$("#angle_down_year").show();
		
			
		
			}
		}

	function set_chartlabel(dd_1_text, dd_2_text, dd_3_text, dd_4_text, dd_5_text, dd_6_text) {

		$("#text_chart_compare1").html(dd_1_text);
		$("#text_chart_compare2").html(dd_2_text);
		$("#text_chart_compare3").html(dd_3_text);
		$("#text_chart_compare4").html(dd_4_text);
		$("#text_chart_compare5").html(dd_5_text);
		$("#text_chart_compare6").html(dd_6_text);
	}

	function ajax_comparedata(timeType, duration, period, station_arr) {
		$(".loader").show();
		$.ajax({
			type: "POST",
			url: base_url + "Compare/ajax_comparedata",
			data: {
				timeType: timeType,
				duration: duration,

				station_arr: station_arr,
				period: period
			},
			success: function (response, status, xhr) {
				var response = JSON.parse(response);

				if (response.status == true) {
					var data = JSON.parse(response.data);
					onaction_drawing_chart(data, timeType);
					event_setdata_datatable(data, timeType);

				}
				ajax_saveuserbehavior(station_arr, timeType, user_id); // log : save user behavior
				$(".loader").hide();
			}
		});



	}

	function ajax_saveuserbehavior(station_arr, timeType) {

		$.ajax({
			type: "POST",
			url: base_url + "Compare/ajax_saveuserbehavior",
			data: {
				timeType: timeType,
				user_id: user_id,
				station_arr: station_arr,
			},
			success: function (response, status, xhr) {
				var response = JSON.parse(response);

				if (response.status == true) {
					console.log('save log successful ..')
				}


			}
		});

	}

	function event_setdata_datatable(data, timeType) {

		var dataTable_ = drawtable(timeType);



		if (data.length > 0) {


			dataTable_.clear().draw(); // clear : data on datatable
			var comparedata_1_dataitem = get_actual_data(data, 0);
			var comparedata_2_dataitem = get_actual_data(data, 1);
			var comparedata_3_dataitem = get_actual_data(data, 2);
			var comparedata_4_dataitem = get_actual_data(data, 3);
			var comparedata_5_dataitem = get_actual_data(data, 4);
			var comparedata_6_dataitem = get_actual_data(data, 5);
			if (timeType == 3) { // case : year period show every month
				for ($month = 0; $month < 12; $month++) {


					add_new_row(dataTable_, comparedata_1_dataitem, comparedata_2_dataitem, comparedata_3_dataitem, comparedata_4_dataitem, comparedata_5_dataitem, comparedata_6_dataitem, $month, 3);


				}
			} else { // case :  show every date

				var duration_month = $("#duration_month").val();
				const duration_month_arr = duration_month.split("/");

				var day_in_month = new Date(duration_month_arr[1], parseInt(duration_month_arr[0]), 0).getDate();
				for ($day = 0; $day < day_in_month; $day++) {
					add_new_row(dataTable_, comparedata_1_dataitem, comparedata_2_dataitem, comparedata_3_dataitem, comparedata_4_dataitem, comparedata_5_dataitem, comparedata_6_dataitem, $day, 2);
				}
			}

		} else {
			dataTable_.clear().draw(); // clear : data on datatable
		}
		dataTable_.draw(false);
		$(".loader").hide();

	}

	function add_new_row(dataTable_, comparedata_1_dataitem, comparedata_2_dataitem, comparedata_3_dataitem, comparedata_4_dataitem, comparedata_5_dataitem, comparedata_6_dataitem, $period, timeType) {
		var period_text = "";
		var generationValue_1 = comparedata_1_dataitem[$period] != undefined ? comparedata_1_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_1 = commawithtwodecimal(generationValue_1);
		var generationValue_2 = comparedata_2_dataitem[$period] != undefined ? comparedata_2_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_2 = commawithtwodecimal(generationValue_2);
		var generationValue_3 = comparedata_3_dataitem[$period] != undefined ? comparedata_3_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_3 = commawithtwodecimal(generationValue_3);
		var generationValue_4 = comparedata_4_dataitem[$period] != undefined ? comparedata_4_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_4 = commawithtwodecimal(generationValue_4);
		var generationValue_5 = comparedata_5_dataitem[$period] != undefined ? comparedata_5_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_5 = commawithtwodecimal(generationValue_5);
		var generationValue_6 = comparedata_6_dataitem[$period] != undefined ? comparedata_6_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_6 = commawithtwodecimal(generationValue_6);

		if (timeType == 3) {
			period_text = GetMonthName($period + 1);
		} else {
			period_text = parseInt($period) + 1;
		}
		dataTable_.row.add([
			period_text, generationValue_1, generationValue_2, generationValue_3, generationValue_4, generationValue_5, generationValue_6
		]).node().id = 'td_' + 1;
	}

	function add_new_arr(comparedata_1_dataitem, comparedata_2_dataitem, comparedata_3_dataitem, comparedata_4_dataitem, comparedata_5_dataitem, comparedata_6_dataitem, $period, timeType) {
		var arr = [];
		var period_text = "";
		var generationValue_1 = comparedata_1_dataitem[$period] != undefined ? comparedata_1_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_1 = commawithtwodecimal(generationValue_1);
		var generationValue_2 = comparedata_2_dataitem[$period] != undefined ? comparedata_2_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_2 = commawithtwodecimal(generationValue_2);
		var generationValue_3 = comparedata_3_dataitem[$period] != undefined ? comparedata_3_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_3 = commawithtwodecimal(generationValue_3);
		var generationValue_4 = comparedata_4_dataitem[$period] != undefined ? comparedata_4_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_4 = commawithtwodecimal(generationValue_4);
		var generationValue_5 = comparedata_5_dataitem[$period] != undefined ? comparedata_5_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_5 = commawithtwodecimal(generationValue_5);
		var generationValue_6 = comparedata_6_dataitem[$period] != undefined ? comparedata_6_dataitem[$period].generationValue.toFixed(2) : 0;
		generationValue_6 = commawithtwodecimal(generationValue_6);

		if (timeType == 3) {
			period_text = GetMonthName($period + 1);

		} else {
			var duration_month = $("#duration_month").val();
			const duration_month_arr = duration_month.split("/");
			var month = parseInt(duration_month_arr[0]) > 0 ? parseInt(duration_month_arr[0]) - 1 : 0;
			period_text = new Date(duration_month_arr[1], month, $period + 1);

		}

		arr.push(period_text, parseFloat(generationValue_1), parseFloat(generationValue_2), parseFloat(generationValue_3), parseFloat(generationValue_4), parseFloat(generationValue_5), parseFloat(generationValue_6));
		return arr;

	}

	function onchange_data_planlist() {
		// alert($('input[type=radio][name=radio_period]').val());

		var radio_value = $("input[type='radio']:checked").val();
		main_controller_event(radio_value);
	}

	function get_actual_data(data, object_key) {

		if (typeof data[object_key].stationDataItems != "undefined") {
			return data[object_key].stationDataItems;
		} else {
			return false;
		}

	}

	function drawtable(timeType) {



		var eu_height = $(".div_graph").height();
		// alert(eu_height);
		var screen = 0;

		if (timeType != "month") {

			var screen = eu_height > 350 ? 115 : 0;
		} else {

			// var screen = eu_height > 500 ? 365 : 115;

		}


		var scrollY = (eu_height) - screen;

		var columns = [{
				"width": "10%",
				"class": "text-center"
			},
			{
				"width": "15%",
				"class": "text-center"
			},
			{
				"width": "15%",
				"class": "text-center"
			},
			{
				"width": "15%",
				"class": "text-center"
			},
			{
				"width": "15%",
				"class": "text-center"
			},
			{
				"width": "15%",
				"class": "text-center"
			},
			{
				"width": "15%",
				"class": "text-center"
			},
		]

		var dataTable_ = $('#table_detail').DataTable({
			"processing": false,
			"bDestroy": true,
			"bPaginate": false,
			"bLengthChange": false,
			// "pageLength" : 5,
			"bFilter": false,
			"bInfo": false,
			"searching": false,
			"ordering": false,
			"pageResize": true,
			"scrollY": scrollY + 'px',
			"scrollCollapse": true,



			initComplete: function () {

			},
			createdRow: function (row, data, dataIndex) {
				// Set the data-status attribute, and add a class
				$(row).addClass("text_font_tabletd");
			},
			"columns": columns
		});

		return dataTable_;
	}


	function onchange_draw_data_year() {
		var radio_value = $("input[type='radio']:checked").val();
		main_controller_event(radio_value);

	}




	// //////// test chart
	// var data = new google.visualization.DataTable();
	// data.addColumn('date', 'Time of Day');
	// data.addColumn('number', 'Rating');

	// data.addRows([
	//   [new Date(2015, 0, 1), 5],  [new Date(2015, 0, 2), 7],  [new Date(2015, 0, 3), 3],
	//   [new Date(2015, 0, 4), 1],  [new Date(2015, 0, 5), 3],  [new Date(2015, 0, 6), 4],
	//   [new Date(2015, 0, 7), 3],  [new Date(2015, 0, 8), 4],  [new Date(2015, 0, 9), 2],
	//   [new Date(2015, 0, 10), 5], [new Date(2015, 0, 11), 8], [new Date(2015, 0, 12), 6],
	//   [new Date(2015, 0, 13), 3], [new Date(2015, 0, 14), 3], [new Date(2015, 0, 15), 5],
	//   [new Date(2015, 0, 16), 7], [new Date(2015, 0, 17), 6], [new Date(2015, 0, 18), 6],
	//   [new Date(2015, 0, 19), 3], [new Date(2015, 0, 20), 1], [new Date(2015, 0, 21), 2],
	//   [new Date(2015, 0, 22), 4], [new Date(2015, 0, 23), 6], [new Date(2015, 0, 24), 5],
	//   [new Date(2015, 0, 25), 9], [new Date(2015, 0, 26), 4], [new Date(2015, 0, 27), 9],
	//   [new Date(2015, 0, 28), 8], [new Date(2015, 0, 29), 6], [new Date(2015, 0, 30), 4],
	//   [new Date(2015, 0, 31), 6], [new Date(2015, 1, 1), 7],  [new Date(2015, 1, 2), 9]
	// ]);


	// var options = {
	//   title: 'Rate the Day on a Scale of 1 to 10',
	//   width: 900,
	//   height: 500,
	//   hAxis: {
	// 	format: 'dd',
	// 	gridlines: {count: 15}
	//   },
	//   vAxis: {
	// 	gridlines: {color: 'none'},
	// 	minValue: 0
	//   }
	// };

	// var chart = new google.visualization.LineChart(document.getElementById('line_chart'));

	// chart.draw(data, options);

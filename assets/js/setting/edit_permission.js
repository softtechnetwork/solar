function checkedEventAbility(element){
	var element = jQuery(element);
	var base_url = jQuery('input[name="base_url"]').val();
	var post_data = {
		'user_menu_id':element.data('menuid'),
		'event':element.val(),
		'user_id':element.data('user'),
		'checked_status':(element.is(':checked'))?'checked':'unchecked'
	};

					jQuery.ajax({
                          type: "POST",
                          url: base_url+"setting/ajaxSetEventAbility",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               //element.parent().parent().fadeOut('slow');
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                    });
	
	console.log(post_data);
}

function enableDisableMenuPermission(element){
	var element = jQuery(element);
	var base_url = jQuery('input[name="base_url"]').val();

	var post_data = {
		'user_id':element.data('user'),
		'user_menu_id':element.val(),
		'checked_status':(element.is(':checked'))?'checked':'unchecked'
	};

	if(post_data.checked_status === 'checked'){
		var closest_tr = element.closest('tr');
		jQuery.each(closest_tr.find('input[name="check_event"]'),function(index,value){
			jQuery(value).removeAttr('disabled');
		});
		//console.log(closest_tr);
	}else{

		var closest_tr = element.closest('tr');
		jQuery.each(closest_tr.find('input[name="check_event"]'),function(index,value){
			jQuery(value).attr('disabled','disabled');
			jQuery(value).attr('checked',false);
		});
	}

					jQuery.ajax({
                          type: "POST",
                          url: base_url+"setting/ajaxSetEnableDisableMenuPermission",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               //element.parent().parent().fadeOut('slow');
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                    });
	
}

jQuery(document).ready(function(){
	jQuery('input[name="check_menu_permission"]').each(function(index,value){
		var checkbox_element = jQuery(value);
		if(checkbox_element.is(':checked')){
			var closest_tr = checkbox_element.closest('tr');
			jQuery.each(closest_tr.find('input[name="check_event"]'),function(index,v){
				jQuery(v).removeAttr('disabled');
			});
		}else{
			var closest_tr = checkbox_element.closest('tr');
			jQuery.each(closest_tr.find('input[name="check_event"]'),function(index,v){
				jQuery(v).attr('disabled','disabled');
			});
		}
	});
});
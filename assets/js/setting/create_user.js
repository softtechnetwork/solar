jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="create-user-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                user_accesstype_id: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                firstname:{
                    validators:{
                        notEmpty:{
                            message: ''
                        }
                    }
                },
                lastname:{
                    validators:{
                        notEmpty:{
                            message: ''
                        }
                    }
                },
                email:{
                    validators:{
                        remote: {
                            message: jQuery('input[name="email_exist_exception"]').val(),
                            url: base_url+'setting/ajaxCheckUserEmailExist'
                        },
                        notEmpty:{
                            message: ''
                        },
                        emailAddress:{
                            message: ''
                        }
                    }
                },
                password:{
                    validators:{
                        notEmpty:{
                            message: ''
                        },
                        stringLength: {
                                min: 6,
                                message: ''
                        }
                    }
                },
                confirm_password:{
                    validators:{
                        notEmpty:{
                            message: ''
                        },
                        identical: {
                                field: 'password',
                                message: ''
                        }
                    }
                }
                // code:{
                //     validators:{
                //         notEmpty:{
                //             message: jQuery('input[name="code_empty_exception"]').val()
                //         },
                //         stringLength: {
                //                 min: 3,
                //                 max: 50,
                //                 message: jQuery('input[name="code_length_exception"]').val()
                //         }
                //     }
                // },
                // name: {
                //     validators: {
                //         notEmpty: {
                //             message: jQuery('input[name="lastname_empty_exception"]').val()
                //         }
                //     }
                // },
                // detail: {
                //     validators: {
                //         notEmpty: {
                //             message: jQuery('input[name="lastname_empty_exception"]').val()
                //         }
                //     }
                // }
            }
    });


    

});
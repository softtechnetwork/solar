jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();

    jQuery('form[name="reset-password-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_password: {
                    validators: {
                        notEmpty: {
                            message: ''
                        },
                        stringLength: {
                                min: 6,
                                message: ''
                        }
                    }
                },
                confirm_new_password:{
                    validators:{
                        notEmpty:{
                            message: ''
                        },
                        identical: {
                                field: 'new_password',
                                message: ''
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
                e.preventDefault();
                /* send data to reset password*/
                var form_data = jQuery('form[name="reset-password-form"]').serialize();
                jQuery.ajax({
                          type: "POST",
                          url: base_url+"setting/ajaxResetUserPassword",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               //element.parent().parent().fadeOut('slow');
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                    });

            }
    });
});
jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="create-techskill-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="firstname_empty_exception"]').val()
                        }
                    }
                },
                code:{
                    validators:{
                        notEmpty:{
                            message: jQuery('input[name="code_empty_exception"]').val()
                        },
                        stringLength: {
                                min: 3,
                                max: 50,
                                message: jQuery('input[name="code_length_exception"]').val()
                        }
                    }
                },
                detail: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="lastname_empty_exception"]').val()
                        }
                    }
                }
            }
    });

});

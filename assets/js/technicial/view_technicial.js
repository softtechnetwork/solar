function initialize_map() {

      var lat_cur = (jQuery('#latitude').val() != '')?parseFloat(jQuery('#latitude').val()):parseFloat('13.736279');
      var lng_cur = (jQuery('#longitude').val() != '')?parseFloat(jQuery('#longitude').val()):parseFloat('100.64064499999995');

      // console.log('lat lon');
      
      // console.log(lat_cur);
      // console.log(lng_cur);

      var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:lat_cur,
                        lng:lng_cur  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:lat_cur,
                        lng:lng_cur                 
                },
                map:map,
                draggable:true
        });

        marker.setMap(map);

        var iw = new google.maps.InfoWindow({
            content: "<strong>ที่อยู่ของช่าง</strong>"
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });  


        
       google.maps.event.addDomListener(window, 'load', initialize_map);

}

jQuery(document).ready(function(){
  renderAgentImage();



});

function renderAgentImage(){
    var agent_code = jQuery('input[name="hide_agent_code"]').val();
  //console.log(agent_code)

                  var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"technicial/ajaxGetTechnicialImageProfile",
                          async:true,
                          dataType:'json',
                          data:{'agent_code':agent_code},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               //element.parent().parent().fadeOut('slow');
                               jQuery('div#tech_image_profile').find('i.fa').remove();
                               jQuery("#agent_image").attr('src','data:image/png;base64,'+response.data.Image);
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

}
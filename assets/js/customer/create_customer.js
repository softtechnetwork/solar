jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="create-customer-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                firstname: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="firstname_empty_exception"]').val()
                        }
                    }
                },
                lastname: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="lastname_empty_exception"]').val()
                        }
                    }
                },
                telephone: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="telephone_empty_exception"]').val()
                        }
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="address_empty_exception"]').val()
                        }
                    }
                },
                select_province:{
                    validators:{
                        notEmpty:{
                          message:''
                        }
                    }
                },
                select_amphur:{
                    validators:{
                        notEmpty:{
                          message:''
                        }
                    }
                },
                select_district:{
                    validators:{
                        notEmpty:{
                          message:''
                        }
                    }
                },
                zipcode:{
                    validators:{
                        notEmpty:{
                          message:''
                        }
                    }
                }
            }
    });

});


function changeProvince(province_id){
    var base_url = jQuery('input[name="base_url"]').val();
    
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getAmphurByProvinceId",
                          async:true,
                          dataType:'json',
                          data:{'province_id':province_id},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="select_amphur"]').html('');
                                            jQuery('select[name="select_district"]').html('');
                                            jQuery('input[name="zipcode"]').val('');

                                            jQuery('select[name="select_district"]').attr('readonly','readonly');
                                            jQuery('input[name="zipcode"]').attr('readonly','readonly');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                 jQuery('select[name="select_amphur"]')
                                                     .append(jQuery("<option></option>")
                                                                .attr("value",key)
                                                                .text(value)); 
                                                });
                                              jQuery('select[name="select_amphur"]').removeAttr('readonly');
                                              jQuery('select[name="select_amphur"]').focus();

                              if(jQuery('input[name="hide_amphur"]').val()){
                                    jQuery('select[name="select_amphur"]').val(jQuery('input[name="hide_amphur"]').val());
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });



}

function changeAmphur(amphur_id){

                if(amphur_id != '0'){
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getDistrictByAmphurId",
                          async:true,
                          dataType:'json',
                          data:{'amphur_id':amphur_id,'province_id':jQuery("select[name='select_province']").val()},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="select_district"]').html('');
                                jQuery('input[name="zipcode"]').val('');
                                jQuery('input[name="zipcode"]').attr('readonly','readonly');
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                     jQuery('select[name="select_district"]')
                                                         .append(jQuery("<option></option>")
                                                                    .attr("value",key)
                                                                    .text(value)); 
                                                });
                                                jQuery('select[name="select_district"]').removeAttr('readonly');
                                                jQuery('select[name="select_district"]').focus();
                                                //changeDistrict(jQuery('select[name="district"]').val());
                                  if(jQuery('input[name="hide_district"]').val()){
                                    jQuery('select[name="select_district"]').val(jQuery('input[name="hide_district"]').val());
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
                }


}

function changeDistrict(district_id){

            if(district_id != '0'){
                var province_id = jQuery("select[name='select_province']").val();
                var amphur_id = jQuery("select[name='select_amphur']").val();

                var post_data = {
                    'province_id':province_id,
                    'amphur_id':amphur_id,
                    'district_id':district_id

                }
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getZipcodeByDistrictId",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                           jQuery('input[name="zipcode"]').val('');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('input[name="zipcode"]').val(response.zipcode).change();
                                jQuery('input[name="zipcode"]').removeAttr('readonly');
                                // jQuery('input[name="zipcode"]').focus();
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

                }


}

function fillAddressProfile(){
    var province_id = jQuery('input[name="hide_province"]').val();
    var amphur_id = jQuery('input[name="hide_amphur"]').val();
    var district_id = jQuery('input[name="hide_district"]').val();

    console.log('fillAddressProfile');
    console.log(province_id);
    console.log(amphur_id);
    console.log(district_id);

    if(province_id && amphur_id && district_id){
        changeProvince(province_id);
        setTimeout(
          function() 
          {
            changeAmphur(amphur_id);
          }, 200);

        setTimeout(
          function() 
          {
            changeDistrict(district_id);
          }, 200);

    }
}

jQuery(document).ready(function(){
    fillAddressProfile();
});


function initialize_map() {

      var lat_cur = (jQuery('#lat').val() != '')?parseFloat(jQuery('#lat').val()):parseFloat('13.736279');
      var lng_cur = (jQuery('#lng').val() != '')?parseFloat(jQuery('#lng').val()):parseFloat('100.64064499999995');

      console.log('lat lon');
      
      console.log(lat_cur);
      console.log(lng_cur);

      var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:lat_cur,
                        lng:lng_cur  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:lat_cur,
                        lng:lng_cur                 
                },
                map:map,
                draggable:true
        });

        marker.setMap(map);  


        // textfield search
        var searchBox = new google.maps.places.SearchBox(document.getElementById('find_location'));
        // seach change event
        google.maps.event.addListener(searchBox,'places_changed',function(){

                var places = searchBox.getPlaces();
                console.log(places);

                var bounds = new google.maps.LatLngBounds();
                var jsonbounds = JSON.stringify(bounds);
                var i,place;
                
                
                for(i=0;place=places[i];i++){   
                        try       
                        {
                                bounds.extend(place.geometry.location);
                                marker.setPosition(place.geometry.location);
                                console.log(places);
                                var latitude = places[i].geometry.location.lat();
                                var longitude  =  places[i].geometry.location.lng();
                                console.log("LAT::"+latitude+"LNG"+longitude);
                                jQuery("#lat").val(latitude);
                                jQuery("#lng").val(longitude);
                        }
                        catch(e)    
                        {
                                console.log(e);    
                        }
                map.setZoom(1);       
                }
                
                
                map.fitBounds(bounds);


                initialize_map();
        });
        
        google.maps.event.addListener(marker, 'dragend', function(marker){
        var latLng = marker.latLng;
        jQuery("#lat").val(latLng.lat());
        jQuery("#lng").val(latLng.lng());
        
        });
        
       google.maps.event.addDomListener(window, 'load', initialize_map);

}
function initialize_map() {

      var lat_cur = (jQuery('#latitude').val() != '')?parseFloat(jQuery('#latitude').val()):parseFloat('13.736279');
      var lng_cur = (jQuery('#longitude').val() != '')?parseFloat(jQuery('#longitude').val()):parseFloat('100.64064499999995');

      console.log('lat lon');
      
      console.log(lat_cur);
      console.log(lng_cur);


      /* for edit */
      var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:lat_cur,
                        lng:lng_cur  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
      });

      var map2 = new google.maps.Map(document.getElementById('map-canvas-create'),{
                center:{
                        lat:lat_cur,
                        lng:lng_cur  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
      }); 

      var marker = new google.maps.Marker({   
                position:{
                        lat:lat_cur,
                        lng:lng_cur                 
                },
                map:map,
                draggable:true
      });

      var marker2 = new google.maps.Marker({   
                position:{
                        lat:lat_cur,
                        lng:lng_cur                 
                },
                map:map,
                draggable:true
      });

      marker.setMap(map);  
      marker2.setMap(map2);

      /* eof for edit */




        ////======================= for edit ============================================
        // textfield search
        var searchBox = new google.maps.places.SearchBox(document.getElementById('find_location'));
        // seach change event
        google.maps.event.addListener(searchBox,'places_changed',function(){

                var places = searchBox.getPlaces();
                console.log(places);

                var bounds = new google.maps.LatLngBounds();
                var jsonbounds = JSON.stringify(bounds);
                var i,place;
                
                jQuery('input[name="confirm_lat_lon"]').attr('checked','checked');
                jQuery('input[name="address_by_customer"]').removeAttr('checked');
                for(i=0;place=places[i];i++){   
                        try       
                        {
                                bounds.extend(place.geometry.location);
                                marker.setPosition(place.geometry.location);
                                console.log(places);
                                var latitude = places[i].geometry.location.lat();
                                var longitude  =  places[i].geometry.location.lng();
                                console.log("LAT::"+latitude+"LNG"+longitude);
                                jQuery("#latitude").val(latitude);
                                jQuery("#longitude").val(longitude);
                        }
                        catch(e)    
                        {
                                console.log(e);    
                        }
                map.setZoom(1);       
                }
                
                
                map.fitBounds(bounds);


                initialize_map();
        });

        //=============================== EOF FOR EDIT ==================


        //=============================== FOR CREATE ====================
        var searchBoxCreate = new google.maps.places.SearchBox(document.getElementById('find_location_create'));
        // seach change event
        google.maps.event.addListener(searchBoxCreate,'places_changed',function(){

                var places = searchBoxCreate.getPlaces();
                console.log(places);

                var bounds = new google.maps.LatLngBounds();
                var jsonbounds = JSON.stringify(bounds);
                var i,place;
                
                jQuery('input[name="confirm_lat_lon"]').attr('checked','checked');
                jQuery('input[name="address_by_customer"]').removeAttr('checked');
                for(i=0;place=places[i];i++){   
                        try       
                        {
                                bounds.extend(place.geometry.location);
                                marker2.setPosition(place.geometry.location);
                                console.log(places);
                                var latitude = places[i].geometry.location.lat();
                                var longitude  =  places[i].geometry.location.lng();
                                console.log("LAT::"+latitude+"LNG"+longitude);
                                jQuery("#create_latitude").val(latitude);
                                jQuery("#create_longitude").val(longitude);
                        }
                        catch(e)    
                        {
                                console.log(e);    
                        }
                map2.setZoom(1);       
                }
                
                
                map2.fitBounds(bounds);


                initialize_map();
        });

        //=============================== EOF FOR CREATE ================
        

        //=============================== FOR EDIT =======================
        google.maps.event.addListener(marker, 'dragend', function(marker){
            var latLng = marker.latLng;
            jQuery("#latitude").val(latLng.lat());
            jQuery("#longitude").val(latLng.lng());

            jQuery('input[name="confirm_lat_lon"]').attr('checked','checked');
            jQuery('input[name="address_by_customer"]').removeAttr('checked');
        
        });


        //================================ FOR CREATE =====================
        google.maps.event.addListener(marker2, 'dragend', function(marker2){
            //console.log('aaaa');
            var latLng = marker2.latLng;
            console.log(latLng.lat());
            console.log(latLng.lng());
            jQuery("#create_latitude").val(latLng.lat());
            jQuery("#create_longitude").val(latLng.lng());

            jQuery('input[name="confirm_lat_lon"]').attr('checked','checked');
            jQuery('input[name="address_by_customer"]').removeAttr('checked');
        
        });

        
       google.maps.event.addDomListener(window, 'load', initialize_map);

}

function editDeviceAddressAndMapModal(element){
    var element = jQuery(element);
    var device_id = element.data('deviceid');

    var edit_device_form = jQuery('form[name="edit-device-form"]');

                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/ajaxGetDeviceSerialNumberData",
                          async:true,
                          dataType:'json',
                          data:{'product_serial_number_id':device_id},
                          beforeSend: function( xhr ) {

                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('#editDeviceAddressAndMapModal').modal('toggle');
                                edit_device_form.find('input[name="hide_device_id"]').val(response.data.id);
                                edit_device_form.find('input[name="serial_number"]').val(response.data.serial_number);
                                edit_device_form.find('textarea[name="address"]').val(response.data.address);
                                edit_device_form.find('input[name="latitude"]').val(response.data.latitude);
                                edit_device_form.find('input[name="longitude"]').val(response.data.longitude);
                                edit_device_form.find('select[name="product_id"]').val(response.data.product_id);
                                edit_device_form.find('input[name="address_by_customer"]').attr('checked','checked');
                                
                                if(response.data.confirm_lat_lon){
                                    edit_device_form.find('input[name="confirm_lat_lon"]').attr('checked','checked');
                                }else{
                                    edit_device_form.find('input[name="confirm_lat_lon"]').removeAttr('checked');
                                }

                                if(response.data.address_by_customer){
                                    edit_device_form.find('input[name="address_by_customer"]').attr('checked','checked');
                                }else{
                                    edit_device_form.find('input[name="address_by_customer"]').removeAttr('checked');
                                }



                                fillAddressData(response);
                                initialize_map();
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                    });

}

function fillAddressData(response){
    var province_id = response.data.province_id;
    var amphur_id = response.data.amphur_id;
    var district_id = response.data.district_id;

    console.log('province_id = '+province_id);
    console.log('amphur_id = '+amphur_id);
    console.log('district_id = '+district_id);


    if(province_id && amphur_id && district_id){
        changeProvince(province_id);
        jQuery('select[name="select_province"]').val(province_id);
        setTimeout(
          function() 
          {
            changeAmphur(amphur_id);
            jQuery('select[name="select_amphur"]').val(amphur_id);

            setTimeout(
              function() 
              {
                jQuery('select[name="select_district"]').val(district_id);
                changeDistrict(response.data.district_id);
                
              }, 200);

          }, 200);

        
    }
}

function changeProvince(province_id){
    var base_url = jQuery('input[name="base_url"]').val();
    
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getAmphurByProvinceId",
                          async:true,
                          dataType:'json',
                          data:{'province_id':province_id},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="select_amphur"]').html('');
                                            jQuery('select[name="select_district"]').html('');
                                            jQuery('input[name="zipcode"]').val('');

                                            jQuery('select[name="select_district"]').attr('readonly','readonly');
                                            jQuery('input[name="zipcode"]').attr('readonly','readonly');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                 jQuery('select[name="select_amphur"]')
                                                     .append(jQuery("<option></option>")
                                                                .attr("value",key)
                                                                .text(value)); 
                                                });
                                              jQuery('select[name="select_amphur"]').removeAttr('readonly');
                                              jQuery('select[name="select_amphur"]').focus();

                              if(jQuery('input[name="hide_amphur"]').val()){
                                    jQuery('select[name="select_amphur"]').val(jQuery('input[name="hide_amphur"]').val());
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });



}

function changeAmphur(amphur_id){

                if(amphur_id != '0'){
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getDistrictByAmphurId",
                          async:true,
                          dataType:'json',
                          data:{'amphur_id':amphur_id,'province_id':jQuery("select[name='select_province']").val()},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="select_district"]').html('');
                                jQuery('input[name="zipcode"]').val('');
                                jQuery('input[name="zipcode"]').attr('readonly','readonly');
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                     jQuery('select[name="select_district"]')
                                                         .append(jQuery("<option></option>")
                                                                    .attr("value",key)
                                                                    .text(value)); 
                                                });
                                                jQuery('select[name="select_district"]').removeAttr('readonly');
                                                jQuery('select[name="select_district"]').focus();
                                                //changeDistrict(jQuery('select[name="district"]').val());
                                  if(jQuery('input[name="hide_district"]').val()){
                                    jQuery('select[name="select_district"]').val(jQuery('input[name="hide_district"]').val());
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
                }


}

function changeDistrict(district_id){

            if(district_id != '0'){
                var province_id = jQuery("select[name='select_province']").val();
                var amphur_id = jQuery("select[name='select_amphur']").val();

                var post_data = {
                    'province_id':province_id,
                    'amphur_id':amphur_id,
                    'district_id':district_id

                }
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getZipcodeByDistrictId",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                           jQuery('input[name="zipcode"]').val('');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('input[name="zipcode"]').val(response.zipcode).change();
                                jQuery('input[name="zipcode"]').removeAttr('readonly');
                                // jQuery('input[name="zipcode"]').focus();
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

                }


}

//=================== for create ======================

function createchangeProvince(province_id){
    var base_url = jQuery('input[name="base_url"]').val();
    
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getAmphurByProvinceId",
                          async:true,
                          dataType:'json',
                          data:{'province_id':province_id},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="create_select_amphur"]').html('');
                                            jQuery('select[name="create_select_district"]').html('');
                                            jQuery('input[name="create_zipcode"]').val('');

                                            jQuery('select[name="create_select_district"]').attr('readonly','readonly');
                                            jQuery('input[name="create_zipcode"]').attr('readonly','readonly');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                 jQuery('select[name="create_select_amphur"]')
                                                     .append(jQuery("<option></option>")
                                                                .attr("value",key)
                                                                .text(value)); 
                                                });
                                              jQuery('select[name="create_select_amphur"]').removeAttr('readonly');
                                              jQuery('select[name="create_select_amphur"]').focus();

                              if(jQuery('input[name="create_hide_amphur"]').val()){
                                    jQuery('select[name="create_select_amphur"]').val(jQuery('input[name="create_hide_amphur"]').val());
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });



}

function createchangeAmphur(amphur_id){

                if(amphur_id != '0'){
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getDistrictByAmphurId",
                          async:true,
                          dataType:'json',
                          data:{'amphur_id':amphur_id,'province_id':jQuery("select[name='create_select_province']").val()},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="create_select_district"]').html('');
                                jQuery('input[name="create_zipcode"]').val('');
                                jQuery('input[name="create_zipcode"]').attr('readonly','readonly');
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                     jQuery('select[name="create_select_district"]')
                                                         .append(jQuery("<option></option>")
                                                                    .attr("value",key)
                                                                    .text(value)); 
                                                });
                                                jQuery('select[name="create_select_district"]').removeAttr('readonly');
                                                jQuery('select[name="create_select_district"]').focus();
                                                //changeDistrict(jQuery('select[name="district"]').val());
                                  if(jQuery('input[name="create_hide_district"]').val()){
                                    jQuery('select[name="create_select_district"]').val(jQuery('input[name="create_hide_district"]').val());
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
                }


}

function createchangeDistrict(district_id){

            if(district_id != '0'){
                var province_id = jQuery("select[name='create_select_province']").val();
                var amphur_id = jQuery("select[name='create_select_amphur']").val();

                var post_data = {
                    'province_id':province_id,
                    'amphur_id':amphur_id,
                    'district_id':district_id

                }
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/getZipcodeByDistrictId",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                           jQuery('input[name="create_zipcode"]').val('');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('input[name="create_zipcode"]').val(response.zipcode).change();
                                jQuery('input[name="create_zipcode"]').removeAttr('readonly');
                                // jQuery('input[name="zipcode"]').focus();
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

                }


}

jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();

    jQuery('form[name="edit-device-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                serial_number: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
                e.preventDefault();
                var form_data = jQuery('form[name="edit-device-form"]').serialize();
                //console.log(form_data);
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/ajaxSetEditProductSerialNumber",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {

                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){

                                /* close model and alert success */
                                jQuery('#editDeviceAddressAndMapModal').modal('toggle');
                                swal({
                                    title: 'สำเร็จ!!',
                                    text: 'แก้ไขข้อมูลสินค้าสำหรับลูกค้าสำเร็จ',
                                    icon: "success",
                                }).then(function(){
                                    location.reload();
                                });

                                
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });


            },
            onError:function(e){

            }
    });

    jQuery('form[name="create-device-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                serial_number: {
                    validators: {
                        remote:{
                            message: 'Serial Number นี้มีอยู่ในระบบแล้ว',
                            url: base_url+'customer/ajaxCheckAlreadySerialNumber',
                            data:{
                                product_id:jQuery('select[name="create_product_id"]').val()
                            }
                        },
                        notEmpty: {
                            message: ''
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
                e.preventDefault();
                var form_data = jQuery('form[name="create-device-form"]').serialize();
                //console.log(form_data);
                jQuery.ajax({
                          type: "POST",
                          url: base_url+"customer/ajaxSetCreateProductSerialNumber",
                          async:true,
                          dataType:'json',
                          data:form_data,
                          beforeSend: function( xhr ) {

                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                /* close model and alert success */
                                jQuery('#CreateDeviceAddressAndMapModal').modal('toggle');
                                swal({
                                    title: 'สำเร็จ!!',
                                    text: 'เพิ่มสินค้าสำหรับลูกค้าสำเร็จ',
                                    icon: "success",
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                });

            }
    });


});


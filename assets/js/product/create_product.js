var base_url = jQuery('input[name="base_url"]').val();
jQuery('#product_image').fileinput({
        theme: 'fa',
        language: 'th',
        uploadUrl: base_url+'customer/UploadProductImage',
        dropZoneEnabled: false,
        showUpload: false,
        allowedFileExtensions: ['jpg', 'png', 'gif'],

}).on('filebatchuploaderror', function(event, data, msg) {
	console.log('event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchuploadsuccess',function(event,data,msg){
	location.reload();
});

function deleteProductImage(element){
	var element = jQuery(element);

	//console.log(element.parent().parent());return false;
	var image_id = element.data('imageid');
	var product_id = element.data('productid');

	var post_data = {
		product_id:product_id,
		image_id:image_id
	};
					var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"product/ajaxDeleteProductImage",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               element.parent().parent().fadeOut('slow');
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}

function changeCoverStatus(element){
	var element = jQuery(element);

	var image_id = element.data('imageid');
	var product_id = element.data('productid');

	var post_data = {
		product_id:product_id,
		image_id:image_id
	};

					var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"product/ajaxChangeImageCoverStatus",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                               //element.parent().parent().fadeOut('slow');
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });



}

jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="create-product-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                product_type_id: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="firstname_empty_exception"]').val()
                        }
                    }
                },
                code:{
                    validators:{
                        notEmpty:{
                            message: jQuery('input[name="code_empty_exception"]').val()
                        },
                        stringLength: {
                                min: 3,
                                max: 50,
                                message: jQuery('input[name="code_length_exception"]').val()
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="lastname_empty_exception"]').val()
                        }
                    }
                },
                detail: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="lastname_empty_exception"]').val()
                        }
                    }
                }
            }
    });

});
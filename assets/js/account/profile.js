jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="profile-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                firstname: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="firstname_empty_exception"]').val()
                        }
                    }
                },
                lastname: {
                    validators: {
                        notEmpty: {
                            message: jQuery('input[name="lastname_empty_exception"]').val()
                        }
                    }
                }
            }
    });

});
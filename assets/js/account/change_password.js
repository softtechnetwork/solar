jQuery(document).ready(function(){

    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="change-password-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                old_password: {
                    validators: {
                        remote:{
                            message: '',
                            url: base_url+'account/ajaxCheckOldPassword'
                        },
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'new_password',
                                message: ''
                        }
                    }
                }
            }
    });

});
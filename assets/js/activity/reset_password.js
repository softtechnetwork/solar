jQuery(document).ready(function(){
    //var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="reset-password-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_password: {
                    validators: {
                        notEmpty: {
                            message: ''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'new_password',
                                message: ''
                        }
                    }
                }
            }
    });

});
jQuery(document).ready(function(){
	renderDashboardStatus();
});

function renderDashboardStatus(){
	var base_url = jQuery('input[name="base_url"]').val();
	console.log(base_url);
					jQuery.ajax({
                          type: "POST",
                          url: base_url+"dashboard/ajaxRenderDashboardStatus",
                          async:true,
                          dataType:'json',
                          data:{},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery('div#available_count').html('').html('<i class="fa fa-spin fa-spinner"></i>').html(response.return_data.available_count);
                                jQuery('div#pending_count').html('').html('<i class="fa fa-spin fa-spinner"></i>').html(response.return_data.pending_count);
                                jQuery('div#done_count').html('').html('<i class="fa fa-spin fa-spinner"></i>').html(response.return_data.done_count);

                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}
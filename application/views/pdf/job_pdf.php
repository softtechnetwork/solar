
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo $job->job_number?></title>

    <style>
        body{
            font-family: "Garuda";
        }
        .header-title{
            font-size: 14px;
        }
        div.row{
            width: 100%;
            margin: 0;
            padding: 0;
        }
        div.partner-logo{
            float:left;
            border:0px solid #000;
            width:30%;
            text-align:left;            
        }
        div.partner-information{
            float:left;
            width:69%;
            border:0px solid #000;
            margin-left:5px;
            padding: 10px 0;
            font-size: 11px;
             text-align:right; 
        }
        div.clearfix{
            clear:both;
        }
        .content{
            font-size: 12px;
        }
        div.jobinfo-left,
        div.jobinfo-right,
        div.customerinfo-left,
        div.customerinfo-right,
        div.techinfo-left,
        div.techinfo-right,
        div.bottominfo-left,
        div.bottominfo-right{
            width: 50%;
            float: left;
        }
        .text-center{
            text-align: center;
        }
        #bottom-generate-datetime{
            width: 100%;
            text-align: right;
            font-size: 8px;
        }
        /* div.partner-logo  img{
            width:20px;
            height:20px;
            margin:0 auto;
        } */
    </style>
</head>
<body>
 
<div id="container">
    <div class="header">
        <div class="row">
            <div class="partner-logo">
                <center><img src="<?php echo base_url('uploaded/pdf/app_chaang_icon.png')?>" width="60" height="60" style="margin:0 auto;" /></center>
                <p>
                    <strong><?php echo __('CHAANG APPLICATION')?></strong><br>
                </p>
            </div>
            <div class="partner-information">
                <div style="float:left;width:50%; border:0px solid #000">
                    &nbsp;
                </div>
                <div style="float:left;width:50%; border: 0px solid #000">
                    <strong><?php echo __('Company')?> : </strong><span><?php echo $partner['company_name']?></span>
                    <br />
                    <strong><?php echo __('Contact')?> : </strong><span><?php echo $partner['firstname'].' '.$partner['lastname']?></span>
                    <br>
                    <strong><?php echo __('Email')?> : </strong><span><?php echo $partner['email']?></span>
                    <br>
                    <strong><?php echo __('Telephone')?> : </strong><span><?php echo $partner['telephone']?></span>
                </div>
                
            </div>

            <div class="clearfix"></div>
        </div>



    </div>

    <div class="content">
        <!-- center content -->
        <div class="row">
            <h1 style="text-align: center">Worksheet</h1>
        </div>
        <div class="row header-title">
            <strong><u><?php echo __('Job Information')?></u></strong>
        </div>
        <div class="row" id="job-information">
            <div class="jobinfo-left">
               
                <strong><?php echo __('Job Number')?> : </strong> <span><?php echo $job->job_number?></span>
                <br />

                <strong><?php echo __('Job Skill')?> : </strong><span><?php echo $job->technicial_skill->get()->name;?></span>
                <br />


                <strong><?php echo __('Job Created')?> : </strong><span><?php echo date('d/m/Y H:i',strtotime($job->created))?></span>
                <br />

               
                    <strong><?php echo __('Job Received')?> : </strong><span><?php echo (@$job_event['received_datetime'])?date('d/m/Y H:i',strtotime($job_event['received_datetime'])):'-'?></span>
                <br />


                <strong><?php echo __('Job Start Work')?> : </strong><span><?php echo (@$job_event['working_datetime'])?date('d/m/Y H:i',strtotime($job_event['working_datetime'])):'-'?></span>
                <br />

                

                
                    <strong><?php echo __('Job Done')?> : </strong><span><?php echo (@$job_event['done_datetime'])?date('d/m/Y H:i',strtotime($job_event['done_datetime'])):'-'?></span>
                <br />

                
                <!-- <p>
                    <strong><?php echo __('Job Detail')?>: </strong><span><?php echo str_replace("\r\n","<br />",$job->job_detail)?></span>
                </p> -->
            </div>
            <div class="jobinfo-right">
                
                    <strong><?php echo __('Approve Status')?> : </strong><span><?php echo ($job->approve_status)?__('Approved'):__('No approve')?></span>
                    <br />

                    <?php if($job->approve_status){?>
                    <strong><?php echo __('Approved By')?> : </strong><span><?php echo $job_approved_data['approved_by']?></span>
                    <br />
                    <?php }?>

                    
                        

                
                    <strong><?php echo __('Payment status')?> : </strong><span><?php echo __(trim($job->payment_status))?></span>

                    <br />

                    <?php if($job->payment_status == 'paid'){?>
                    <strong><?php echo __('Payment Date')?> : </strong><span><?php echo date('d/m/Y',strtotime($job->payment_date))?></span>
                    <br />
                    <?php }?>

                    <strong><?php echo __('Income')?> : </strong><span><?php echo $job->technicial_skill->get()->income?> <?php echo __('Baht')?></span>
                    <br />


                <br />
            </div>  

        </div>
        <!-- <hr /> -->
        <br />
        <div class="row header-title">
            <strong><u><?php echo __('Customer Information')?></u></strong>
        </div>
        

        <div class="row" id="customermer-information">
            <div class="customerinfo-left">
                <strong><?php echo __('Customer Contact')?> : </strong><span><?php echo $job->customer->get()->firstname.' '.$job->customer->get()->lastname?></span>

                <br />

                <strong><?php echo __('Customer Email')?> : </strong><span><?php echo $job->customer->get()->email?></span>
                <br />

                <strong><?php echo __('Customer Telephone')?> : </strong><span><?php echo $job->customer->get()->telephone?></span>
            </div>
            <div class="customerinfo-right">
                <strong><?php echo __('Customer Address')?> : </strong><span><?php echo "<br>".$job->customer->get()->address?> <?php echo @$customer_address['district']?> <?php echo @$customer_address['amphur']?> <?php echo $customer_address['province']?> <?php echo $job->customer->get()->zipcode?></span>
            </div>
        </div>

        <br />
        <div class="row header-title">
            <strong><u><?php echo __('Technicial Information')?></u></strong>

        </div>


        <div class="row" id="technician-information">
            <div class="techinfo-left">

                <strong><?php echo __('Technician Code')?> : </strong><span><?php echo $job->technicial->get()->code?></span>

                <br />
                <strong><?php echo __('Technician Contact')?> : </strong><span><?php echo $job->technicial->get()->firstname.' '.$job->technicial->get()->lastname?></span>

                <br />

                <strong><?php echo __('Technician Telephone')?> : </strong><span><?php echo $job->technicial->get()->telephone?></span>

                <br />

            </div>
            <div class="techinfo-right">
                <strong><?php echo __('Technicial Address')?> : </strong><span><br><?php echo @$technicial_address['district']?> <?php echo @$technicial_address['amphur']?> <?php echo @$technicial_address['province']?> <?php echo @$technicial_address['zipcode']?></span>
                <br />


                <strong><?php echo __('Technicial ID Card')?> : </strong><span><?php echo ($job->technicial->get()->id_card)?$job->technicial->get()->id_card:'-'?></span>
            </div>
        </div>
        <br />

        <hr />

        <div class="row" id="bottom-info">
            <div class="bottominfo-left text-center">
                <h4><?php echo __('Customer Signature')?> : </h4>

                <?php if($job->customer_signature){?>
                    <img src="<?php echo $this->api_url.'uploaded/job_signature/'.$job->id.'/'.$job->customer_signature?>" width="200" height="200">
                <?php }?>
            </div>
            <div class="bottominfo-right">

            </div>
        </div>

        <div class="row" id="bottom-generate-datetime">
            <small><?php echo __('Export Datetime')?> - <?php echo date('d/m/Y H:i:s')?></small>
        </div>
    </div>
 
</div>
 
</body>
</html>
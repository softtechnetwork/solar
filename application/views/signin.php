<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PSI SOLAR - AUTHENTICATION</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css') ?>">


    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />

    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/themify-icons.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/flag-icon.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/cs-skin-elastic.css') ?>">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/scss/style.css') ?>">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
            font-family: Kanit !important;
        }
    </style>
</head>
<style>
    .wrapper {
        text-align: center;
    }

    form i {
        margin-left: -30px;
        cursor: pointer;
    }

    input::-ms-reveal,
      input::-ms-clear {
        display: none;
      }
    input {
        border-top: none !important;
        border-left: none !important;
        border-right: none !important;
        border-radius: 0px !important;

    }

    input:-webkit-autofill,
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
    input:-webkit-autofill:active {
        transition: background-color 5000s ease-in-out 0s;

        /* -webkit-text-fill-color: #fff !important;
    color:black !important; */
    }

    input:-webkit-autofill {
        -webkit-text-fill-color: #18c3d7 !important;
    }

    @media screen and (min-width: 0px) {

        body {
            height: 100%;

            display: flex;
            justify-content: center;
            align-items: center;

        }


        html {
            height: 100%;
            background-color: white;
        }



        .row {

            height: 100%;
            background-color: white;
            /* background-image: url("./assets/images/login_background.jpg");
                background-repeat: no-repeat;
                background-size: 100% 100%; */

        }

        .panel_1 {}



        .panel_2 {


            background-color: white;
            display: flex;
            justify-content: center;
            align-items: center;

        }

        .panel_2_div {
            width: 85%;

        }



    }


    @media screen and (min-width: 768px) {
        html {
            height: 100%;
            background-color: white;
        }

        body {
            height: 100%;

            display: unset;
            justify-content: unset;
            align-items: unset;


        }

        .row {}

        .panel_1 {

            min-height: 100vh;

            display: flex;
            justify-content: center;
            align-items: center;
        }

        .panel_1_bg {
            background-image: url("./assets/images/login_background.jpg");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }


        .panel_2 {
            background-color: white;


            display: flex;
            justify-content: center;
            align-items: center;

        }

        .panel_2_div {
            width: 75%;
        }


    }

    .text_color_center {
        color: #a2a2a3;
    }

    .input_color_center {
        color: #18c3d7 !important;
    }

    input,
    select,
    textarea {
        color: #18c3d7 !important;
    }
</style>

<body class="bg-white">



    <div class="main-div">
        <div class="container-fluid w-100 h-100">
            <div class="row">
                <div class="panel_1 col-sm-12 col-md-7 col-lg-7   panel_1_bg">


                    <img class="" src="<?php echo base_url('assets/images/logo.png') ?>" style="" alt="">

                </div>
                <div class="panel_2 col-sm-12 col-md-5 col-lg-5">
                    <div style="" class="panel_2_div">
                        <?php echo message_warning($this) ?>
                        <?php echo form_open('', array('name' => 'login-form')) ?>
                        <?php
                        $csrf = array(
                            'name' => $this->security->get_csrf_token_name(),
                            'hash' => $this->security->get_csrf_hash()
                        );
                        ?>
                        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                        <div class="form-group">
                            <label class="text_color_center"><?php echo __('ชื่อเข้าระบบ') ?></label>
                            <?php echo form_input(array(
                                'type' => 'text',
                                'id'   => 'email',
                                'name' => 'email',
                                'class' => 'form-control ',
                                'style' => 'background-color: white !important;'
                            )) ?>
                        </div>


                        <div class="form-group">
                            <label class="text_color_center"><?php echo __('รหัสผ่าน') ?></label>
                            <div class="input-group">

                                <input type="password" name="password" value="" class="form-control" id="password" style="   background-color: white !important;" />
                                <span toggle="#password" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="border-bottom:1px solid #CED4DA;color:#24A0B0;"></span>
                            </div>
                        </div>
                        <div class="wrapper">
                            <button type="submit" style="border-radius: 24px;background-color:#18C3D7;color:white;" class="btn btn-success col-6 m-b-30 m-t-30"><?php echo __('Sign in') ?></button>
                        </div>
                        <?php echo form_close(); ?>


                        <p style="text-align:center;font-size:12px;">Power by 2021 - 2022 Softtech Network Co.,Ltd.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>




    <script src="<?php echo base_url('assets/js/vendor/jquery-2.1.4.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
    <!-- <script src="<?php echo base_url('assets/js/main.js') ?>"></script> -->
    <!-- Cookie Consent by https://www.cookiewow.com -->
    <script type="text/javascript" src="https://cookiecdn.com/cwc.js"></script>
    <script id="cookieWow" type="text/javascript" src="https://cookiecdn.com/configs/WpwNGRUop2wsgyz4UypcmKHB" data-cwcid="WpwNGRUop2wsgyz4UypcmKHB"></script>

    <script>
        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye-slash fa-eye");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
</body>

</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<style>
    #JSFiddle {
        position: relative;
    }

    .dot {
        height: 7px;
        width: 7px;

        border-radius: 50%;
        display: inline-block;
    }

    .bg-green {
        background-color: #02E857;
    }

    .bg-red {
        background-color: red;
    }

    body {
        min-height: 100vh;
        min-height: -webkit-fill-available;
    }

    html {
        height: -webkit-fill-available;
    }

    .overlay {
        position: relative;
        display: block;

        height: 240px;
        text-align: center;
        vertical-align: middle;
        /* position: absolute; */
        top: 0px;
        /* chartArea top  */
        left: 0px;
        /* chartArea left */
        font-size: 80px;
        margin-top: 60px;
    }

    .overlay-label {

        width: 100%;
        display: block;

        height: 240px;
        text-align: center;
        vertical-align: middle;
        position: absolute;
        top: 0px;
        /* chartArea top  */
        left: 4px;
        /* chartArea left */
        margin-top: 75px;
        font-size: 20px;
    }




    @media only screen and (min-width: 0px) {
        .piechart_labelbottom {
            font-size: 12px;
            margin-bottom: 0px;
            color: black;
            font-weight: bold;
        }

        .circle_value {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 12px;
            color: black;
            font-weight: bold;
        }

        .circle_text {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 10px;
            color: black;
            font-weight: bold;
        }

        .card {
            border-radius: 15px !important;
        }

        /**card1 :  */
        .card_energyproduct {
            border-radius: 15px !important;
            background-image: linear-gradient(#8058E1, #8A12D9);
        }

        /**card2 :  */
        .card_energyproductinventer {
            border-radius: 15px;
            background-image: linear-gradient(#F4A032, #E77106);
        }

        /**card3 :  */
        .card_amountsaving {
            border-radius: 15px;
            background-image: linear-gradient(#E17E89, #D6136E);
        }


        .status_stationname_label {
            font-size: 16px;
            font-weight: bold;
            color: #1C1C1C;
        }

        .status_active_label {
            font-size: 1.5vh;
            color: #1C1C1C;
            /* line-height: 0; */
        }

        .form-check-input {
            height: 6px;
            bottom: 4px;
        }

        .status_powerproduced_label {
            font-size: 16px;
            font-weight: bold;
            color: #1C1C1C
        }

        .status_powerproducedtext_label {
            font-size: 1.5vh;
            color: #1C1C1C;
            /* line-height: 0; */
            margin-bottom: 0px;
        }



        .title_label {
            font-size: 2vh;
            color: white;
            text-align: center;
        }

        .content_label {
            font-size: 18px;
            color: white;
        }

        /**card4 :  */
        .card_report {
            height: 500px;
            border-radius: 15px;
            /* background-image: linear-gradient(#494DD4, #2B07BD); */
            color: black !important;
        }

        .titlereport_label {
            font-size: 1.5vh;
            font-weight: bold;
            color: black;
            text-align: center;
        }

        .contentreport_label {
            font-size: 18px;
            color: #62B0E9;
        }

        .contentreportsv_label {
            font-size: 18px;
            color: #F19291;
        }

    }

    @media only screen and (min-width: 468px) {

        .piechart_labelbottom {
            font-size: 12px;
            margin-bottom: 0px;
            color: black;
            font-weight: bold;
        }

        .card {
            border-radius: 15px !important;
        }

        /**card1 :  */
        .card_energyproduct {
            border-radius: 15px !important;
            background-image: linear-gradient(#8058E1, #8A12D9);
        }

        /**card2 :  */
        .card_energyproductinventer {
            border-radius: 15px;
            background-image: linear-gradient(#F4A032, #E77106);
        }

        /**card3 :  */
        .card_amountsaving {
            border-radius: 15px;
            background-image: linear-gradient(#E17E89, #D6136E);
        }

        .status_stationname_label {
            font-size: 16px;
            font-weight: bold;
            color: #1C1C1C;
        }

        .status_active_label {
            font-size: 2vh;
            color: #1C1C1C;
            /* line-height: 0; */
        }

        .form-check-input {
            height: 6px;
            bottom: 5px;
        }

        .status_powerproduced_label {
            font-size: 16px;
            font-weight: bold;
            color: #1C1C1C
        }

        .status_powerproducedtext_label {
            font-size: 2vh;
            color: #1C1C1C;
            /* line-height: 0; */
            margin-bottom: 0px;
        }



        .title_label {
            font-size: 2vh;
            color: white;
            text-align: center;
        }

        .content_label {
            font-size: 18px;
            color: white;
        }

        /**card4 :  */
        .card_report {
            height: 500px;
            border-radius: 15px;
            /* background-image: linear-gradient(#494DD4, #2B07BD); */
            color: black !important;
        }

        .titlereport_label {
            font-size: 2vh;
            font-weight: bold;
            color: black;
            text-align: center;
        }

        .contentreport_label {
            font-size: 18px;
            color: #62B0E9;
        }

        .contentreportsv_label {
            font-size: 18px;
            color: #F19291;
        }

        .circle_value {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 12px;
            color: black;
            font-weight: bold;
        }

        .circle_text {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 10px;
            color: black;
            font-weight: bold;
        }

    }

    @media only screen and (min-width: 767px) {

        .piechart_labelbottom {
            font-size: 12px;
            margin-bottom: 0px;
            color: black;
            font-weight: bold;
        }


        .circle_value {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 12px;
            color: black;
            font-weight: bold;
        }

        .circle_text {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 10px;
            color: black;
            font-weight: bold;
        }

        .card {
            border-radius: 15px !important;
        }

        /**card1 :  */
        .card_energyproduct {
            border-radius: 15px !important;
            background-image: linear-gradient(#8058E1, #8A12D9);
        }

        /**card2 :  */
        .card_energyproductinventer {
            border-radius: 15px;
            background-image: linear-gradient(#F4A032, #E77106);
        }

        /**card3 :  */
        .card_amountsaving {
            border-radius: 15px;
            background-image: linear-gradient(#E17E89, #D6136E);
        }

        .status_stationname_label {
            font-size: 18px;
            font-weight: bold;
            color: #1C1C1C;
        }

        .status_active_label {
            font-size: 1.6vh;
            color: #1C1C1C;
            /* line-height: 0; */
        }

        .form-check-input {
            height: 6px;
            bottom: 4px;
        }

        .status_powerproduced_label {
            font-size: 18px;
            font-weight: bold;
            color: #1C1C1C
        }

        .status_powerproducedtext_label {
            font-size: 1.6vh;
            color: #1C1C1C;
            /* line-height: 0; */
            margin-bottom: 0px;
        }



        .title_label {
            font-size: 2vh;
            color: white;
            text-align: center;
        }

        .content_label {
            font-size: 18px;
            color: white;
        }

        /**card4 :  */
        .card_report {
            height: 500px;
            border-radius: 15px;
            /* background-image: linear-gradient(#494DD4, #2B07BD); */
            color: black !important;
        }

        .titlereport_label {
            font-size: 2vh;
            font-weight: bold;
            color: black;
            text-align: center;
        }

        .contentreport_label {
            font-size: 18px;
            color: #62B0E9;
        }

        .contentreportsv_label {
            font-size: 18px;
            color: #F19291;
        }

        /** card 4  */
        .percent_inventer_label {
            font-size: 2.2vh;
            color: #1C1C1C;
        }

        .name_inventer_label {
            font-size: 16px;
            color: #1C1C1C;
        }
    }

    @media only screen and (min-width: 1024px) {

        .overlay-label {

            width: 100%;
            display: block;

            height: 240px;
            text-align: center;
            vertical-align: middle;
            position: absolute;
            top: 0px;
            /* chartArea top  */
            left: 4px;
            /* chartArea left */
            margin-top: 75px;
            font-size: 20px;
        }

        .card {
            border-radius: 15px !important;
        }

        /**card1 :  */
        .card_energyproduct {
            border-radius: 15px !important;
            background-image: linear-gradient(#8058E1, #8A12D9);
        }

        /**card2 :  */
        .card_energyproductinventer {
            border-radius: 15px;
            background-image: linear-gradient(#F4A032, #E77106);
        }

        /**card3 :  */
        .card_amountsaving {
            border-radius: 15px;
            background-image: linear-gradient(#E17E89, #D6136E);
        }

        .form-check-input {
            height: 6px;
            bottom: 4px;
        }

        .status_stationname_label {
            font-size: 16px;
            font-weight: bold;
            color: #1C1C1C;
        }

        .status_active_label {
            font-size: 12px;
            color: #1C1C1C;
            /* line-height: 0; */
        }



        .status_powerproduced_label {
            font-size: 16px;
            font-weight: bold;
            color: #1C1C1C
        }

        .status_powerproducedtext_label {
            font-size: 12px;
            color: #1C1C1C;
            /* line-height: 0; */
            margin-bottom: 0px;
        }



        .title_label {
            font-size: 12px;
            color: white;
            text-align: center;
        }

        .content_label {
            font-size: 18px;
            color: white;
        }

        /**card4 :  */
        .card_report {
            height: 500px;
            border-radius: 15px;
            /* background-image: linear-gradient(#494DD4, #2B07BD); */
            color: black !important;
        }

        .titlereport_label {
            font-size: 12px;
            font-weight: bold;
            color: black;
            text-align: center;
        }

        .contentreport_label {
            font-size: 18px;
            color: #62B0E9;
        }

        .contentreportsv_label {
            font-size: 18px;
            color: #F19291;
        }

        /** card 4  */
        .percent_inventer_label {
            font-size: 12px;
            color: #1C1C1C;
        }

        .name_inventer_label {
            font-size: 16px;
            color: #1C1C1C;
        }

        .piechart_labeltop {
            border-color: black;

            font-size: 18px;
            margin-bottom: 0px;
            bottom: 0px;
            font-weight: bold;
        }

        .piechart_labelbottom {
            font-size: 12px;
            margin-bottom: 0px;
            color: black;
            font-weight: bold;
        }





        .circle_value {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 18px;
            color: black;
            font-weight: bold;
        }

        .circle_text {
            margin-bottom: 0px;
            bottom: 0px;
            font-size: 12px;
            color: black;
            font-weight: bold;
        }
    }

    /* change "blue" browser chrome to yellow */
    /* input[type="radio"] {
      
        filter: invert(100%) hue-rotate(18deg) brightness(1.7);

    } */
    input[type="radio"]:checked+label {

        color: red;

    }
</style>



<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1><?php echo __('หน้าแรก') ?></h1>
            </div>
        </div>
    </div>

</div>

<div class="content mt-3">


    <?php

    $summarytoday_inverter_producted_text = $summarytoday_inverter_producted > 0 ? number_format($summarytoday_inverter_producted, 2, '.', ',') : 0;

    $summarytoday_save_producted_text = $summarytoday_save_producted > 0 ? number_format($summarytoday_save_producted, 2, '.', ',') : 0;


    $summarytoday_inverter_producted_kwh_text = $summarytoday_inverter_producted_kwh > 0 ? number_format($summarytoday_inverter_producted_kwh, 2, '.', ',') : 0;
    ?>
    <!-- <div class="col-lg-3 col-md-12 col-sm-12">

        <a href="javascript:void(0)">
            <div class="card">
                <div class="card-body card_energyproduct" align="center">
                    <div class="stat-widget-one">
                        <div class="">
                            <div class="stat-text ">
                                <p class="title_label mb-0"><?php echo __('พลังงานที่ผลิตวันนี้') ?></p>
                            </div>
                            <div class="stat-text ">
                                <p class="title_label"><?php echo __('รวมทุกโครงการ (Kwh)') ?></p>
                            </div>

                            <div class="stat-digit " id="wating_for_payment">
                                <p class="content_label"><?php echo $summarytoday_inverter_producted_kwh_text; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div> -->
    <div class="col-lg-4 col-md-12 col-sm-12">

        <a href="javascript:void(0)">
            <div class="card">
                <div class="card-body card_energyproductinventer" align="center">
                    <div class="stat-widget-one">
                        <div class="">
                            <div class="stat-text ">
                                <p class="title_label mb-0"><?php echo __('พลังงานที่ผลิตได้จากอินเวอร์เตอร์') ?></p>
                            </div>
                            <div class="stat-text ">
                                <p class="title_label"><?php echo __('รวมทุกโครงการ (Kw)') ?></p>
                            </div>

                            <div class="stat-digit " id="wating_for_payment">
                                <p class="content_label"><?php echo number_format($summaryalltime_inverter_producted, 2, '.', ','); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12">

        <a href="javascript:void(0)">
            <div class="card">
                <div class="card-body card_amountsaving" align="center">
                    <div class="stat-widget-one">
                        <div class="">
                            <div class="stat-text ">
                                <p class="title_label mb-0"><?php echo __('ผลตอบแทน') ?></p>
                            </div>
                            <div class="stat-text ">
                                <p class="title_label"><?php echo __('รวมทุกโครงการ (บาท)') ?></p>
                            </div>

                            <div class="stat-digit " id="wating_for_payment">
                                <p class="content_label"><?php echo number_format($summarytoday_businessresult, 2, '.', ','); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12">

        <a href="javascript:void(0)">
            <div class="card">
                <div class="card-body card_energyproduct" align="center">
                    <div class="stat-widget-one">
                        <div class="">
                            <div class="stat-text ">
                                <p class="title_label mb-0"><?php echo __('ลดการปล่อยคาร์บอน') ?></p>
                            </div>
                            <div class="stat-text ">
                                <p class="title_label"><?php echo __('รวมทุกโครงการ (KgCo2)') ?></p>
                            </div>

                            <div class="stat-digit " id="wating_for_payment">
                                <p class="content_label"><?php echo number_format($summaryalltime_kgco2_producted, 2, '.', ','); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

</div><!-- .content -->

<div class="content mt-3">
    <!-- <div class="col-lg-4 col-md-12">


        <div class="card">
            <div class="card-body card_report  ">
                <div class="stat-widget-one">
                    <div class="col-lg-12 col-md-12 pl-0 pr-0">

                        <div class="col-lg-12 col-md-12 pl-0 pr-0">
                            <p class="titlereport_label mb-0"><?php echo __('พลังงานที่ผลิตได้และใช้รวมทุกโครงการ(วันนี้) ') ?></p>
                        </div>
                        <br><br>

                        <div class="col-lg-12 col-md-12 pl-0 pr-0">
                            <div class="col-lg-6 col-md-6 pl-0 pr-l" align="center">

                                <p class="contentreport_label"><?php echo $summarytoday_inverter_producted_kwh_text; ?></p>
                                <p class="titlereport_label"><?php echo __('พลังงานที่ผลิตได้(Kwh)') ?></p>




                            </div>
                            <div class="col-lg-6 col-md-6 pl-0 pr-0" align="center">
                                <p class="contentreportsv_label"><?php echo $summarytoday_save_producted_text; ?></p>
                                <p class="titlereport_label"><?php echo __('ประหยัดได้(บาท)') ?></p>
                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 pr-0 pl-0">
                            <div id="curve_chart" style="width: 100%; min-height: 250px;"></div>
                          
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div> -->
    <div class="col-lg-6 col-md-12">
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td,
            th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>

        <div class="card">
            <div class="card-body card_report  ">
                <div class="stat-widget-one">
                    <div class="">
                        <div class="stat-text ">
                            <div class="container">
                                <div class="row">
                                    <div class="col pl-0 pr-0">
                                        <p class="titlereport_label mb-0" style="float:left;"><?php echo __('จำนวนเครื่องในระบบทั้งหมด') ?>&nbsp;<u><?php echo number_format($all_devices, 0, '.', ','); ?></u>&nbsp;เครื่อง</p>
                                    </div>
                                    <!-- <div class="col pl-0 pr-0">
                                        <a href="<?php echo base_url('Projectstatus') ?>" class="titlereport_label mb-0" style="float:right;color:#66C4ED;"><u><?php echo __('เพิ่มเติม') ?></u></a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 pl-0 pr-0">
                                        <p class="status_powerproducedtext_label mb-0" style="font-weight:bold;">ลงทะเบียนแล้ว&nbsp;<u><?php echo number_format($count_devicesregister, 0, '.', ','); ?></u>&nbsp;เครื่อง</p>
                                        <br>
                                        <p class="status_powerproducedtext_label mb-0" style="font-weight:bold;"><u>ตารางแสดงจำนวนอุปกรณ์ตามประเภทอินเวอร์เตอร์</u></p>
                                      
                                        <table style="width:100%;">
                                            <tr>
                                                <th>รายละเอียด</th>
                                                <th>จำนวนทั้งหมด</th>
                                                <th>เปิดใช้งาน</th>
                                            </tr>
                                            <?php foreach ($devices_register as $key => $value) { ?>
                                                <tr>
                                                    <th><?php echo $key ?></th>
                                                    <th><?php echo $value['all_devices'] ?></th>
                                                    <th><?php echo $value['online'] ?></th>
                                                </tr>
                                            <?php } ?>

                                        </table>

                                    </div>
                                    <!-- <p class="status_powerproducedtext_label mb-0" style="font-weight:bold;color:red;">จำนวนอุปกรณ์ตามประเภทอินเวอร์เตอร์อ้างอิงจาก</p> -->
                                    <?php

                                    //for ($i = 0; $i < 4; $i++) {
                                    ?>
                                    <!-- <div class="col-6 pl-0 pr-0">
                                            <p class="status_stationname_label mb-0"><?php echo $planlist[$i]->name ?></p>
                                            <p class="status_powerproducedtext_label mb-0" style="font-weight:bold;"><?php echo DATE("Y/m/d H:i:s", $planlist[$i]->lastUpdateTime); ?></p>
                                          

                                        </div>
                                        <div class="col-6 pl-0 pr-0">
                                            <?php
                                            $plan_generationPower = $planlist[$i]->plan_generationPower > 0 ? number_format($planlist[$i]->plan_generationPower, 0, '.', ',') : 0;
                                            ?>
                                            <p class="status_powerproduced_label mb-0"><?php echo $plan_generationPower; ?></p>
                                            <p class="status_powerproducedtext_label">พลังงงานที่ผลิตได้ (w)</p>
                                        </div>

                                        <div class="col-12">
                                            <hr>
                                        </div> -->

                                    <?php
                                    //}
                                    ?>


                                </div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-lg-6 col-md-12">


        <div class="card ">
            <div class="card-body card_report ">
                <div class="stat-widget-one">
                    <div class="">
                        <div class="stat-text ">
                            <p class="titlereport_label mb-0"><?php echo __('แสดงจำนวนอินเวอเตอร์ที่ติดตั้งทุกโครงการ') ?></p>
                        </div>





                    </div>
                    <div class="container">
                        <div class="form-group">
                            <div class="row" id="piechart_legend"></div>
                        </div>
                    </div>
                    <div id="JSFiddle">
                        <div id="donutchart" style=""></div>
                        <div id="cnt" class="overlay"></div>
                        <div class="overlay-label" id="total_components">
                            <p class="circle_value" style="">
                                <?php echo $summary_inverter_countvalue; ?>
                            </p>
                            <p class="circle_text" style="">
                                จำนวนทั้งหมด
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var summary_inverter = <?= empty($summary_inverter) ?  0 :  json_encode($summary_inverter); ?>;
    var twentyfourhr_linechart = <?= empty($twentyfourhr_linechart) ?  0 :  json_encode($twentyfourhr_linechart); ?>;
    var grid_invertermodel = <?= empty($grid_invertermodel) ? 0 : json_encode($grid_invertermodel); ?>;
    var year = <?php echo empty($year) ? DATE("Y") : $year; ?>;
    var month = <?php echo empty($month) ? intval(DATE("m")) : $month; ?>;
    var day = <?php echo empty($day) ? intval(DATE("d")) : $day; ?>;
</script>
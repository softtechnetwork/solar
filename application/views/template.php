<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PSI - SOLAR</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <?php echo $this->template->stylesheet; ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
            font-family: Kanit !important;
            height: 100%;
        }

        html {
            height: 100%;
        }

        a.btn:hover {
            background: #87CEEB;
        }

        .btn-success:hover,
        .btn-success:focus,
        .btn-success:active,
        .btn-success.active,
        .open {
            background: #87CEEB !important;
        }

        @media only screen and (min-width: 312px) {

            .header-right{
                display: none !important;
            }
            #menu__toggle:checked ~ .menu__btn{
                z-index: 100000;
            }
            .menu__btn {

                position: fixed;
                top: 10px;
                left: 20px;
                width: 26px;
                height: 25px;
                cursor: pointer;
                
            }

            /* styles for browsers larger than 768px; */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 30px;
            }

        }
.activeclass{
color: #19C3D8 !important;
}
        @media only screen and (min-width: 578px) {

            .menu__btn {

                position: fixed;
                top: 15px;
                left: 20px;
                width: 26px;
                height: 25px;
                cursor: pointer;
                
            }

            /* styles for browsers larger than 768px; */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 30px;
            }
            .header-right{
                display: none !important;
            }

        }

        @media only screen and (min-width: 960px) {


            .menu__btn {

                position: fixed;
                top: 15px;
                left: 20px;
                width: 26px;
                height: 25px;
                cursor: pointer;
                
            }

            .header-right{
                display: block !important;
            }

            /* styles for browsers larger than 960px; */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 0px;
            }

        }

        @media only screen and (min-width: 1440px) {
            .header-right{
                display: block !important;
            }

            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 0px;
            }
        }

        @media only screen and (min-width: 2000px) {
            .header-right{
                display: block !important;
            }


            /* for sumo sized (mac) screens */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 0px;
            }

        }

        @media only screen and (max-device-width: 480px) {
            

            /* styles for mobile browsers smaller than 480px; (iPhone) */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 30px;
            }

            .header-right{
                display: none !important;
            }

        }

        @media only screen and (device-width: 768px) {

            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 0px;
            }

            .header-right{
                display: block !important;
            }

        }

        /* different techniques for iPad screening */
        @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) {

            /* For portrait layouts only */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 0px;
            }

            .header-right{
                display: block !important;
            }

        }

        @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {

            /* For landscape layouts only */
            .logo_class {
                margin-left: 30px;
            }

            .breadcrumbs_responsive {
                margin-left: 30px;
            }

            .header-right{
                display: none !important;
            }

        }
    </style>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JZQ9EK9WY3"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'G-JZQ9EK9WY3');
</script>


<body class="<?php echo @$this->session->userdata('menu_open') ?>" style="">
    <input type="hidden" name="base_url" value="<?php echo base_url() ?>">
    <?php
    // $this->load->view('template_left_menu');

    ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel" style="">

        <!-- Header-->
        <header id="header" class="header pt-0 pb-0 pl-0 pr-0">

            <div class="header-menu">

                <?php

                ?>

                <div class="col-sm-5">
                    <?php
                    $this->load->view('template_nav_menu');
                    ?>
                    <div class="user-area dropdown float-left logo_class">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" style="width:150px !important;" src="<?php echo base_url('assets/images/logo_200x200.png'); ?>" alt="">

                        </a>

                    </div>



                </div>


                <div class="col-sm-7">
                    <!-- <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a> -->
                    <div class="header-right">
        
                        <div class="breadcrumbs breadcrumbs_responsive" style="   display: flex;
                        justify-content: center;
                        align-items: center;">

                            <div class="col-sm-10">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <?php
                                       
                                        if ($this->controller == 'Main' || $this->controller == 'Compare') {
                                        ?>
                                            <ol class="breadcrumb text-right">
                                                <li class=""><a href="<?php echo base_url("Main") ?>"  class="<?php echo $this->controller == 'Main' ? "activeclass" : "" ?>"><?php echo __('หน้าแรก', 'default') ?></a></li>
                                                <li ><a href="<?php echo base_url('Compare') ?>" class="<?php echo $this->controller == "Compare" ? "activeclass" : "" ?> "><?php echo __('เปรียบเทียบข้อมูล') ?></a></li>
                                            </ol>
                                        <?php
                                        }
                                        ?>
                                        


                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 pl-0 mr-0">
                                <div class="page-header" style="min-height:unset;margin:unset;padding:unset;width:25px !important;">
                                    <a title="ออกจากระบบ" style="cursor:pointer;" onclick="showmodal()">
                                        <img class="user-avatar rounded-circle" style="width:150px !important;" src="<?php echo base_url('assets/images/logout_icon.png'); ?>" alt="">

                                    </a>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <?php echo $this->template->content; ?>


    </div><!-- /#right-panel -->
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" style="z-index:99999999999999;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo "แจ้งเตือน"; ?>?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <!-- <button type="button" class=" btn btn-success col-2 button_main float-right " data-dismiss="modal">ปิด</button> -->
                    </button>
                </div>
                <div class="modal-body"><?php echo "ยืนยันที่จะออกจากระบบหรือไม่" ?></div>
                <div class="modal-footer">
                    <button class="btn btn-secondary col-2 button_main " style="background-color:alice-blue;" type="button" data-dismiss="modal"><?php echo __('ยกเลิก', 'backend/default') ?></button>
                    <a class="btn btn-success col-2 button_main " style="background-color:#DD6B55;" href="<?php echo base_url('signin/signout') ?>"><?php echo __('ยืนยัน', 'backend/default') ?></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Right Panel -->


    <?php echo $this->template->javascript; ?>

    <script>
        $('#exampleModal').on('hidden.bs.modal', function() {
            // do something…
            $('.site_div_class_active').css('opacity', '1');
        })

        function showmodal() {

            $('.site_div_class_active').css('opacity', '0.2');
            $("#exampleModal").modal("show");
        }
        
    </script>
</body>

</html>
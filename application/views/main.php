<style>

table.dataTable thead th {
    border-top: 0px;
}
    @media only screen and (min-width: 960px) {

        /* styles for browsers larger than 960px; */
        .panelsite_responsive{
            height:90vh;
        }
        .site_panel_ovf {
            height: 70vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        }
    }

    @media only screen and (min-width: 1440px) {
        .panelsite_responsive{
            height:90vh;
        }
        /* .site_panel_ovf {
            height: 70vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        } */
    }

    @media only screen and (min-width: 2000px) {
        .panelsite_responsive{
            height:90vh;
        }
        /* for sumo sized (mac) screens */
        /* .site_panel_ovf {
            height: 50vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        } */
    }

    @media only screen and (max-device-width: 480px) {
        
        /* styles for mobile browsers smaller than 480px; (iPhone) */
        .panelsite_responsive{
            height:50vh;
        }
        /* .site_panel_ovf {
            height: 40vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        } */
    }

    @media only screen and (device-width: 768px) {
        .panelsite_responsive{
            height:90vh;
        }
        /* .site_panel_ovf {
            height: 40vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        } */
    }

    /* different techniques for iPad screening */
    @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) {
       
        /* For portrait layouts only */
        .panelsite_responsive{
            height:50vh;
        }
        /* .site_panel_ovf {
            height: 60vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        } */
    }

    @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {
        .panelsite_responsive{
            height:90vh;
        }
        /* For landscape layouts only */
        /* .site_panel_ovf {
            height: 60vh !important;
            overflow-y: auto;
            overflow-x: hidden;
        } */
    }
</style>


<div class="container-fluid w-100" id="bigContain" style="height:90%">
    <!-- <div class="lds-roller" style="display:block !important;"></div> -->
    <div class="loader" style="display:none;"></div>
    <div class="row" style="min-height:90vh;">

        <div class="panel_site col-sm-12 col-md-12 col-lg-3 pl-0 pr-0  panelsite_responsive " style="">
            <!-- div : header text -->
            <div class="container ml-2 mr-2 " style="height:8%;">
                <div class="row">
                    <div class="">
                        <p class="" style="padding-top:15px;">
                            <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                        <p>
                    </div>
                    <div class="col-10 pr-0">
                        <p class="text_headertitle" style="padding-top:16px;">ไซต์งาน</p>
                    </div>

                </div>
            </div>
            <!-- div : end header text -->
            <!-- div : search text -->
            <div class="container ml-2 mr-2 mt-1 mb-1"  style="height:10%;">

                <?php echo form_input(array(
                    'type' => 'text',
                    'id'   => 'search_input',

                    'placeholder' => 'พิมพ์เพื่อค้นหา',
                    'class' => 'form-control text_color_blue only_bottom_border ',
                    'style' => 'background-color: white !important;'
                )) ?>
                <span class="fa fa-search searchspan"></span>
            </div>

            <!-- div : end search text -->

            <div id="site_div" class="site_div_class " style="height:80%;overflow-x:hidden;overflow-y:auto;">
                <?php
                if (!empty($customer_rel_plan)) {
                    foreach ($customer_rel_plan as $key => $value) {
                ?>
                        <div class="size_tag_class  " style="cursor:pointer;" id="tag_<?php echo $value->stationId; ?>" onclick="set_active_class(<?php echo $value->stationId; ?>)">
                            <p class="text_tag text_color_blue text_font_subject mb-1 pl-4 ml-2 mr-2" id="textsubjecttag_<?php echo $value->stationId; ?>"><?php echo $value->name_solarapp; ?></p>
                            <p class="text_tag text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="textdigittag_<?php echo $value->stationId; ?>"><span id="generationPower_<?php echo $value->stationId; ?>"><?php echo empty($value->generationPower) ? 0 : number_format($value->generationPower, 0, '.', ','); ?></span>&nbsp;<span id="" class="text_tag_wat text_font_subject ">วัตต์</span></p>
                            <hr class="mt-0 mb-0">
                        </div>
                        <!-- <div class="ar" id="ar_<?php echo $value->stationId; ?>"></div> -->

                    <?php }
                } else { ?>
                    <p class="text_tag  text_font_subject mb-1 pl-4 ml-2 mr-2">ไม่พบข้อมูลไซต์งาน</p>
                <?php } ?>
            </div>
        </div>
        <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCtr0HTJtlo-P2G3XciW14UPih6tThkty0&callback=initialize_map&libraries=geometry,places&time=<?php echo strtotime(date('Y-m-d H:i:s')) ?>">
        </script>

        <div class="panel_description col-sm-12 col-md-12 col-lg-4 box  ">
            <!-- div : header text -->
            <div class="container" style="height:10%;">
                <div class="row">
                    <div class="">
                        <p class="" style="padding-top:15px;">
                            <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;cursor:default !important;" class="text_color_center text_font_header"></a>
                        <p>
                    </div>
                    <div class="col-10 pr-0">
                        <p class="text_headertitle" style="padding-top:16px;margin-bottom:8px;">รายละเอียดไซต์งาน <a href="#" onclick="showdiagram()" style="margin-bottom:8px;margin-right:-10%;" class="btn btn-success col-6 button_main float-right ">ดูแผนผัง</a></p>



                    </div>

                </div>
            </div>
            <div class="container mt-2 mb-1" style="height:42%;">
                <div id="map-canvas" style="border:1px solid;width:100%;height:100%; ">

                </div>
            </div>
            <!-- div : end header text -->




            <div id="description_div" class="description_class" style="height:45%;overflow-x:hidden;overflow-y:auto;">
                <div class="container">
                    <div class="row">
                        <div class="col-2 pr-0">
                            <p class="">
                                <a href="#"> <img src="<?php echo base_url('assets/images/solarcell_icon.png') ?>" style="margin-top:5px;width:50px;height:50px;cursor:default !important;" class="text_color_center text_font_header"></a>
                            <p>
                        </div>
                        <div class="col-10 pr-0" align="right">
                            <div class="  ">
                                <p class="text_tag  text_font_subject mb-1 pl-4 ml-2 mr-2 mt-2">โซล่าเซลล์ให้พลังงานรวมทั้งหมด</p>
                                <p class="text_tag text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="text_solar">-</p>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-0 mb-0">
                </div>
                <div class="  ">
                    <div class="container">
                        <div class="row">
                            <div class="col-2 pr-0">
                                <p class="">
                                    <a href="#"> <img src="<?php echo base_url('assets/images/inverter_icon.png') ?>" style="margin-top:5px;width:50px;height:50px;cursor:default !important;" class="text_color_center text_font_header"></a>
                                <p>
                            </div>
                            <div class="col-10 pr-0" align="right">

                                <p class="text_tag  text_font_subject mb-1 pl-4 ml-2 mr-2 mt-2">ตัวผลิตไฟฟ้าไฮบริดอินเวอเตอร์</p>
                                <p class="text_tag text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="text_hybrid">-</p>

                            </div>
                        </div>
                    </div>
                    <hr class="mt-0 mb-0">
                </div>
                <div class="  ">
                    <div class="container">
                        <div class="row">
                            <div class="col-2 pr-0">
                                <p class="">
                                    <a href="#"> <img src="<?php echo base_url('assets/images/meter_icon.png') ?>" style="margin-top:5px;width:50px;height:50px;cursor:default !important;" class="text_color_center text_font_header"></a>
                                <p>
                            </div>
                            <div class="col-10 pr-0" align="right">
                                <p class="text_tag  text_font_subject mb-1 pl-4 ml-2 mr-2 mt-2">ประเภทมิเตอร์</p>
                                <p class="text_tag text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="text_typebusiness">-</p>

                            </div>
                        </div>
                    </div>
                    <hr class="mt-0 mb-0">
                </div>


                <div class="  ">
                    <div class="container">
                        <div class="row">
                            <div class="col-2 pr-0">
                                <p class="">
                                    <a href="#"> <img src="<?php echo base_url('assets/images/calendar_icon.png') ?>" style="margin-top:5px;width:50px;height:50px;cursor:default !important;" class="text_color_center text_font_header"></a>
                                <p>
                            </div>
                            <div class="col-10 pr-0" align="right">

                                <p class="text_tag  text_font_subject mb-1 pl-4 ml-2 mr-2 mt-2">วันเริ่มต้นใช้งาน</p>
                                <p class="text_tag text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="text_datecreate">-</p>

                            </div>
                        </div>
                    </div>

                </div>



            </div>

        </div>
        <div class="panel_energyused col-sm-12 col-md-12 col-lg-5  ">
            <!-- div : header text -->
            <div class="container">
                <div class="row">
                    <div class="">
                        <p class="" style="padding-top:15px;">
                            <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                        <p>
                    </div>
                    <div class="col-10 pr-0">
                        <p class="text_headertitle" style="padding-top:16px;">พลังงานที่ผลิตได้</p>
                    </div>

                </div>
            </div>
            <!-- div : end header text -->
            <!-- div : radio button -->
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="radio_container  text_color_blue p-0">
                            <input type="radio" name="radio_period" value="month" style="cursor:pointer;" id="radio_month" checked>
                            <label for="radio_month" style="cursor:pointer;" class="mb-0">เดือน</label>
                            <input type="radio" name="radio_period" value="year" style="cursor:pointer;" id="radio_year">
                            <label for="radio_year" style="cursor:pointer;" class="mb-0">ปี</label>

                        </div>
                    </div>

                    <div class="col">
                        <input name="duration_month" id="duration_month" style="height:30px;cursor:pointer;" class="form-control button_main float-right " value="<?php echo DATE("m") . '/' . DATE("Y"); ?>">
                        <span class="fa fa-angle-down angledown" onclick="showdatepicker()" id="angle_down_month"></span>

                        <select onchange="onchange_draw_data_year()" name="duration_year" id="duration_year" class="dropdown_year dropdown_year_select float-right" style="" data-live-search="true" title="Please select">
                            <?php
                            $this_year =  intval(DATE("Y"));
                            for ($i = $this_year; $i >= 2010; $i--) {
                                $selected = "";
                                if (isset($startYear)) {
                                    if ($startYear == $i) {
                                        $selected = "selected";
                                    }
                                }
                            ?>
                                <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                        <span class="fa fa-angle-down angledown" id="angle_down_year" style="margin-right:-65px;pointer-events: none;"></span>


                    </div>
                </div>
            </div>




            <!-- div : end radio button -->

            <a href="#" onclick="exportdata()" style="margin-bottom:8px;margin-right:15px;" class="mt-2 btn btn-success col-6 button_main float-right ">นำข้อมูลออก</a>

            <!-- div : barchart -->
            <div class="form-group ">
                <div class="col-md-12 col-sm-12">
                    <div id="chart_div" style="cursor:pointer;" title="คลิ๊กเพื่อดูเต็มจอ" onclick="showfullscreen()"></div>
                </div>
            </div>
            <!-- div : end barchart -->


            <!-- div : table -->

            <div class="col-md-12 col-sm-12" style="margin-top:15px;">
                <!-- <div class="table-responsive"> -->
                <table style="width:100%;height:100% !important;" class="table pageResize" id="tablestructure">
                    <thead>
                        <tr>

                            <th style="text-align:left;" class=" text_font_headertable"><?php echo __("วันที่") ?></th>
                            <th style="text-align:center;" class=" text_font_headertable"><?php echo "พลังงานที่ผลิตได้ (kWh)"; ?></th>

                            <th style="" class=" text_font_headertable"><?php echo __('ประหยัดได้(บาท)') ?></th>

                        </tr>
                    </thead>
                    <tbody id="body_tbl">
                        <?php
                        date_default_timezone_set("Asia/Bangkok");
                        ?>



                    </tbody>
                </table>
                <!-- </div> -->
                <!-- div : end table -->
            </div>
        </div>

        <div id="modal_googlechart" class="modal fade w-80" role="dialog" style="z-index:9999999;">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header m-0 p-2 pt-2">
                        <h4 class="modal-title text_headertitle">
                            <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                            รายงานพลังงานที่ผลิตได้
                        </h4>
                        <button type="button" class=" btn btn-success col-2 button_main float-right " data-dismiss="modal">ปิด</button>

                    </div>
                    <div class="modal-body p-2" id="modal_chartbody">
                        <div id="chart_div_modal" width="350"></div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>


        <div id="modal_exportdata" class="modal fade w-80" role="dialog" style="z-index:9999999;">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header m-0 p-2 pt-2">
                        <div class="container col-12">
                            <div class="row">

                                <div class="col-12 ">
                                    <button type="button" class=" btn btn-success button_main float-right " data-dismiss="modal">ปิด</button>


                                </div>
                                <div class="col-12">
                                    <h4 class="modal-title text_headertitle" style="text-align:center;font-size:13px;">

                                        เลือกประเภทและเวลาที่ต้องการนำข้อมูลออก
                                    </h4>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-body p-2" id="modal_body_exportdata">
                        <div class="container-fluid w-100" style="height:90%">
                            <!-- <div class="lds-roller" style="display:block !important;"></div> -->

                            <div class="row">
                                <div class="panel_site_modal col-sm-12 col-md-12 col-lg-6 pl-0 pr-0  ">
                                    <!-- div : header text -->
                                    <div class="container ml-2 mr-2">
                                        <div class="row">
                                            <div class="">
                                                <p class="" style="padding-top:15px;">
                                                    <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                                                <p>
                                            </div>
                                            <div class="col-10 pr-0">
                                                <p class="text_headertitle" style="padding-top:16px;font-size:13px;">เลือกไซต์งาน</p>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- div : end header text -->
                                    <!-- div : search text -->
                                    <div class="form-group ml-2 mr-2">

                                        <?php echo form_input(array(
                                            'type' => 'text',
                                            'id'   => 'search_input_modal',

                                            'placeholder' => 'พิมพ์เพื่อค้นหา',
                                            'class' => 'form-control text_color_blue only_bottom_border ',
                                            'style' => 'background-color: white !important;'
                                        )) ?>
                                        <span class="fa fa-search searchspan"></span>
                                    </div>

                                    <!-- div : end search text -->

                                    <div id="site_modaldiv" class="site_modaldiv_class" style="">
                                        <?php
                                        foreach ($customer_rel_plan as $key => $value) {
                                        ?>
                                            <div class="size_modaltag_class  mt-1" onclick="set_active_checkbox(<?php echo $value->stationId; ?>)" style="cursor:pointer;" id="modaltag_<?php echo $value->stationId; ?>" for="checkbox_<?php echo $value->stationId; ?>">
                                                <p class="text_tag text_color_blue text_font_subject mb-1 pl-2 ml-2 mr-2" style="font-size:12px;" id="textsubjectmodaltag_<?php echo $value->stationId; ?>"><?php echo $value->name_solarapp; ?>
                                                    <span class="round  float-right">
                                                        <input type="checkbox" name="checkbox_modal" value="<?php echo $value->stationId; ?>" class="checkbox_modal" id="checkbox_<?php echo $value->stationId; ?>" />
                                                        <label for="checkbox_<?php echo $value->stationId; ?>"></label>
                                                    </span>
                                                </p>
                                                <p class="text_tag text_color_blue text_font_digit pl-2 ml-2 mr-2 mb-2" id="textdigitmodaltag_<?php echo $value->stationId; ?>"><span id="modalgenerationPower_<?php echo $value->stationId; ?>"><?php echo empty($value->generationPower) ? 0 : number_format($value->generationPower, 0, '.', ','); ?></span>&nbsp;<span id="" class="text_tag_wat text_font_subject ">วัตต์</span></p>

                                                <hr class="mt-0 mb-0">
                                            </div>


                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="panel_export_modal col-sm-12 col-md-12 col-lg-6 pl-0 pr-0  ">
                                    <div class="container ml-2 mr-2">
                                        <div class="row">
                                            <div class="">
                                                <p class="" style="padding-top:15px;">
                                                    <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                                                <p>
                                            </div>
                                            <div class="col-10 pr-0">
                                                <p class="text_headertitle" id="text_modal_period" style="font-size:13px;padding-top:16px;">เลือกเดือนที่ต้องนำข้อมูลออก</p>
                                            </div>

                                            <div class="container" align="center">



                                                <div class="col-md-12">
                                                    <input name="duration_month_modal" id="duration_month_modal" style="height:30px;cursor:pointer;" class="form-control button_main" value="<?php echo DATE("m") . '/' . DATE("Y"); ?>">
                                                    <span id="angle_down_month_modal" class="fa fa-angle-down angledownmodal" onclick="showmodaldatepicker()"></span>

                                                    <select name="duration_year_modal" id="duration_year_modal" class="dropdown_year" style="" " class=" form-control button_main" data-live-search="true" title="Please select">
                                                        <?php
                                                        $this_year =  intval(DATE("Y"));
                                                        for ($i = $this_year; $i >= 2010; $i--) {
                                                            $selected = "";
                                                            if (isset($startYear)) {
                                                                if ($startYear == $i) {
                                                                    $selected = "selected";
                                                                }
                                                            }
                                                        ?>
                                                            <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <span class="fa fa-angle-down angledown" id="angle_down_year_modal" style="margin-left:-23px;position:absolute;pointer-events: none;"></span>




                                                </div>


                                                <div class="col-md-12">
                                                    <a href="#" onclick="onaction_exportdata()" style="margin-bottom:8px;" class="mt-2 btn btn-success col-6 button_main ">นำข้อมูลออก</a>

                                                </div>
                                            </div>




                                            <!-- div : end radio button -->



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">

                    </div> -->
                </div>

            </div>
        </div>



        <div id="modal_main" class="modal fade w-80 " role="dialog" style="z-index:9999999;">
            <div class="modal-dialog modal-sm" style="max-width:450px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header m-0 p-2 pt-2">
                        <!-- div : container header title -->
                        <div class="container col-12">
                            <div class="row">
                                <div class="col-8">
                                    <h4 class="modal-title text_headertitle">
                                        <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                                        แผนผังแสดงการทำงาน
                                    </h4>

                                </div>
                                <div class="col-4 ">
                                    <button type="button" class=" btn btn-success button_main float-right " data-dismiss="modal">ปิด</button>


                                </div>

                            </div>

                            <div class="col-12 mt-0" style="text-align: center;">
                                <span id="" class="text_font_subject">โครงการนี้ทำงานตั้งแต่วันที่</span>&nbsp;<span class="text_font_subject" id="diagramtext_datecreate">-</span>

                            </div>
                            <div class="col-12 mt-0" style="text-align: center;">
                                <span class="text_font_subject">อัปเดตล่าสุด</span>&nbsp;<span class="text_font_subject" id="diagramtext_dateupdate">-</span>
                            </div>
                        </div>



                    </div>



                    <div class="modal-body" id="modal_diagrambody" style="text-align:center;">
                        <img style="width:65%;" src="<?php echo base_url('assets/images/animate_edit.gif') ?>">

                        <!-- text : พลังงานผลิตได้ -->
                        <p id="" class=" text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="" style="
                            display: flex;
                            position: fixed;
                            margin-top: -15%;
                            left: 20%
                        "><span id="diagramtext_generationpower">0</span>
                            &nbsp;<span id="" class=" text_font_subject" style="
    padding-top: 1px;
">วัตต์</span></p>
                        <span class=" text_font_subject  pl-4 ml-2 mr-2 mb-2" id="" style="
                            display: flex;
                            position: fixed;
                            margin-top: -10%;
                            left: 15%
                        ">พลังงานที่ผลิตได้</span>
                        <!-- end text : พลังงานผลิตได้ -->
                        <p id="" class=" text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-2" id="" style="
                            display: flex;
                            position: fixed;
                            margin-top: -15%;
                            right: 20%
                        "><span id="text_reverse_pw">0</span>
                            &nbsp;<span id="" class=" text_font_subject" style="
    padding-top: 1px;
">วัตต์</span></p>
                        <span class=" text_font_subject  pl-4 ml-2 mr-2 mb-2" id="" style="
                            display: flex;
                            position: fixed;
                            margin-top: -10%;
                            right: 16%
                        ">ไฟฟ้าย้อนกลับ</span>


                    </div>
                    <div class="modal-footer p-2">
                        <!-- พลังงานที่ผลิตวันนี้ -->
                        <div class="container col-12">
                            <div class="col-12" style="text-align: center;">
                                <p class=" text_font_subject  pl-4 ml-2 mr-2 mb-0" id="" style="">พลังงานที่ผลิตวันนี้</p>
                            </div>
                            <div class="col-12" style="text-align: center;">
                                <p class=" text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-0" id="" style=""><span id="today_generationpower">0</span>
                                    &nbsp;<span id="" class="text_font_subject">กิโลวัตต์</span>
                            </div>

                            <!-- <div class="row"> -->
                            <div class="col-6" style="text-align: center;">
                                <p class=" text_font_subject  pl-4 ml-2 mr-2 mb-0" id="" style="">ผลตอบแทนวันนี้</p>
                                <p class=" text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-0" id="" style=""><span id="today_revenue">0</span>
                                    &nbsp;<span id="" class="text_font_subject">บาท</span>

                            </div>
                            <div class="col-6" style="text-align: center;">
                                <p class=" text_font_subject  pl-4 ml-2 mr-2 mb-0" id="" style="">ผลตอบแทนเดือนนี้</p>
                                <p class=" text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-0" id="" style=""><span id="month_revenue">0</span>
                                    &nbsp;<span id="" class="text_font_subject">บาท</span>
                            </div>
                            <!-- </div> -->

                            <div class="col-12" style="text-align: center;">
                                <p class=" text_font_subject  pl-4 ml-2 mr-2 mb-0" id="" style="">โครงการนี้ลดการผลิตคาร์บอนได้</p>
                                <p class=" text_color_blue text_font_digit pl-4 ml-2 mr-2 mb-0" id="" style=""><span id="kgco">0</span>
                                    &nbsp;<span id="" class="text_font_subject">กิโลกรัมคาร์บอน</span>

                            </div>
                        </div>
                        <!-- end  พลังงานที่ผลิตวันนี้ -->



                    </div>
                </div>

            </div>
        </div>


        <input type="hidden" id="active_stationId" />

        <script tyle="text/javascript">
            var planlist_data = <?php echo json_encode($googlemap) ?>;
            var customer_rel_plan = <?php echo json_encode($customer_rel_plan) ?>;
            var dtbarchart_plan = <?= empty($stationDataItems) ?  0 :  json_encode($stationDataItems); ?>;

            var thaimonthname = <?php echo json_encode($thaimonthname) ?>;
        </script>
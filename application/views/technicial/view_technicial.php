        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><?php echo __('View Technicial')?> (<?php echo $technicial->code?>)</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?php echo base_url()?>"><?php echo __('Dashboard','default')?></a></li>
                            <li class="active"><?php echo __('View Technicial')?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-cog"></i> <?php echo __('Technicial Detail')?></div>
                            <div class="card-body">
                                <?php echo form_input(array(
                                    'type'=>'hidden',
                                    'name'=>'hide_agent_code',
                                    'value'=>$technicial->code
                                ))?>

                                <div id="tech_image_profile" class="text-center">
                                <i class="fa fa-spin fa-spinner fa-3x"></i>
                                <img src="" id="agent_image" class="img-fluid">
                                </div>
                                <p>&nbsp;</p>
                                <p><strong><?php echo __('Code')?> : </strong><?php echo $technicial->code?></p>

                                <p><strong><?php echo __('Firstname&Lastname')?> : </strong><?php echo $technicial->firstname.' '.$technicial->lastname?></p>

                                <p><strong><?php echo __('Email')?> : </strong><?php echo $technicial->email?></p>

                                <p><strong><?php echo __('Telephone')?> : </strong><?php echo $technicial->telephone?></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card">
                            <?php //print_r($technicial->to_array())?>
                            <div class="card-header"><i class="fa fa-cog"></i> <?php echo __('Technicial Address')?></div>
                            <div class="card-body">
                                <?php echo form_input(array(
                                    'type'=>'hidden',
                                    'name'=>'latitude',
                                    'id'=>'latitude',
                                    'value'=>$technicial->residence_latitude
                                ))?>
                                <?php echo form_input(array(
                                    'type'=>'hidden',
                                    'name'=>'longitude',
                                    'id'=>'longitude',
                                    'value'=>$technicial->residence_longitude
                                ))?>

                                <?php $technicial_address = $technicial->technicial_address->get()?>
                                <p><strong><?php echo __('Address')?> : </strong><?php echo $technicial_address->address?></p>

                                <p><strong><?php echo __('Province')?> : </strong><?php echo $technicial_address->province->get()->province_name?></p>

                                <p><strong><?php echo __('Amphur')?> : </strong><?php echo $technicial_address->amphur->get()->amphur_name?></p>

                                <p><strong><?php echo __('District')?> : </strong><?php echo $technicial_address->district->get()->district_name?></p>

                                <p><strong><?php echo __('Zipcode')?> : </strong><?php echo $technicial_address->zipcode?></p>

                                <div id="map-canvas" style="width:100%; min-height:450px;">

                                              </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASMadJM_c7vFjnWLWLrM5u2rQeZYGOLKM&callback=initialize_map&libraries=geometry,places&time=<?php echo strtotime(date('Y-m-d H:i:s'))?>">
    </script>
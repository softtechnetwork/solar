<div class="row pl-3 pt-3">
    <div class="col-4 col-sm-4 col-md-4 col-lg-4 pr-0 pl-2"  style="position:relative;">
        <span class="dot compare1_color" style="display:inline-block;position:absolute;top:2px;">

        </span>
        <span id="text_chart_compare1" class="text_chart"></span>
    </div>
    <div class="col-4 col-sm-4 col-md-4 col-lg-4 pr-0 pl-2"  style="position:relative;">
        <span class="dot compare2_color" style="display:inline-block;position:absolute;top:2px;">

        </span>
        <span id="text_chart_compare2" class="text_chart"></span>
    </div>
    <div class="col-4 col-sm-4 col-md-4 col-lg-4 pr-0 pl-2"  style="position:relative;">
        <span class="dot compare3_color" style="display:inline-block;position:absolute;top:2px;">

        </span>
        <span id="text_chart_compare3" class="text_chart"></span>
    </div>
    <div class="col-4 col-sm-4 col-md-4 col-lg-4 pr-0 pl-2"  style="position:relative;">
        <span class="dot compare4_color" style="display:inline-block;position:absolute;top:2px;">

        </span>
        <span id="text_chart_compare4" class="text_chart"></span>
    </div>
    <div class="col-4 col-sm-4 col-md-4 col-lg-4 pr-0 pl-2"  style="position:relative;">
        <span class="dot compare5_color" style="display:inline-block;position:absolute;top:2px;">

        </span>
        <span id="text_chart_compare5" class="text_chart"></span>
    </div>
    <div class="col-4 col-sm-4 col-md-4 col-lg-4 pr-0 pl-2"  style="position:relative;">
        <span class="dot compare6_color" style="display:inline-block;position:absolute;top:2px;">

        </span>
        <span id="text_chart_compare6" class="text_chart"></span>
    </div>
</div>
<input name="duration_month" id="duration_month" style="height:30px;cursor:pointer;" class="form-control button_main duration_month  float-right " value="<?php echo DATE("m") . '/' . DATE("Y"); ?>">
            <span class="fa fa-angle-down angledown" onclick="showdatepicker()" id="angle_down_month"></span>

            <select onchange="onchange_draw_data_year()" name="duration_year" id="duration_year" class="dropdown_year dropdown_year_select float-right" style="" data-live-search="true" title="Please select">
                <?php
                $this_year =  intval(DATE("Y"));
                for ($i = $this_year; $i >= 2010; $i--) {
                    $selected = "";
                    if (isset($startYear)) {
                        if ($startYear == $i) {
                            $selected = "selected";
                        }
                    }
                ?>
                    <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                <?php } ?>
            </select>
            <span class="fa fa-angle-down angledown" id="angle_down_year" style="margin-right:-65px;pointer-events: none;"></span>

<style>
   
    table.dataTable thead th {
        border-top: 0px;
    }

    table.dataTable thead .sorting_asc:before {
        display: none;
    }

    table.dataTable thead .sorting_asc:after,
    table.dataTable thead .sorting_desc:after,
    table.dataTable thead .sorting_asc_disabled:after,
    table.dataTable thead .sorting_desc_disabled:after {
        right: 0.5em;
        content: "\2193";
    }

    table.dataTable thead .sorting:before,
    table.dataTable thead .sorting:after,
    table.dataTable thead .sorting_asc:before,
    table.dataTable thead .sorting_asc:after,
    table.dataTable thead .sorting_desc:before,
    table.dataTable thead .sorting_desc:after,
    table.dataTable thead .sorting_asc_disabled:before,
    table.dataTable thead .sorting_asc_disabled:after,
    table.dataTable thead .sorting_desc_disabled:before,
    table.dataTable thead .sorting_desc_disabled:after {
        position: absolute;
        bottom: 0.9em;
        display: none;
        opacity: 0.3;
    }


    @media only screen and (min-width: 960px) {

        /* styles for browsers larger than 960px; */

    }

    @media only screen and (min-width: 1440px) {}

    @media only screen and (min-width: 2000px) {

        /* for sumo sized (mac) screens */

    }

    @media only screen and (max-device-width: 480px) {

        /* styles for mobile browsers smaller than 480px; (iPhone) */

    }

    @media only screen and (device-width: 768px) {}

    /* different techniques for iPad screening */
    @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) {

        /* For portrait layouts only */

    }

    @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {

        /* For landscape layouts only */

    }
</style>


<div class="container-fluid w-100 pl-0 pr-0" id="bigContain" style="height:90%">
    <div class="loader" style="display:none;"></div>
    <div class="panel_period col-sm-12 col-md-12 col-lg-12 pl-0 pr-0   " style="height:23%;">


        <div class="row mt-3 ml-2 mr-2">
            <div class="col-sm-12 col-md-6 col-lg-6 mb-2">
                <!-- nested row bootstrap-->
                <div class="row">
                    <div class="flex-column">
                        <?php
                        /** load view period  */
                        $this->load->view('./layout/period_radio');

                        ?>
                    </div>
                    <div class="flex-column pl-2">
                        <?php
                        $this->load->view('./layout/period_dropdown');
                        ?>
                    </div>
                </div>
                <!-- eof :nested row bootstrap-->
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
                <!-- div : period -->

                <!-- nested row bootstrap-->
                <div class="row d-flex">

                    <?php
                    for ($count = 1; $count <= 6; $count++) {

                    ?>
                        <div class="col-4 col-sm-6 col-md-6 col-lg-4 pl-0 pr-0 <?php echo $count > 3 ? "mt-1" : "mb-1" ?> <?php echo $count == 6  ? "" : "mb-1" ?>">
                            <select onchange="onchange_data_planlist(<?php echo $count; ?>)" name="dropdown_planlist[]" id="dropdown_planlist_<?php echo $count; ?>" class="dropdown_planlist dropdown_planlist_<?php echo $count; ?>  " style="width:90%;" data-live-search="true" title="Please select">
                                <?php
                                if (!empty($customer_rel_plan)) {

                                    foreach ($customer_rel_plan as $key => $value) {
                                ?>
                                        <option value="<?php echo $value->stationId ?>"><?php echo $value->name_solarapp; ?></option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                            <span class="fa fa-angle-down angledowncompare" id="angledown_compare" style="pointer-events: none;"></span>
                        </div>
                    <?php
                    }
                    ?>
                </div>

                <!-- eof :nested row bootstrap-->
            </div>
        </div>


    </div>
    <div class="panel_twoside col-sm-12 col-md-12 col-lg-12 pl-0 pr-0   " style="height:77%;">

        <div class="row h-100 ml-0 mr-0" style="max-width:100%;">
            <div class="div_graph col-12 col-md-6 col-lg-6 pl-0 pr-0  " >
                <div class="row ml-0 mr-0">
                    <div class="pl-2">
                        <p class="" style="padding-top:15px;">
                            <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                        <p>
                    </div>
                    <div class="col-10 pr-0">
                        <p class="text_headertitle" style="font-size:14px;padding-top:16px;margin-bottom:0px;">กราฟเปรียบเทียบพลังงานที่ผลิตได้ (kWh)</p>
                        <p class="text_headertitle text_period mb-0" style="font-size:14px;">แสดงเป็นรายเดือน</p>


                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-md-12 col-sm-12">
                        <div id="line_chart"></div>
                    </div>
                </div>

                <?php
                /** load view compare chart  */
                $this->load->view('./layout/compare_charttextlabel');

                ?>
            </div>
            <div class="div_table col-12 col-md-6 col-lg-6 pl-0 pr-0  " style="">
                <div class="row ml-0 mr-0" style="">
                    <div class="pl-2" style="">
                        <p class="" style="padding-top:15px;">
                            <a href="#"> <img src="<?php echo base_url('assets/images/home_icon.png') ?>" style="width:25px;height:25px;" class="text_color_center text_font_header"></a>
                        <p>
                    </div>
                    <div class="col-10 pr-0">
                        <p class="text_headertitle" style="font-size:14px;padding-top:16px;margin-bottom:0px;">ตารางข้อมูลเปรียบเทียบพลังงานที่ผลิตได้ (kWh)</p>
                        <p class="text_headertitle text_period mb-0" style="font-size:14px;">แสดงเป็นรายเดือน</p>

                    </div>


                </div>
                <div class="col-12 col-lg-12 col-md-12 pl-0 pr-0">
                    <table style="width:100%;" class="table pageResize" id="table_detail">
                        <thead style="">
                            <tr>

                                <th class="text-center text_font_headertable"><?php echo __("วันที่") ?></th>
                                <!-- position:absolute;bottom:13px;text-align:center;margin-left:20px; -->
                                <th class="text_font_headertable" style="padding-left:5%;">
                                    <div class="dot compare1_color" style=""></div>
                                </th>
                                <th class="text_font_headertable" style="padding-left:5%;">
                                    <div class="dot compare2_color" style=""></div>
                                </th>
                                <th class="text_font_headertable" style="padding-left:5%;">
                                    <div class="dot compare3_color" style=""></div>
                                </th>
                                <th class="text_font_headertable" style="padding-left:5%;">
                                    <div class="dot compare4_color" style=""></div>
                                </th>
                                <th class="text_font_headertable" style="padding-left:5%;">
                                    <div class="dot compare5_color" style=""></div>
                                </th>
                                <th class="text_font_headertable" style="padding-left:5%;">
                                    <div class="dot compare6_color" style=""></div>
                                </th>

                            </tr>
                        </thead>
                        <tbody id="body_tbl" style="">
                            <?php
                            date_default_timezone_set("Asia/Bangkok");
                            ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>




<script tyle="text/javascript">
    var user_id = <?php echo $user_id; ?>;
    var customer_lastselected = <?php echo json_encode($customer_lastselected) ?>;
</script>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><?php echo __('Change Password')?></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?php echo base_url()?>"><?php echo __('Dashboard','default')?></a></li>
                            <li class="active"><?php echo __('Change Password')?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        
            

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">
                    <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                    </div>  
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-key"> <?php echo __('Change password form')?></i></div>
                            <div class="card-body">

                                <?php echo form_open('',array('name'=>'change-password-form'))?>
                                    <div class="row">
                                        <div class="col-lg-4">

                                            <div class="form-group">
                                                <label><strong><?php echo __('Old Password')?> : </strong></label>
                                                <?php echo form_input(array(
                                                    'type'=>'password',
                                                    'name'=>'old_password',
                                                    'class'=>'form-control'
                                                ))?>
                                            </div>

                                            <div class="form-group">
                                                <label><strong><?php echo __('New Password')?> : </strong></label>
                                                <?php echo form_input(array(
                                                    'type'=>'password',
                                                    'name'=>'new_password',
                                                    'class'=>'form-control'
                                                ))?>
                                            </div>

                                            <div class="form-group">
                                                <label><strong><?php echo __('Confirm new password')?> : </strong></label>
                                                <?php echo form_input(array(
                                                    'type'=>'password',
                                                    'name'=>'confirm_new_password',
                                                    'class'=>'form-control'
                                                ))?>
                                            </div>

                                            <div class="form-group">
                                                <?php echo form_button(array(
                                                    'type'=>'submit',
                                                    'class'=>'btn btn-success float-right',
                                                    'content'=>__('Submit','default')
                                                ))?>
                                            </div>

                                        </div>
                                    </div>

                                <?php echo form_close()?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><?php echo __('Profile')?></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?php echo base_url()?>"><?php echo __('Dashboard','default')?></a></li>
                            <li class="active"><?php echo __('Profile')?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        
            

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">
                    <div class="col-lg-12">
                    <?php echo message_warning($this)?>
                    </div>  
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><i class="fa fa-cog"> <?php echo __('Edit Profile')?></i></div>
                            <div class="card-body">
                                <?php echo form_open('',array('name'=>'profile-form'))?>
                                    <div class="row">
                                        <div class="col-lg-6">

                                            <!-- for firstname and lastname -->
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label><strong><?php echo __('Firstname')?> : </strong></label>
                                                        <?php echo form_input(array(
                                                            'name'=>'firstname',
                                                            'class'=>'form-control',
                                                            'value'=>@$account_data['firstname']
                                                        ))?>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label><strong><?php echo __('Lastname')?> : </strong></label>
                                                        <?php echo form_input(array(
                                                            'name'=>'lastname',
                                                            'class'=>'form-control',
                                                            'value'=>@$account_data['lastname']
                                                        ))?>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- eof for firstname and lastname -->

                                            <div class="form-group">
                                                <label><strong><?php echo __('Email')?> : </strong></label>
                                                <?php echo form_input(array(
                                                    'name'=>'email',
                                                    'class'=>'form-control',
                                                    'value'=>@$account_data['email'],
                                                    'readonly'=>'readonly'
                                                ))?>
                                            </div>

                                            <div class="form-group">
                                                <label><strong><?php echo __('Telephone')?> : </strong></label>
                                                <?php echo form_input(array(
                                                    'name'=>'telephone',
                                                    'class'=>'form-control',
                                                    'value'=>@$account_data['telephone']
                                                ))?>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <?php echo form_button(array(
                                                    'type'=>'submit',
                                                    'class'=>'btn btn-success float-right',
                                                    'content'=>__('Submit','default')
                                                ))?>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">

                                        </div>
                                    </div>


                                <?php echo form_close()?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
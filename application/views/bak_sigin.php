<div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container col-12 col-lg-4 col-md-4">
            <div class="login-content ">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="<?php echo base_url('assets/images/logo.png') ?>" alt="">
                    </a>

                    <h2 class="text-white">
                        <!-- SOLAR -->

                    </h2>
                </div>


                <div class="login-form" style="border-radius:10px !important;">
                    <?php echo message_warning($this) ?>
                    <?php echo form_open('', array('name' => 'login-form')) ?>
                    <div class="form-group">
                        <label><?php echo __('ชื่อเข้าระบบ') ?></label>
                        <?php echo form_input(array(
                            'type' => 'text',
                            'name' => 'email',
                            'class' => 'form-control',
                            'style' => 'background-color: white !important;',
                            'value' => @$remember_me['email']
                        )) ?>
                    </div>
                    
                  
                    <div class="form-group">
                        <label><?php echo __('รหัสผ่าน') ?></label>
                        <div class="input-group">

                            <input type="password" name="password" class="form-control" id="password" style="   background-color: white !important;" />
                            <span class="bi bi-eye-slash" id="togglePassword"></span>
                        </div>
                    </div>
                    <div class="wrapper">
                        <button  type="submit" style="border-radius: 24px;background-color:#18C3D7;color:white;" class="btn btn-success col-6 m-b-30 m-t-30"><?php echo __('Sign in') ?></button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
                    
            <p style="text-align:center;font-size:12px;">Power by 2021 - 2022 Softtech Network Co.,Ltd.</p>
                   
        </div>
    </div>
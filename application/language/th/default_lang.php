<?php

$lang['ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง,กรุณาลองใหม่อีกครั้ง'] = "ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง,กรุณาลองใหม่อีกครั้ง";
$lang['Dashboard'] = "Dashboard";
$lang['Status'] = "สถานะ";
$lang['Created'] = "สร้างเมื่อ";
$lang['Jan'] = "มกราคม";
$lang['Feb'] = "กุมภาพันธ์";
$lang['Mar'] = "มีนาคม";
$lang['Apr'] = "เมษายน";
$lang['May'] = "พฤษภาคม";
$lang['Jun'] = "มิถุนายน";
$lang['Jul'] = "กรกฎาคม";
$lang['Aug'] = "สิงหาคม";
$lang['Sep'] = "กันยายน";
$lang['Oct'] = "ตุลาคม";
$lang['Nov'] = "พฤศจิกายน";
$lang['Dec'] = "ธันวาคม";
$lang['Updated'] = "แก้ไขล่าสุด";
$lang['Submit'] = "ตกลง";
$lang['Save'] = "บันทึก";
$lang['Close'] = "ปิด";
$lang['Success'] = "Success";
$lang['Search'] = "Search";
$lang['หน้าแรก'] = "หน้าแรก";

<?php

$lang['Email address'] = "อีเมล์";
$lang['Email'] = "อีเมล์";
$lang['Password'] = "รหัสผ่าน";
$lang['Remember Me'] = "จำการเข้าใช้ระบบ";
$lang['Forgotten Password?'] = "ลืมรหัสผ่าน?";
$lang['Email or password invalid,Please try again'] = "ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง,กรุณาลองใหม่อีกครั้ง";
$lang['No set user permission,Please set permission'] = "ยังไม่มีการสิทธิ์การเข้าถึงเมนู กรุณาตั้งค่าสิทธิ์การเข้าถึงเมนูก่อนเข้าใช้ระบบ";
$lang['Sign in'] = "เข้าสู่ระบบ";
$lang['ชื่อเข้าระบบ'] = "ชื่อเข้าระบบ";
$lang['รหัสผ่าน'] = "รหัสผ่าน";

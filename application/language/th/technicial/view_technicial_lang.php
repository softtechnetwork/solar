<?php

$lang['View Technicial'] = "ข้อมูลช่าง";
$lang['Technicial List'] = "รายการช่าง";
$lang['Technicial Detail'] = "รายละเอียด";
$lang['Technicial Address'] = "รายละเอียดที่อยู่";
$lang['Code'] = "รหัสช่าง";
$lang['Firstname&amp;Lastname'] = "ชื่อ-สกุล";
$lang['Email'] = "อีเมล์";
$lang['Telephone'] = "เบอร์โทร";
$lang['Address'] = "ที่อยู่";
$lang['Province'] = "จังหวัด";
$lang['Amphur'] = "อำเภอ/เขต";
$lang['District'] = "ตำบล/แขวง";
$lang['Zipcode'] = "รหัสไปรษณีย์";

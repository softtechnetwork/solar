<?php

$lang['Change Password'] = "เปลี่ยนรหัสผ่าน";
$lang['Change password form'] = "แบบฟอร์มเปลี่ยนรหัสผ่าน";
$lang['Old Password'] = "รหัสผ่านเดิม";
$lang['New Password'] = "รหัสผ่านใหม่";
$lang['Confirm new password'] = "ยืนยันรหัสผ่านใหม่";
$lang['Change password success'] = "เปลี่ยนรหัสผ่านสำเร็จ";

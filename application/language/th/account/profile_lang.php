<?php

$lang['Profile'] = "ข้อมูลส่วนตัว";
$lang['Edit Profile'] = "แก้ไขข้อมูลส่วนตัว";
$lang['Firstname'] = "ชื่อ";
$lang['Lastname'] = "สกุล";
$lang['Email'] = "อีเมล์";
$lang['Telephone'] = "เบอร์โทรศัพท์";
$lang['Edit profile success'] = "แก้ไขข้อมูลส่วนตัวสำเร็จ";

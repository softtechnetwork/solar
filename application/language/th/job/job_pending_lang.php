<?php

$lang['Job Pending List'] = "รายการงานที่รอเบิกจ่าย";
$lang['Job Pending Table'] = "ตารางรายการงานที่รอเบิกจ่าย";
$lang['Job Number'] = "เลขที่งาน";
$lang['Technicial'] = "ช่าง";
$lang['Customer'] = "ลูกค้า";
$lang['Product'] = "สินค้า";
$lang['Approve Status'] = "สถานะการอนุมัติ";
$lang['Payment Status'] = "สถานะการจ่ายเงิน";
$lang['Approved'] = "อนุมัติแล้ว";
$lang['Unpaid'] = "รอจ่ายเงิน";
$lang['Paid'] = "จ่ายเงินแล้ว";
$lang['Pending'] = "รอเบิกจ่าย";
$lang['Request Paid'] = "จ่ายเงินแล้ว";
$lang['Select Action'] = "ทำที่เลือก";
$lang['Pay Date'] = "กำหนดจ่ายเงิน";
$lang['Partner'] = "Partner";
$lang['Export to excel file'] = "ส่งออกเป็น excel ไฟล์";

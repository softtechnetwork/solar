<?php

$lang['Job Paid List'] = "รายการงานที่จ่ายเงินแล้ว";
$lang['Job List Table'] = "ตารางงาน";
$lang['Job Number'] = "เลขที่งาน";
$lang['Technicial'] = "ช่าง";
$lang['Customer'] = "ลูกค้า";
$lang['Product'] = "สินค้า";
$lang['Approve Status'] = "สถานะการอนุมัติ";
$lang['Payment Status'] = "สถานะการจ่ายเงิน";
$lang['Approved'] = "อนุมัติแล้ว";
$lang['Unpaid'] = "รอจ่ายเงิน";
$lang['Paid'] = "จ่ายเงินแล้ว";
$lang['Payment Date'] = "กำหนดจ่ายเงิน";
$lang['Partner'] = "Partner";
$lang['Select Action'] = "ทำที่เลือก";
$lang['Export to excel'] = "ส่งออกเป็น excel ไฟล์";

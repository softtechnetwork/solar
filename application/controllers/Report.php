<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . 'libraries/AccountantLibrary.php';
class Report extends AccountantLibrary
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->load->library(array('pagination'));

	}

	public function payloadreport()
	{
	}
	public function index()
	{

		date_default_timezone_set('Asia/Bangkok');

		$this->bootstrapDatepicker();

		$this->loadDatePickerStyle();
		$this->loadDatePickerScript();


		$this->loadDataTableStyle();
		$this->loadDataTableScript();
		// $this->loadSweetAlert();



		$this->loadDateRangePickerStyle(); // load jquery datepicker rang
		$this->loadDateRangePickerScript(); // load jquery picker


		$this->template->javascript->add("https://www.gstatic.com/charts/loader.js");
		$this->template->javascript->add("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js");


		$search_text = "";
		$postData = $this->input->post();
		$startDate = "";
		$endDate = "";
		$startYear = "";
		$endYear = "";
		$stationDataItems = array();
		$timeType = 0;
		$duration_month = "";
		if (isset($_GET['station_id'])) {

			$startDate = DATE("Y-m-d");
			$endDate = DATE("Y-m-d");
			$_POST["submit"] = true;
			$_POST["search_planlist"] = $_GET["station_id"];
			$_POST["duration"]  = $startDate . ' - ' . $endDate;
		}
		# if get period
		if (isset($_GET["period"])) {
			$this->session->set_userdata(array("period" => $_GET["period"]));
		}


		if ($this->input->post(NULL, FALSE) || !empty($_POST["search_planlist"])) {

			if ($this->input->post('submit') != NULL) {

				$search_text = $this->input->post('search_planlist');
				$this->session->set_userdata(array("search_planlist" => $search_text));

				# case : ถ้าเป็นวันรายเดือน
				if (isset($_POST["duration_month"])) {
					$duration_month = $_POST["duration_month"];

					$arr_duration = explode("/", $_POST["duration_month"]);
					$month =  $arr_duration[0];
					$year =  $arr_duration[1];
					$duration_date = $this->onactive_select_getdatetimereport($year, $month);

					$_POST["duration"] = $duration_date[key($duration_date)] . ' - ' . end($duration_date);
				}
			} else {
				if ($this->session->userdata('search_planlist') != NULL) {

					if ($this->input->post('search_planlist')) {

						$search_text = $this->input->post('search_planlist');
						$this->session->set_userdata(array("search_planlist" => $search_text));
					} else {

						if (!empty($postData)) {
							if (isset($postData['search_planlist'])) {
								$this->session->set_userdata(array('search_planlist' => $postData['search_planlist']));
							}
						} else {
							$search_text = $this->session->userdata('search_planlist');
						}
					}
				} else {
					$search_text = $this->input->post('search_planlist');
					$this->session->set_userdata(array('search_planlist' => $search_text));
				}
			}

			$period = "";
			if ($this->input->post('duration') != NULL  ||  ($this->input->post('year_start') != NULL)) {

				//print_r($criteria);

				$period = !empty($this->session->userdata("period")) ? $this->session->userdata("period") : "";
				if ($period == "date") {


					$startDate = $this->input->post("duration");
					$endDate = $this->input->post("duration");


					$this->session->set_userdata(array("duration_date" => $this->input->post("duration"))); // set session start date 

				}
				if ($period == "month") {
					$exDuration = explode(' - ', $this->input->post('duration'));
					$convert_startDate = new DateTime($exDuration[0]);
					$convert_endDate = new DateTime($exDuration[1]);

					$startDate = $convert_startDate->format('Y-m-d');
					$endDate   = $convert_endDate->format('Y-m-d');

					$this->session->set_userdata(array("duration_month" => $duration_month)); // set session start date 


				}
				if ($period == "year") {
					$fullYear =  $this->input->post("year_start");
					$startYear = $fullYear . '-' . "01";
					$endYear   = $fullYear . '-' . "12";


					$this->session->set_userdata(array("startYear" => $startYear)); // set session start date 
					$this->session->set_userdata(array("endYear" => $endYear)); // set session end date	
				}
			}

			$search_planlist = $this->input->post('search_planlist');
			//echo $search_planlist;exit();
			$returndata = $this->get_stationdataitem(
				$search_planlist,
				$period == "year" ? $startYear :  $startDate,
				$period == "year" ? $endYear : $endDate,
				$period
			);


			if (isset($returndata["stationDataItems"])) {
				$stationDataItems = $returndata["stationDataItems"];
			}
			if (isset($returndata["unit_price"])) {
				$unit_price = $returndata["unit_price"];
			}
			$timeType = $returndata["timeType"];
		}
		# case : get thaimonth name
		for ($i = 1; $i <= 12; $i++) {
			$get_monththainame[$i]["full"] = $this->get_thaimonthname($i, "full");
			$get_monththainame[$i]["short"] = $this->get_thaimonthname($i, "short");
		}

		$arr_planlist = $this->onaction_set_removeduplicateplankey($this->fetch_planlistreloauth());

		$data = array(
			'energy_used' => array(),
			'planlist' => $arr_planlist,
			'timeType' => $timeType,
			"get_monththainame" => $get_monththainame,
			"period" => !empty($this->session->userdata("period")) ? $this->session->userdata("period") : ""
		);

		$data['startDate'] = $startDate; // customer selected
		$data['endDate'] = $endDate; // customer selected
		$data["duration_month"] = $duration_month; // duration : month
		$data["startYear"] = $startYear;
		$data["endYear"] = $endYear;
		$data["stationDataItems"] = $stationDataItems;
		$data['search_text'] = $search_text;

		$this->template->content->view('report/enerygy_used', $data);
		$this->template->javascript->add(base_url('assets/js/energy_used.js')); // load 
		$this->template->publish();
	}
	public function onactive_select_getdatetimereport($year, $month)
	{

		$start_date = "01-" . $month . "-" . $year;
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		for ($i = $start_time; $i < $end_time; $i += 86400) {
			$list[] = date('Y-m-d', $i);
		}
		return $list;
	}

	public function get_thaimonthname($monthNum = null, $monthname_type = null)
	{

		$monthNum = intval($monthNum);
		switch ($monthNum) {

			case 1:
				$month = "มกราคม";
				if($monthname_type == "short") { return "ม.ค.";}
				return $month;
				break;
			case 2:
				$month = "กุมภาพันธ์";
				if($monthname_type == "short") { return "ก.พ.";}
				return $month;
				break;
			case 3:
				$month = "มีนาคม";
				if($monthname_type == "short") { return "มี.ค.";}
				return $month;
				break;
			case 4:
				$month = "เมษายน";
				if($monthname_type == "short") { return "เม.ย.";}
				return $month;
				break;
			case 5:
				$month = "พฤษภาคม";
				if($monthname_type == "short") { return "พ.ค.";}
				return $month;
				break;
			case 6:
				$month = "มิถุนายน";
				if($monthname_type == "short") { return "มิ.ย.";}
				return $month;
				break;
			case 7:
				$month = "กรกฎาคม";
				if($monthname_type == "short") { return "ก.ค.";}
				return $month;
				break;
			case 8:
				$month = "สิงหาคม";
				if($monthname_type == "short") { return "ส.ค.";}
				return $month;
				break;
			case 9:
				$month = "กันยายน";
				if($monthname_type == "short") { return "ก.ย.";}
				return $month;
				break;
			case 10:
				$month = "ตุลาคม";
				if($monthname_type == "short") { return "ต.ค.";}
				return $month;
				break;
			case 11:
				$month = "พฤศจิกายน";
				if($monthname_type == "short") { return "พ.ย.";}
				return $month;
				break;
			case 12:
				$month = "ธันวาคม";
				if($monthname_type == "short") { return "ธ.ค.";}
				return $month;
				break;
		}
	}
	public function oauth2_list_history($timeType, $startDate, $endDate, $stationId, $url, $access_token)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$headers = array(
			"Accept: application/json",
			"Authorization: Bearer {$access_token}",
			"Content-Type: application/json",
		);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		# json response time type 1 = show every hour on date , time type 2 = date range
		$data = '{"stationId":"' . $stationId . '","startTime":"' . $startDate . '","endTime":"' . $endDate . '","timeType":"' . $timeType . '"}';
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$resp = curl_exec($curl);
		curl_close($curl);

		return $resp;
	}


	public function get_stationdataitem($search_planlist, $startDate, $endDate, $period)
	{
		$returndata = array();
		$get_planlist = $this->fetch_planlistreloauth(null, null, null, null, null, $search_planlist);

		if (!empty($get_planlist)) {

			$auth_account_id = $get_planlist[key($get_planlist)]->auth_account_id;
			$stationId = $get_planlist[key($get_planlist)]->station_id;

			$get_accountdata = $this->get_customeraccesstoken($auth_account_id);
			if (!empty($get_accountdata)) {
				$access_token =  $get_accountdata->access_token; # get access token

				$url = $this->oauth2_url("history"); # get url 
				# json param : time type 
				$timeType = ($startDate == $endDate) ? 1 : 2;
				# case : ถ้าเป็นรายเดือนให้ default เป็น time type 2
				if ($period == "month") {
					$timeType = 2;
				}
				if ($period == "year") {
					$timeType = 3;
				}

				$returndata["timeType"] = $timeType;

				# case : get history data from solarman api 
				$resp = $this->oauth2_list_history($timeType, $startDate, $endDate, $stationId, $url, $access_token);
				# case : convert json to array
				$decode_result = json_decode($resp);

				$get_stationinput = $this->fetch_stationrelplaninput($stationId);
				if (empty($get_stationinput)) {
					$returndata["unit_price"] = 0;
				} else {
					$returndata["unit_price"] = isset($get_stationinput[key($get_stationinput)]->unit_priceSolarApp) ? $get_stationinput[key($get_stationinput)]->unit_priceSolarApp : 0;
				}

				if (!empty($decode_result)) {
					if (isset($decode_result->total)) {
						if ($decode_result->total > 0) {
							# case : set return station data
							$returndata["stationDataItems"]  = $this->onaction_set_returnstationitem($decode_result, $returndata["unit_price"], $timeType);
							return $returndata;
						} else {
							# not found data
							return $returndata;
						}
					}
				}
			}
		}

		return false;
	}


	public function plan_location()
	{

		// $arrdata_planlist = $this->fetch_planlistreloauth();
		$arrdata_planlist = $this->fetch_planstationoauth();

		$planlist  = $this->onaction_set_removeduplicateplankey($arrdata_planlist); # remove duplicate station id 
		$station_arr = array_map(function ($value) { # case : get active station
			return $value->station_id;
		}, $planlist);
		# case : find device serial number
		$devicesn = $this->fetch_stationrelplaninputoauth(null, null, null, null, null, null, $station_arr, null);

		$data = array(
			"googlemap" => $planlist,
			"devicesn" => $devicesn
		);
		$this->template->content->view('report/plan_location', $data);
		$this->template->javascript->add(base_url('assets/js/plan_location.js'));
		$this->template->publish();
	}
}

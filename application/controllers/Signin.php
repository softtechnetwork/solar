<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->helper(array(
			'lang',
			'cookie',
			'our'
		));
		$this->load->library(array(
			'Lang_controller',
			'Msg'
		));
	}
	public function index(){

	
		if($this->session->userdata('accountantdata')){
		
			redirect('Main');
			
		}

		if($this->input->post(NULL,FALSE)){
			$partner = array();
			// $users = new M_Accountant();
		
			// $users->where('email',$this->input->post('email'))
			// ->where('password',sha1($this->input->post('password')))->get();
			$users = "";
			$query_users = $this->db->select('*')->from('customers')->where('username',$this->input->post('email'))
			->where('password', hash('sha256' , $this->input->post('password')))->get();
			if($query_users->num_rows() > 0 ){
				$users = $query_users->row();
			}

		
		
			if(isset($users->id)){
				
				
					$this->session->set_userdata('accountantdata',(array)$users);
					
					//print_r($this->session->userdata());exit;
					// redirect(base_url('Main'));
					redirect('Main');


			}else{
				$this->msg->add(__('Email or password invalid,Please try again','signin'),'error');
				redirect($this->uri->uri_string());

			}

		
		}


		$data = array(
			
		);
		$this->load->view('signin',$data);
	}

	public function signout(){
		$this->session->sess_destroy();
		redirect($this->controller);
	}

	public function testSendEmail(){
		$user = new M_user(1007);
		$this->sendResetPasswordEmail(array(
			'user'=>$user
		));

	}
	public function forgot_password(){


		if($this->input->post(NULL,FALSE)){
			//print_r($this->input->post());exit;
			/* check email exist in system */
			$user = new M_user();
			$user->where('email',$this->input->post('email'))->get();

			if(!$user->id){
				$this->msg->add(__('Not found this email from system','forgot_password'),'error');
				redirect($this->uri->uri_string());
			}

			$this->sendResetPasswordEmail(array(
				'user'=>$user
			));

			$this->msg->add(__('Send reset password to email success','forgot_password'),'success');
			redirect($this->uri->uri_string());
			


			//psi_sendmail($email_subject,$email_message,$email_to,$email_from,$email_from_name);


		}


		$data = array(

		);

		$this->load->view('forgot_password',$data);
	}
	private function getRememberMe(){
    	$arReturn = array(
    		'email'=>'',
    		'password'=>''
    	);

    	if(get_cookie('_psitdc_remember_me_accountant') && get_cookie('_psitdc_remember_me_password')){

    		$email = base64_decode(base64_decode(get_cookie('_psitdc_remember_me_accountant')));
    		$admin_password = base64_decode(base64_decode(get_cookie('_psitdc_remember_me_password')));
    		
    		$arReturn['email'] = $email;
    		$arReturn['password'] = $admin_password;
    	}
    	return $arReturn;
    }

    
    private function sendResetPasswordEmail($data = array()){

			$this->load->helper(array('sendmail'));

    		$email_subject = __('forgot password email subject','email/forgot_password');
			$email_to = $data['user']->email;
			$email_message = $this->load->view('email/forgot_password',array(
				'user'=>$data['user'],
				'reset_password_url'=>$this->getResetPasswordUrl(array(
					'user'=>$data['user']
				))
			),true);	
			$email_from = 'kridsada@psisat.com';
			$email_from_name = "Chaang Application";

			//echo $email_message;
			psi_sendmail($email_subject,$email_message,$email_to,$email_from,$email_from_name);


    }


    private function getResetPasswordUrl($data = array()){
    	$hash = $data['user']->id.'||'.$data['user']->email;
    	$hash = base64_encode(base64_encode($hash));

    	$url = base_url('activity/reset_password/'.$hash);
    	return $url;


    } 
}

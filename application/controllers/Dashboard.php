<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . 'libraries/AccountantLibrary.php';
class Dashboard extends AccountantLibrary
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('pagination'));
    }

    public function set_newmodelname($model)
    {
        $model_strlower = strtolower($model);
        switch ($model_strlower) {
            case "P100/3.3k x3":
                return "10Kw";
                break;
            case "P150/5K x3":
                return "15Kw";
                break;
            case "P300 x3p":
                return "30Kw";
                break;
            case "P600 x3p":
                return "60Kw";
                break;
            default:
                return $model;
        }
    }

    
    public function index()
    {

        $this->template->javascript->add(base_url('assets/js/dashboard.js'));

        $planlist = $this->onaction_set_removeduplicateplankey($this->fetch_planstationoauth()); # case : remove duplicate key and get only station_id 
        $station_arr = array_map(function ($value) { # case : get active station
            return $value->station_id;
        }, $planlist);

        $summary_year = $this->fetch_report_summaryyear_planlist(null, null, null, null, null, null, $station_arr);
        $summary_day = $this->fetch_report_summaryday_planlist(null, null, null, null, null, null, $station_arr, DATE("Y"), intval(DATE("m")), intval(DATE("d")));

        $summary_inverter = $this->dashboard_groupbyinverter(); # แสดงจำนวน อินเวอเตอร์ ที่ติดตั้งทุกโครงการ
        //echo '<PRE>';print_r($summary_inverter);exit();
        $summary_inverter_countvalue = array_sum(array_column($summary_inverter, 'count_inverter')); # หายอดรวม

        $grid_invertermodel = $this->fetch_gridinvertermodel(); # get all grid inverter model
        
        # case : วนลูปเพื่อ set ชื่อ inverter ใหม่ และ set ค่าไปแสดงบน frontend
        foreach($summary_inverter as $key => $value){
            $summary_inverter[$key]->model =  $this->set_newmodelname($summary_inverter[$key]->model);
        }
        foreach($grid_invertermodel as $grid_key => $grid_value){
            
            $find_invkey = array_search($grid_value->model, array_column($summary_inverter, 'model'));
            # case : เช็คว่ามีในระบบไหม
            if(is_numeric($find_invkey)){
                $summary_inverter[$find_invkey]->model =  $this->set_newmodelname($summary_inverter[$find_invkey]->model);
                if($summary_inverter[$find_invkey]->model == $grid_value->model){ # case : เพื่อจัดลำดับ
                    $x =  $summary_inverter[$find_invkey]->count_inverter * 100 / $summary_inverter_countvalue; # คำนวณว่าได้กี่%
                    $grid_invertermodel[$grid_key]->percent = number_format($x, 1, '.', ',');
                    $grid_invertermodel[$grid_key]->no = $grid_value->no;
                    $grid_invertermodel[$grid_key]->count_inverter = $summary_inverter[$find_invkey]->count_inverter;
                }
            }else{
                $grid_invertermodel[$grid_key]->percent = "00";
                $grid_invertermodel[$grid_key]->count_inverter =0;
               
            }
            
        }

        
      
      


        if (!empty($summary_day)) {
            $saveamounth_arr = array_map(function ($value) { # case : แต่ละ station ประหยัดได้กี่บาท
                return $value->generationValue * $value->unit_price;
            }, $summary_day);
        } else {
            $saveamounth_arr = array();
        }
       
       
        // case : line chart report พลังที่ผลิตได้ในวันนี้เทียบเป็น %
        $summary_linechart = $this->fetch_report_summarypwhr(intval(DATE("Y")), intval(DATE("m")), intval(DATE("d")));
        $twentyfourhr_linechart = array();
        $maxvalue = 0;
        
      
        if(!empty($summary_linechart)){
            for ($i = 0; $i < 24; $i++) {
                $twentyfourhr_linechart[$i]  = array("watt" => 0, "hr" => $i);
                $foundkey = array_search($i, array_column($summary_linechart, 'hr'));
                if (is_numeric($foundkey)) {
                    $twentyfourhr_linechart[$i] = (array)$summary_linechart[$foundkey];
                    //$maxvalue = $twentyfourhr_linechart[$i]["watt"] > $maxvalue ? $twentyfourhr_linechart[$i]["watt"] : $maxvalue;
                }
            }
            $summary_watt = array_sum(array_column($twentyfourhr_linechart, 'watt')); # case : พลังที่ผลิตได้วันนี้ ( w )
            if (!empty($twentyfourhr_linechart) && !empty($summary_watt)) {

                foreach ($twentyfourhr_linechart as $linechartkey => $linechartvalue) {
                    $watt   = $linechartvalue['watt'];
                    $percent = $watt > 0 ? $watt * 100 / $summary_watt : 0;
                    $twentyfourhr_linechart[$linechartkey]['percent'] = number_format($percent, 0, '.', ',');
                }
            }
        }else{
            for ($i = 0; $i < 24; $i++) {
                $twentyfourhr_linechart[$i]  = array("watt" => 0, "hr" => $i , "percent" => 0);
            }
            $summary_watt = 0;
        }

        # case : count all device
        $all_devices = $this->fetch_devices();
        
        $get_devicesrelationwithmodel = $this->fetch_devices( null  , null , null , true);
        // echo $this->db->last_query();exit();
        $group_by = array();
  
        foreach($get_devicesrelationwithmodel as $key => $value){
            $model = $value->model == "set default " || $value->model == "set default" ? "อื่นๆ" :   $value->model;
            if(!isset($group_by[$model])){
                $group_by[$model]['all_devices'] = 0;
                $group_by[$model]['online']      = 0;
            }
            if(!empty($value->lastUpdateTime)){
                $group_by[$model]['online']  += 1;
            }
            $group_by[$model]['all_devices'] += 1;
        }
        
        $count_devicesregister = $this->count_devicesregister();
    
        // $saveamounth_arr = array_map (function($value){ # case : แต่ละ station ประหยัดได้กี่บาท
        //     return $value->generationPower * $value->unit_price;
        // } , $planlist);


        //$summary_year  = $this->onaction_set_removeduplicateplankey($query_summaryyear); # remove duplicate station id 

        # case : calcurate and send it to dashboard
        $summaryalltime_inverter_producted = array_sum(array_column($summary_year, 'generationValue')); # case : พลังที่ผลิตได้และรวมทุกโครงการวันนี้ ( kw )
        $summarytoday_businessresult = $summaryalltime_inverter_producted * 4.4; # case : ผลตอบแทนรวมทุกโครงการ
        $summaryalltime_kgco2_producted = $summaryalltime_inverter_producted * 0.5113; # case : ลดคาร์บอนกี่บาท

        $summarytoday_inverter_producted = array_sum(array_column($planlist, 'generationPower')); # case : พลังที่ผลิตได้และรวมทุกโครงการวันนี้ ( w )
        $summarytoday_inverter_producted = $summarytoday_inverter_producted > 0 ? $summarytoday_inverter_producted / 10000 : 0;
        $hour = intval(DATE("H")) > 0 ? intval(DATE("H")) : 1;
        
        
        $summarytoday_inverter_producted_kwh = $summary_watt > 0 ?  $summary_watt / 10000 : 0; # case : พลังที่ผลิตได้วันนี้ ( kwh กิโลวัต)
     
        $summarytoday_save_producted = array_sum($saveamounth_arr); # case : ประหยัดได้ทั้งหมดกี่บาท

        $summarytoday_inverter_productedtoday = empty($summary_day) ? 0 :  array_sum(array_column($summary_day, 'generationValue')); # case : พลังที่ผลิตได้และรวมทุกโครงการวันนี้ ( w )

        $data = array(
            "planlist" => $summary_year,
            "summarytoday_inverter_producted" => $summarytoday_inverter_producted,
            "summaryalltime_inverter_producted" => $summaryalltime_inverter_producted,
            "summaryalltime_kgco2_producted" => $summaryalltime_kgco2_producted,
            "summarytoday_save_producted" => $summarytoday_save_producted,
            "summarytoday_inverter_producted_kwh" => $summarytoday_inverter_producted_kwh,
            "summarytoday_businessresult" => $summarytoday_businessresult,
            "summary_inverter" => $grid_invertermodel,
            "summary_inverter_countvalue" => $summary_inverter_countvalue,
            "twentyfourhr_linechart" => $twentyfourhr_linechart,
            "grid_invertermodel" => $grid_invertermodel,
            "all_devices" => count($all_devices),
            "count_devicesregister" => count($count_devicesregister),
            "devices_register" => $group_by,
            "year" => intval(DATE("Y")),
            "month" => intval(DATE("m")),
            "day" => intval(DATE("d"))


        );
        // echo '<PRE>';
        // print_r($data);exit();
        $this->template->content->view('dashboard/dashboard', $data);
        $this->template->publish();
    }
}

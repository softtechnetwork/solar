<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . 'libraries/AccountantLibrary.php';
class Main extends AccountantLibrary
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->load->library(array('pagination'));

	}


	public function index()
	{



		date_default_timezone_set('Asia/Bangkok');



		$this->_load_css();

		$this->bootstrapDatepicker();

		$this->loadDatePickerStyle();
		$this->loadDatePickerScript();

		$this->loadDateRangePickerStyle(); // load jquery datepicker rang
		$this->loadDateRangePickerScript(); // load jquery picker


		$this->loadDataTableStyle();
		$this->loadDataTableScript();
		$this->template->stylesheet->add(base_url('assets/css/main.css'));
		$this->template->javascript->add("https://www.gstatic.com/charts/loader.js");


		$users_data = $this->accountantdata;

		$customer_rel_plan = $this->fetch_customerrelplan(null, null, $users_data['id']);
		$customer_accesstoken = $this->get_customeraccesstoken(null, $users_data['id']);
		// $station
		// $api_planlist = $this->oauth2_plan_list($customer_accesstoken->access_token); # get all plan list

		$electric_meter_type = $this->fetch_electric_meter();
		$url = $this->oauth2_url("realTime");
		if (!empty($customer_accesstoken)) {
			if (!empty($customer_rel_plan)) {
				foreach ($customer_rel_plan as $key => $value) {
					$response_api_obj = $this->oauth2_realtime($value->stationId, $url, $customer_accesstoken->access_token);
					$response = json_decode($response_api_obj);
					if (!empty($response)) {
						$value->generationPower = isset($response->generationPower) ? $response->generationPower : 0;
						$value->lastUpdateTime = isset($response->lastUpdateTime) ? $response->lastUpdateTime : '';
						$value->gridPower = empty($response->gridPower) ? 0 : $response->gridPower;
					} else {
						$value->generationPower = 0;
						$value->lastUpdateTime  = 0;
						$value->gridPower = 0;
					}


					// $value->generationPower = 0;
					// $value->lastUpdateTime  = 0;
					// $value->gridPower = 0;

					$value->model    = $value->model == "set default " ? "" : $value->model;
					$value->model_pw = $value->model_pw == "set default " ? "" : $value->model_pw;
					$value->access_token = $customer_accesstoken->access_token;

					# today generation
					//   $value->allPowerGeneration = $value->allPowerGeneration;
				}
			}
		}


		$this->loadDateRangePickerStyle(); // load jquery datepicker rang
		$this->loadDateRangePickerScript(); // load jquery picker
		// echo '<PRE>';
		// print_r($customer_rel_plan);exit();

		$this->template->javascript->add("https://www.gstatic.com/charts/loader.js");
		$this->template->javascript->add("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js");

		$thaimonthname = array();
		for ($i = 1; $i <= 12; $i++) {
			$get_fullthaimonthname = $this->get_thaimonthname($i); // get full thai monthname
			$get_shortmonthname    = $this->get_thaimonthname($i, "short");

			$arr = array(
				"long" => $get_fullthaimonthname,
				"short" => $get_shortmonthname
			);
			array_push($thaimonthname, $arr);
		}


		// echo '<PRE>';print_r($customer_rel_plan);exit();
		$data  = array(
			"customer_rel_plan" => $customer_rel_plan,
			"googlemap" => $customer_rel_plan,
			"thaimonthname" => $thaimonthname
		);

		$this->template->content->view('main', $data);
		$this->template->javascript->add(base_url('assets/js/main.js')); // load 
		$this->template->javascript->add(base_url('assets/js/core.js')); // load 
		$this->template->publish();
	}


	public function ajax_exportdata()
	{
		require_once APPPATH . 'third_party/PHPExcel/Classes/PHPExcel.php';


		$station_arr  = json_decode($_GET['station_arr']);
		$type         = $_GET['type'];
		$month = "";
		$year = "";
		$filter_date        = isset($_GET['filter_date']) ? $_GET['filter_date'] : "";
		$month_report = array();
		if ($type == "month") {
			$exp  = 	explode("/", $filter_date);
			$month =     $exp[0];
			$year =     $exp[1];
			$timeType = 2;
			$month_report = $this->list_everydate_of_month($year,  $month);
			$startDate = $month_report[key($month_report)];
			$endDate   = end($month_report);
		} else {
			$year =     $filter_date;
			# case : get history data year generation power

			$timeType = 3;
			$startDate = $year . '-1';
			$endDate   = $year . '-12';
		}



		$filename = 'SolarReport' . $type . '_' . date('YmdHis') . '.xlsx';
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();


		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Solar Report")
			->setLastModifiedBy("ADMIN")
			->setTitle($filename)
			->setSubject("Excel file for Solar report")
			->setDescription("Excel file for Solar report")
			->setKeywords("Solar Report")
			->setCategory("Solar REport");



		$query_solar  = $this->db->select('plan_list.station_id,plan_list.name_solarapp,setting_electricmeter.unitprice,
		(select top 1 access_token from access_token 
		where access_token.auth_account_id = "oauth_account_rel_planlist".auth_account_id
		order by id desc
		) as access_token
		')->from('plan_list')
			->join('oauth_account_rel_planlist', 'oauth_account_rel_planlist.station_id = plan_list.station_id')
			->join('station', 'station.stationSolarId = plan_list.station_id')
			->join('setting_electricmeter' , 'station.electric_meter_type = setting_electricmeter.electric_meter_type' , 'left')
			->where_in('plan_list.station_id', $station_arr)
			->get();

		// echo $this->db->last_query();exit();

		$url = $this->oauth2_url("history"); # get url 
		$sheet = 0;
		//for($year = 2020; $year<=2022; $year++){ // start loop year

		$data = array();
	
		$objPHPExcel->createSheet();
		$activesheet = $objPHPExcel->setActiveSheetIndex($sheet);
		$activesheet->setTitle("รายงานการผลิตไฟ");
		/**============================= sheet จำนวนเงิน ============================= */
		# case  : set new sheet
		$objsheetenergyused = $objPHPExcel->createSheet(1); // set sheet 2
		$objsheetenergyused->setTitle("จำนวนเงินประหยัดได้");			// Rename sheet
	    /**============================= (eof) sheet จำนวนเงิน ============================= */

		# case : set first column
		$activesheet->setCellValueByColumnAndRow(0, 1, 'ลำดับ');

		$colIndex = PHPExcel_Cell::stringFromColumnIndex(0); # case : get column string name
		$activesheet->getColumnDimension("{$colIndex}")->setWidth(10); # set column width
		$activesheet->mergeCells('A1:A2');
		$activesheet->getStyle("A1:A2")->applyFromArray(
			array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
				)
			)
		);
		# case : sheet (จำนวนเงิน)  set header
		# case : set first column
		$objsheetenergyused->setCellValueByColumnAndRow(0, 1, 'ลำดับ');

		$colIndex = PHPExcel_Cell::stringFromColumnIndex(0); # case : get column string name

		$objsheetenergyused->getColumnDimension("{$colIndex}")->setWidth(10); # set column width
		$objsheetenergyused->mergeCells('A1:A2');
		$objsheetenergyused->getStyle("A1:A2")->applyFromArray(
			array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
				)
			)
		);

		$activesheet->setCellValueByColumnAndRow(1, 1, 'Inverter');
		$colIndex = PHPExcel_Cell::stringFromColumnIndex(1); # case : get column string name
		$activesheet->getColumnDimension("{$colIndex}")->setWidth(25); # set column width
		$activesheet->mergeCells('B1:B2');
		$activesheet->getStyle("B1:B2")->applyFromArray(
			array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
				)
			)
		);
		$objsheetenergyused->setCellValueByColumnAndRow(1, 1, 'Inverter');
		$objsheetenergyused->getColumnDimension("{$colIndex}")->setWidth(25); # set column width
		$objsheetenergyused->mergeCells('B1:B2');
		$objsheetenergyused->getStyle("B1:B2")->applyFromArray(
			array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
				)
			)
		);
		# case : set month cell


		$row = 3;
		$no  = 1;
		$last_colIndex = 1;
		$data_arr = array();
		$max  = array();
		$min  = array();
		$zero = array();
		foreach ($query_solar->result() as $key => $value) {
			/** begin : call api to get history of this day */
			# case : get history data month generation power
			$stationId    = $value->station_id;
			$access_token = $value->access_token;

			$result_json = $this->oauth2_list_history($timeType, $startDate, $endDate, $stationId, $url, $access_token);
			// echo $startDate;exit();
			# case : convert json to array
			$decode_result = json_decode($result_json);


			if (isset($decode_result->stationDataItems)) {
				// echo '<PRE>';
				// print_r($decode_result);exit();
				// case : array sort
				if ($timeType == 2) {
					$stationDataItems = (array)$decode_result->stationDataItems;
					$day = array_column($stationDataItems, 'day');
					array_multisort($day, SORT_ASC, $stationDataItems);

					foreach ($month_report as $month_key => $month_value) {
						$date = explode("-", $month_value);
						$day  = $date[2];
						$day  = intval($day);
						$data_arr[$value->station_id][$day] = array(); # case : date[2] = day
						$find_key = array_search($day, array_column($stationDataItems, 'day'));

						if (is_numeric($find_key)) {
							$data_arr[$value->station_id][$day] = $stationDataItems[$find_key];
							$generationPower = $stationDataItems[$find_key]->generationValue;
							# case : set max array
							$max = $this->set_max($generationPower, $max, $stationId, $day);
							# case : set min array
							$min = $this->set_min($generationPower, $min, $stationId, $day);
						}
					}
				} else {
					$year_fromstartdate  =  explode("-", $startDate); // case  : get year from startdate
					$stationDataItems = (array)$decode_result->stationDataItems;
					$month_sort = array();
					$month_sort = array_column($stationDataItems, 'month');
					array_multisort($month_sort, SORT_ASC, $stationDataItems);
					// echo '<PRE>';print_r($stationDataItems);exit();
					for ($month_value = 1; $month_value <= 12; $month_value++) {

						$data_arr[$value->station_id][$month_value] = array(); # case : date[2] = day
						$find_key = array_search($month_value, array_column($stationDataItems, 'month'));

						if (is_numeric($find_key)) {
							if ($stationDataItems[$find_key]->year == $year_fromstartdate[0]) {
								$data_arr[$value->station_id][$month_value] = $stationDataItems[$find_key];
								$generationPower = $stationDataItems[$find_key]->generationValue;
								# case : set max array
								$max = $this->set_max($generationPower, $max, $stationId, $month_value);
								# case : set min array
								$min = $this->set_min($generationPower, $min, $stationId, $month_value);
							}
						}
					}
				}
			}
		}

	

		foreach ($query_solar->result() as $key => $value) {
		
			
	
			

			# case : get history data month generation power
			$stationId    = $value->station_id;
			$access_token = $value->access_token;
			$unitprice  = $value->unitprice > 0 ?  $value->unitprice :0;

			# case : set data of this station
			$data = $data_arr[$stationId];



			$cell = 2;

			$activesheet->setCellValueByColumnAndRow(0, $row, $no); // case : set number of row.
			$activesheet->setCellValueByColumnAndRow(1, $row, $value->name_solarapp); // case : set station name
			// ====  sheet ( จำนวนเงิน ) =====
			$objsheetenergyused->setCellValueByColumnAndRow(0, $row, $no); // case : set number of row.
			$objsheetenergyused->setCellValueByColumnAndRow(1, $row, $value->name_solarapp); // case : set station name

			# case : merge cell date report

			$last_index    = count($data) + 1;
			$header_title  = $timeType == 2 ? "วันที่" : "ปี " . $year;

			$activesheet->setCellValueByColumnAndRow(2, 1, "{$header_title}");
			$last_colIndex = PHPExcel_Cell::stringFromColumnIndex($last_index); # case : get column string name
			$activesheet->mergeCells("C1:{$last_colIndex}1");

			$activesheet->getStyle("C1:{$last_colIndex}1")->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				)
			);

			// ====  sheet ( จำนวนเงิน ) =====
			$objsheetenergyused->mergeCells("C1:{$last_colIndex}1");
			$objsheetenergyused->getStyle("C1:{$last_colIndex}1")->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				)
			);

			# case : set cell รวม
			$summary_index = $last_index + 1;
			// ====  sheet ( รายงานผลิตไฟ ) =====
			$activesheet->setCellValueByColumnAndRow($summary_index, 1, 'รวม');
			$summary_cellindex = PHPExcel_Cell::stringFromColumnIndex($summary_index); # case : get column string name	
			$activesheet->mergeCells("{$summary_cellindex}1:{$summary_cellindex}2");
			$activesheet->getStyle("{$summary_cellindex}1:{$summary_cellindex}2")->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				)
			);
			// ====  sheet ( จำนวนเงิน ) =====
			$objsheetenergyused->mergeCells("{$summary_cellindex}1:{$summary_cellindex}2");
			$objsheetenergyused->getStyle("{$summary_cellindex}1:{$summary_cellindex}2")->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				)
			);


			foreach ($data as $d_key => $m_value) { # case : looop data



				# set date value
				$date_report = "";
				$generationPower = 0;
				if ($timeType == 2) {
					if (isset($m_value->day) && isset($m_value->month) && isset($m_value->year)) {
						$date_report = $m_value->day . '/' . $m_value->month . '/' . $m_value->year;
						$generationPower = $m_value->generationValue;
					} else {
						$date_report 	 = $d_key . '/' . $month . '/' . $year;
						$generationPower = "ไม่พบข้อมูล";
					}
				} else {
					if (isset($m_value->month)) {
						$date_report = $this->get_thaimonthname($m_value->month, "full");
						$generationPower = $m_value->generationValue;
					} else {
						$date_report 	 = $this->get_thaimonthname($d_key, "full");
						$generationPower = "ไม่พบข้อมูล";
					}
				}
				$activesheet->setCellValueByColumnAndRow($cell, 2, $date_report); # case : set date time report
				$colIndex = PHPExcel_Cell::stringFromColumnIndex($cell); # case : get column string name
				$activesheet->getColumnDimension("{$colIndex}")->setWidth(12); # set column width

				// ===== sheet ( จำนวนเงิน ) ========
				$objsheetenergyused->setCellValueByColumnAndRow($cell, 2, $date_report); # case : set date time report
				$objsheetenergyused->getColumnDimension("{$colIndex}")->setWidth(12);

				$activesheet->setCellValueByColumnAndRow($cell, $row, $generationPower); # case : set generation power
			
				$case_generationPower = floatval($generationPower) > 0 ? floatval($generationPower) : 0;
				$energyused = $case_generationPower * $unitprice;
				
				$energyused = number_format($energyused, 2, '.', '');
				$objsheetenergyused->setCellValueByColumnAndRow($cell, $row, $energyused); # case : set generation power ( sheet จำนวนเงิน)
				$filter  = $timeType == 2  ? $d_key : $d_key; // 
				$this->set_minmaxColor($activesheet, $filter, $max, $min, $stationId, $colIndex, $row);

				++$cell;
			} # case : end loop data

			$activesheet->setCellValueByColumnAndRow($cell, $row, "=SUM(C{$row}:{$last_colIndex}{$row})"); # case : set summary data
			// sheet (จำนวนเงิน ): summary data
			$objsheetenergyused->setCellValueByColumnAndRow($cell, $row, "=SUM(C{$row}:{$last_colIndex}{$row})"); # case : set summary data
			++$no;
			++$row;
		}
		# case : set border style
		$last_row = $row  - 1;
		$activesheet->getStyle("A1:{$summary_cellindex}{$last_row}")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
						'color' => array('rgb' => '000000')
					)
				)
			)
		);

		$objsheetenergyused->getStyle("A1:{$summary_cellindex}{$last_row}")->applyFromArray(
			array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
						'color' => array('rgb' => '000000')
					)
				)
			)
		);

		# case : สีและ lable ผลิตได้สูงสุด color code = 92d051
		$row_max = $row + 1;
		// $activesheet->setCellValueByColumnAndRow(0, $row_max, '');
		// $activesheet->setCellValueByColumnAndRow(1, $row_max, 'ผลิตได้สูงสุด');
		// $colIndex = PHPExcel_Cell::stringFromColumnIndex(0); # case : get column string name
		// $activesheet->getStyle($colIndex . $row_max)->applyFromArray(
		// 	array(
		// 		'fill' => array(
		// 			'type' => PHPExcel_Style_Fill::FILL_SOLID,
		// 			'color' => array('rgb' => '92d051')
		// 		)
		// 	)
		// );

		# case : สีและ lable ผลิตได้ต่ำสุด color code = ffff00
		$row_min = $row + 2;
		// $activesheet->setCellValueByColumnAndRow(0, $row_min, '');
		// $activesheet->setCellValueByColumnAndRow(1, $row_min, 'ผลิตได้ต่ำสุด');
		// $colIndex = PHPExcel_Cell::stringFromColumnIndex(0); # case : get column string name
		// $activesheet->getStyle($colIndex . $row_min)->applyFromArray(
		// 	array(
		// 		'fill' => array(
		// 			'type' => PHPExcel_Style_Fill::FILL_SOLID,
		// 			'color' => array('rgb' => 'ffff00')
		// 		)
		// 	)
		// );
		# case : ไม่พบข้อมูล
		$row_alert = $row + 3;
		$activesheet->setCellValueByColumnAndRow(0, $row_alert, '*หมายเหตุ');
		$activesheet->setCellValueByColumnAndRow(1, $row_alert, 'ไม่พบข้อมูลคือไม่มีข้อมูลการใช้งานภายในช่วงเวลานั้นของinverterตัวนี้(กำลังผลิตเป็นกิโลวัตต์)');

		


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter->save('php://output');


		exit;
	}
	public function set_minmaxColor($activesheet, $day, $max, $min, $stationId, $colIndex, $row)
	{
		// if (isset($max[$day]) || isset($min[$day])) {
		// 	if (isset($max[$day])) {

		// 		if ($max[$day]["station_id"] == $stationId) {
		// 			$activesheet->getStyle($colIndex . $row)->applyFromArray(
		// 				array(
		// 					'fill' => array(
		// 						'type' => PHPExcel_Style_Fill::FILL_SOLID,
		// 						'color' => array('rgb' => '92d051')
		// 					)
		// 				)
		// 			);
		// 		}
		// 	}
		// 	if (isset($min[$day])) {
		// 		if ($min[$day]["station_id"] == $stationId) {
		// 			$activesheet->getStyle($colIndex . $row)->applyFromArray(
		// 				array(
		// 					'fill' => array(
		// 						'type' => PHPExcel_Style_Fill::FILL_SOLID,
		// 						'color' => array('rgb' => 'ffff00')
		// 					)
		// 				)
		// 			);
		// 		}
		// 	}
		// 	# case : มีข้อมูลเดียว
		// 	if ($min[$day]["station_id"] == $stationId && $max[$day]["station_id"] == $stationId) {
		// 		$activesheet->getStyle($colIndex . $row)->applyFromArray(
		// 			array(
		// 				'fill' => array(
		// 					'type' => PHPExcel_Style_Fill::FILL_SOLID,
		// 					'color' => array('rgb' => '92d051')
		// 				)
		// 			)
		// 		);
		// 	}
		// }
	}
	public function set_max($generationPower, $max, $stationId, $day)
	{
		if (!isset($max[$day])) {
			$max[$day] =  array(
				"station_id" => $stationId,
				"generationPower" => $generationPower
			);
		} else {
			//echo $generationPower.' '.floatval($max[$day]["generationPower"]);exit();
			if (floatval($generationPower) > floatval($max[$day]["generationPower"])) {
				$max[$day] =  array(
					"station_id" => $stationId,
					"generationPower" => $generationPower
				);
			}
		}

		return $max;
	}
	public function set_min($generationPower, $min, $stationId, $day)
	{
		if (!isset($min[$day])) {
			$min[$day] =  array(
				"station_id" => $stationId,
				"generationPower" => $generationPower
			);
		} else {
			if (floatval($generationPower) < floatval($min[$day]["generationPower"])) {
				$min[$day] =  array(
					"station_id" => $stationId,
					"generationPower" => $generationPower
				);
			}
		}
		return $min;
	}

	public function set_zero($generationPower, $min, $stationId, $day)
	{
		$zero[$day][$stationId] =  array(
			"station_id" => $stationId,
			"generationPower" => 0
		);
		return $min;
	}
	public function get_monthname($m = null)
	{

		$arr = array(
			"1" => "Jan",
			"2" => "Feb",
			"3" => "Mar",
			"4" => "Apr",
			"5" => "May",
			"6" => "Jun",
			"7" => "Jul",
			"8" => "Aug",
			"9" => "Sep",
			"10" => "Oct",
			"11" => "Nov",
			"12" => "Dec"
		);
		if (!empty($m)) {
			return $arr[$m];
		}
		return $arr;
	}
}

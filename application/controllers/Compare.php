<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . 'libraries/AccountantLibrary.php';
class Compare extends AccountantLibrary
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->load->library(array('pagination'));

	}


	public function index()
	{



		date_default_timezone_set('Asia/Bangkok');



		$this->_load_css();

		$this->bootstrapDatepicker();

		$this->loadDatePickerStyle();
		$this->loadDatePickerScript();

		$this->loadDateRangePickerStyle(); // load jquery datepicker rang
		$this->loadDateRangePickerScript(); // load jquery picker


		$this->loadDataTableStyle();
		$this->loadDataTableScript();
		$this->template->stylesheet->add(base_url('assets/css/compare.css'));
		


		$this->loadDateRangePickerStyle(); // load jquery datepicker rang
		$this->loadDateRangePickerScript(); // load jquery picker


		$this->template->javascript->add("https://www.gstatic.com/charts/loader.js");
		$this->template->javascript->add("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js");

		$users_data = $this->accountantdata;
	
		$customer_rel_plan = $this->fetch_customerrelplan(null, null, $users_data['id']);
		$customer_accesstoken = $this->get_customeraccesstoken(null, $users_data['id']);
		$customer_lastselected    = $this->fetch_userbehaviour($users_data['id']);
		
		//echo '<PRE>';print_r($customer_rel_plan);exit();
		$data  = array(
			"customer_rel_plan" => $customer_rel_plan,
			"user_id"   => $users_data['id'],
			"customer_lastselected" => $customer_lastselected
		);

		$this->template->content->view('compare', $data);

		$this->template->javascript->add(base_url('assets/js/compare.js')); // load  compare js
		$this->template->javascript->add(base_url('assets/js/core.js')); // load core js
		$this->template->publish();
	}
	function ajax_comparedata(){
		$station_object = array();
		if(isset($_POST['station_arr'])){
			if(!empty($_POST['station_arr'])){
				foreach($_POST['station_arr'] as $key => $value){
					$_POST['stationId'] = $value;
					$station_object[$key] = $this->ajax_curlgetsolarreport( $_POST );
				}

				if(!empty($station_object)){
					echo json_encode(array("status" => true, "data" => json_encode($station_object)));
				}else{
					echo json_encode(array("status" => false ));
				}
				
			}else{
				echo json_encode(array("status" => false ));
			}
		}
		
		
	}

	function ajax_saveuserbehavior(){
		$station_object = array();
	
		if(isset($_POST['station_arr'])){
			if(!empty($_POST['station_arr'])){
			

				$this->db->insert(
					'customers_comparedata_selected',
					array(
						"customer_id"  => $_POST['user_id'],
						"compare_data" => json_encode($_POST['station_arr']),
						"timeType"     => $_POST['timeType'],
						"created"      => DATE("Y-m-d H:i:s")
					)
				);

				if($this->db->affected_rows() != 1){
					echo json_encode(array("status" => false));
				}else{
					echo json_encode(array("status" => true ));
				}
				
			}else{
				echo json_encode(array("status" => false ));
			}
		}
		
		
	}


	public function fetch_userbehaviour($user_id){
		$query = $this->db->select('*')->from('customers_comparedata_selected')
		->where('customer_id' , $user_id)
		->order_by('id','desc')
		->limit(1)
		->get();
		if($query->num_rows() > 0){
			$row =  $query->row();
			return $row;
		}else{
			return 0;
		}
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['arr_dashboard'] = array(
	'kanlaya@psisat.com',
	'jantima@psisat.com'
);

$config['arr_administrator'] = array(
	'kridsada@psisat.com'
);

$config['arr_job_approved'] = array(
	'kanlaya@psisat.com'
);

$config['arr_job_pending'] = array(
	'jantima@psisat.com',
	'piyanush@psisat.com',
	'thanittha@psisat.com',
	'anutsara@psisat.com'
);

$config['arr_job_paid'] = array(
	'kanlaya@psisat.com',
	'jantima@psisat.com'
);
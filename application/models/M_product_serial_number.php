<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_product_serial_number extends DataMapper {

    //put your code here
    var $table = 'product_serial_number';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'product_type' => array(
   //           'class' => 'M_product_type',
   //             'other_field' => 'product',
   //             'join_other_as' => 'product_type',
   //             'join_table' => 'product_type'
   //         )
   // );
    
    var $has_many = array(
       // 'technicial_job' => array(
       //     'class' => 'M_technicial_job',
       //     'other_field' => 'product',
       //     'join_self_as' => 'product',
       //     'join_other_as' => 'product',
       //     'join_table' => 'technicial_job'
       //  ),
       'technicial_job'=> array(
           'class' => 'M_technicial_job',
           'other_field' => 'product_serial_number',
           'join_self_as' => 'product_serial_number',
           'join_other_as' => 'product_serial_number',
           'join_table' => 'technicial_job'
       )

    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_technicial_job extends DataMapper {

    //put your code here
    var $table = 'technicial_job';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'partner'=>array(
              'class' => 'M_partner',
               'other_field' => 'technicial_job',
               'join_other_as' => 'partner',
               'join_table' => 'partner'
           ),
           'technicial' => array(
               'class' => 'M_technicial',
               'other_field' => 'technicial_job',
               'join_other_as' => 'technicial',
               'join_table' => 'technicial'
           ),
           'customer'=>array(
               'class' => 'M_customer',
               'other_field' => 'technicial_job',
               'join_other_as' => 'customer',
               'join_table' => 'customer'
           ),
           'product'=>array(
               'class' => 'M_product',
               'other_field' => 'technicial_job',
               'join_other_as' => 'product',
               'join_table' => 'product'
           ),
           'technicial_skill'=>array(
                'class'=>'M_technicial_skill',
                'other_field'=>'technicial_job',
                'join_other_as'=>'technicial_skill',
                'join_table'=>'technicial_skill'
           ),
           'product_serial_number'=>array(
                'class'=>'M_product_serial_number',
                'other_field'=>'technicial_job',
                'join_other_as'=>'product_serial_number',
                'join_table'=>'product_serial_number'
           )

   );
    
    var $has_many = array(
       'technicial_job_image' => array(
           'class' => 'M_technicial_job_image',
           'other_field' => 'technicial_job',
           'join_self_as' => 'technicial_job',
           'join_other_as' => 'technicial_job',
           'join_table' => 'technicial_job_image'
         )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
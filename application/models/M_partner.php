<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_partner extends DataMapper {

    //put your code here
    var $table = 'partner';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'technicial_job'=>array(
           'class' => 'M_technicial_job',
           'other_field' => 'partner',
           'join_self_as' => 'partner',
           'join_other_as' => 'partner',
           'join_table' => 'technicial_job'
       ),
       'technicial_type' => array(
           'class' => 'M_technicial_type',
           'other_field' => 'partner',
           'join_self_as' => 'partner',
           'join_other_as' => 'partner',
           'join_table' => 'technicial_type'
       ),
       'user' => array(
           'class' => 'M_user',
           'other_field' => 'partner',
           'join_self_as' => 'partner',
           'join_other_as' => 'partner',
           'join_table' => 'user'
       )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
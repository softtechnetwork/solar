<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_user_menu_event extends DataMapper {

    //put your code here
    var $table = 'user_menu_event';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'user_menu' => array(
             'class' => 'M_user_menu',
               'other_field' => 'user_menu_event',
               'join_other_as' => 'user_menu',
               'join_table' => 'user_menu'
           )
   );
    
    // var $has_many = array(
    //    'customer' => array(
    //        'class' => 'M_customer',
    //        'other_field' => 'province',
    //        'join_self_as' => 'province',
    //        'join_other_as' => 'province',
    //        'join_table' => 'customer'
    //    )
    // );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
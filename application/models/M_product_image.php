<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_product_image extends DataMapper {

    //put your code here
    var $table = 'product_image';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'product' => array(
             'class' => 'M_product',
               'other_field' => 'product_image',
               'join_other_as' => 'product',
               'join_table' => 'product'
           )
   );
    
    // var $has_many = array(
    //    'technicial_job' => array(
    //        'class' => 'M_technicial_job',
    //        'other_field' => 'product',
    //        'join_self_as' => 'product',
    //        'join_other_as' => 'product',
    //        'join_table' => 'technicial_job')
    // );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
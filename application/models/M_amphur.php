<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_amphur extends DataMapper {

    //put your code here
    var $table = 'amphur';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'customer' => array(
           'class' => 'M_customer',
           'other_field' => 'amphur',
           'join_self_as' => 'amphur',
           'join_other_as' => 'amphur',
           'join_table' => 'customer'
       ),
       'technicial_address' => array(
           'class' => 'M_technicial_address',
           'other_field' => 'amphur',
           'join_self_as' => 'amphur',
           'join_other_as' => 'amphur',
           'join_table' => 'technicial_address'
       )

    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
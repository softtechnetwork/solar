<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_technicial extends DataMapper {

    //put your code here
    var $table = 'technicial';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'technicial' => array(
   //           'class' => 'M_technicial',
   //             'other_field' => 'technicial_jobc',
   //             'join_other_as' => 'technicial',
   //             'join_table' => 'technicial'
   //         )
   // );
    
    var $has_many = array(
       'technicial_job' => array(
           'class' => 'M_technicial_job',
           'other_field' => 'technicial',
           'join_self_as' => 'technicial',
           'join_other_as' => 'technicial',
           'join_table' => 'technicial_job'
        ),
       'technicial_address' => array(
           'class' => 'M_technicial_address',
           'other_field' => 'technicial',
           'join_self_as' => 'technicial',
           'join_other_as' => 'technicial',
           'join_table' => 'technicial_address'
        )

    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
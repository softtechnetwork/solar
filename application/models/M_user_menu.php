<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_user_menu extends DataMapper {

    //put your code here
    var $table = 'user_menu';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'user_menu_event' => array(
           'class' => 'M_user_menu_event',
           'other_field' => 'user_menu',
           'join_self_as' => 'user_menu',
           'join_other_as' => 'user_menu',
           'join_table' => 'user_menu_event'
       ),
       'user_menu_permission' => array(
           'class' => 'M_user_menu_permission',
           'other_field' => 'user_menu',
           'join_self_as' => 'user_menu',
           'join_other_as' => 'user_menu',
           'join_table' => 'user_menu_permission'
       )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
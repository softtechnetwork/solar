<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_technicial_skill extends DataMapper {

    //put your code here
    var $table = 'technicial_skill';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'technicial_type' => array(
             'class' => 'M_technicial_type',
               'other_field' => 'technicial_skill',
               'join_other_as' => 'technicial_type',
               'join_table' => 'technicial_type'
           )
   );
    
    var $has_many = array(
       'technicial_skill_image_task' => array(
           'class' => 'M_technicial_skill_image_task',
           'other_field' => 'technicial_skill',
           'join_self_as' => 'technicial_skill',
           'join_other_as' => 'technicial_skill',
           'join_table' => 'technicial_skill_image_task'
        ),
       'technicial_has_skill'=>array(
          'class'=>'M_technicial_has_skill',
          'other_field'=>'technicial_skill',
          'join_self_as'=>'technicial_skill',
          'join_other_as'=>'technicial_skill',
          'join_table'=>'technicial_has_skill'
       ),
       'technicial_job'=>array(
          'class'=>'M_technicial_job',
          'other_field'=>'technicial_skill',
          'join_self_as'=>'technicial_skill',
          'join_other_as'=>'technicial_skill',
          'join_table'=>'technicial_job'
       )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_product_type extends DataMapper {

    //put your code here
    var $table = 'product_type';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'product' => array(
           'class' => 'M_product',
           'other_field' => 'product_type',
           'join_self_as' => 'product_type',
           'join_other_as' => 'product_type',
           'join_table' => 'product'
        )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
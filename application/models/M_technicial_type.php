<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_technicial_type extends DataMapper {

    //put your code here
    var $table = 'technicial_type';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'partner' => array(
             'class' => 'M_partner',
               'other_field' => 'technicial_type',
               'join_other_as' => 'partner',
               'join_table' => 'partner'
           )
   );
    
    var $has_many = array(
       'technicial_skill' => array(
           'class' => 'M_technicial_skill',
           'other_field' => 'technicial_type',
           'join_self_as' => 'technicial_type',
           'join_other_as' => 'technicial_type',
           'join_table' => 'technicial_skill'
        )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_technicial_address extends DataMapper {

    //put your code here
    var $table = 'technicial_address';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'technicial' => array(
             'class' => 'M_technicial',
               'other_field' => 'technicial_address',
               'join_other_as' => 'technicial',
               'join_table' => 'technicial'
           ),
           'district'=>array(
              'class' => 'M_district',
               'other_field' => 'technicial_address',
               'join_other_as' => 'district',
               'join_table' => 'district'
           ),
           'amphur'=>array(
              'class' => 'M_amphur',
               'other_field' => 'technicial_address',
               'join_other_as' => 'amphur',
               'join_table' => 'amphur'
           ),
           'province'=>array(
              'class' => 'M_province',
               'other_field' => 'technicial_address',
               'join_other_as' => 'province',
               'join_table' => 'province'
           )
   );
    
    // var $has_many = array(
    //    'technicial_job' => array(
    //        'class' => 'M_technicial_job',
    //        'other_field' => 'technicial',
    //        'join_self_as' => 'technicial',
    //        'join_other_as' => 'technicial',
    //        'join_table' => 'technicial_job'
    //     )
    // );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_technicial_skill_image_task extends DataMapper {

    //put your code here
    var $table = 'technicial_skill_image_task';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'technicial_skill' => array(
             'class' => 'M_technicial_skill',
               'other_field' => 'technicial_skill_image_task',
               'join_other_as' => 'technicial_skill',
               'join_table' => 'technicial_skill'
           )
   );
    
    // var $has_many = array(
    //    'technicial_job' => array(
    //        'class' => 'M_technicial_job',
    //        'other_field' => 'technicial',
    //        'join_self_as' => 'technicial',
    //        'join_other_as' => 'technicial',
    //        'join_table' => 'technicial_job'
    //     )
    // );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
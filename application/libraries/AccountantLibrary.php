<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class AccountantLibrary extends CI_Controller
{

    public $meta_title = '';
    public $meta_keywords = '';
    public $meta_description = '';
    public $data = array();
    public $page_num = 10;
    public $controller = '';
    public $method = '';
    public $language = 'th';
    public $default_language;
    public $site_setting = '';
    public $partner_data = array();
    public $accountantdata = array();
    public $controller_available = array();
    public $controller_method_available = array();
    private $conn_mysql;
    public $event_ability;
    public $api_url = "";

    public $arr_administrator = array();
    public $arr_dashboard = array();
    public $arr_job_approved  = array();
    public $arr_job_pending  = array();
    public $arr_job_paid = array();

    public function __construct()
    {
        //echo 'aaaa';exit;
        parent::__construct();
        $CI = &get_instance();
        $this->setController($CI->router->class);
        $this->setMethod($CI->router->method);



        $this->load->library(array('Msg', 'Lang_controller'));
        $this->load->helper(array('lang', 'our', 'inflector', 'cookie'));


        $this->template->set_template('template');


        //print_r($this->session->userdata());exit;

        $this->_checkAlready_signin();
        $this->_getAccountData();
        $this->_setApiUrl();
        $this->_initialAccountPermission();

        $this->_load_js();
        $this->_load_css();

        // print_r($this->session->userdata());


    }

    /**
     * load javascript
     */
    // public function _load_js() {

    //     $this->template->javascript->add(base_url('assets/js/vendor/jquery-2.1.4.min.js'));
    //     $this->template->javascript->add(base_url('assets/js/popper.min.js'));
    //     $this->template->javascript->add(base_url('assets/js/plugins.js'));
    //     $this->loadTypeAheadScript();
    //     $this->template->javascript->add(base_url('assets/js/main.js'));



    // }
    public function _load_js()
    {
        $this->template->javascript->add(base_url('assets/vendor/jquery/jquery.min.js'));
        $this->template->javascript->add(base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'));
        // $this->template->javascript->add(base_url('assets/js/sb-admin.min.js'));
    }
    /**
     * load style sheet
     */
    public function _load_css()
    {

        $this->template->stylesheet->add(base_url('assets/css/normalize.css'));
        $this->template->stylesheet->add(base_url('assets/css/bootstrap.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/font-awesome.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/themify-icons.css'));
        $this->template->stylesheet->add(base_url('assets/css/flag-icon.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/cs-skin-elastic.css'));
        //$this->template->stylesheet->add(base_url('assets/css/bootstrap-select.less'));
        $this->template->stylesheet->add(base_url('assets/css/style.css'));
        $this->template->stylesheet->add('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800');
        $this->template->stylesheet->add(base_url('assets/css/additional_style.css'));
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-select.min.css'));
    }

    public function loadSweetAlert()
    {
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
    }

    public function loadValidator()
    {
        $this->template->stylesheet->add(base_url('assets/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/bootstrapValidator.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/language/th_TH.js'));
    }
    public function loadImageUploadStyle()
    {
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_common.css'));
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_hover.css'));
    }
    public function loadImageUploadScript()
    {
        $this->template->javascript->add(base_url('assets/js/fileupload_script.js'));
    }
    public function loadDataTableStyle()
    {
        $this->template->stylesheet->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.css'));
        $this->template->stylesheet->add(base_url('assets/vendor/datatables/DataTables-1.10.23/css/rowReorder.dataTables.min.css'));
        $this->template->stylesheet->add(base_url('assets/vendor/datatables/DataTables-1.10.23/css/responsive.dataTables.min.css'));
    }
    public function loadDataTableScript()
    {
        $this->template->javascript->add(base_url('assets/vendor/datatables/jquery.dataTables.js'));
        $this->template->javascript->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.js'));
        $this->template->javascript->add(base_url('assets/js/enable_datatable.js'));


        $this->template->javascript->add(base_url('assets/vendor/datatables/DataTables-1.10.23/js/dataTables.rowReorder.min.js'));
        $this->template->javascript->add(base_url('assets/vendor/datatables/DataTables-1.10.23/js/dataTables.responsive.min.js'));
    }

    public function loadDateRangePickerStyle()
    {
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-daterangepicker/daterangepicker.css'));
    }
    public function loadDateRangePickerScript()
    {
        $this->template->javascript->add(base_url('assets/js/bootstrap-daterangepicker/moment.min.js'));
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');
        $this->template->javascript->add(base_url('assets/js/bootstrap-daterangepicker/daterangepicker.js'));
    }

    public function loadDatePickerStyle()
    {
        $this->template->stylesheet->add(base_url('assets/vendor/jquery-ui-1.12.1/jquery-ui.css'));
    }

    public function bootstrapDatepicker()
    {

        $this->template->stylesheet->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css');
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js');
    }

    public function loadDatePickerScript()
    {

        $this->template->javascript->add(base_url('assets/vendor/jquery-ui-1.12.1/jquery-ui.js'));
        $this->template->javascript->add(base_url('assets/vendor/jquery-ui-1.12.1/jquery-ui.min.js'));
    }

    public function loadTempusDominusStyle()
    {
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-tempusdominus/tempusdominus-bootstrap-4.min.css'));
    }
    public function loadTempusDominusScript()
    {
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment.min.js');
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');

        $this->template->javascript->add(base_url('assets/js/bootstrap-tempusdominus/tempusdominus-bootstrap-4.min.js'));
    }

    public function loadTypeAheadScript()
    {
        $this->template->javascript->add(base_url('assets/js/typeahead/bootstrap3-typeahead.min.js'));
    }

    public function loadTagsInputStyle()
    {
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-tagsinput/bootstrap-tagsinput.css'));
    }
    public function loadTagsInputScript()
    {
        $this->template->javascript->add(base_url('assets/js/bootstrap-tagsinput/bootstrap-tagsinput.min.js'));
    }

    protected function loadBootstrapFileuploadMasterScript()
    {
        //$this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/plugins/sortable.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/fileinput.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/locales/th.js'));

        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/themes/explorer-fa/theme.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/themes/fa/theme.js'));
    }
    protected function loadBootstrapFileuploadMasterStyle()
    {
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-fileupload-master/fileinput.min.css'));

        $this->template->stylesheet->add(base_url('assets/js/bootstrap-fileupload-master/themes/explorer-fa/theme.css'));
    }

    public function loadChartJsScript()
    {
        $this->template->javascript->add(base_url('assets/vendor/chart.js/Chart.min.js'));
    }
    public function _checkAlready_signin()
    {
        if (!$this->session->userdata('accountantdata')) {
            redirect(base_url());
        }
    }

    public function _getAccountData()
    {
        if ($this->session->userdata('accountantdata')) {
            $this->accountantdata = $this->session->userdata('accountantdata');
        }
    }

    private function _initialAccountPermission()
    {
        $ci = &get_instance();
        $ci->load->config('account');

        //print_r($ci->config->item('arr_administrator'));exit;
        $this->arr_administrator = $ci->config->item('arr_administrator');
        $this->arr_dashboard = $ci->config->item('arr_dashboard');
        $this->arr_job_approved = $ci->config->item('arr_job_approved');
        $this->arr_job_pending = $ci->config->item('arr_job_pending');
        $this->arr_job_paid = $ci->config->item('arr_job_paid');

        //print_r($this->arr_administrator);exit;

    }

    private function setControllerAvailable()
    {
        if ($this->session->userdata('controller_available')) {
            $this->controller_available = $this->session->userdata('controller_available');
        }

        $this->setControllerMethodAvailable();
    }
    private function setControllerMethodAvailable()
    {
        if ($this->session->userdata('controller_method_available')) {
            $this->controller_method_available = $this->session->userdata('controller_method_available');
        }
    }

    private function _setApiUrl()
    {
        switch (ENVIRONMENT) {
            case 'development':
                $this->api_url = 'http://psitdc.development/';
                break;
            case 'testing':

                break;
            case 'production':
                $this->api_url = 'https://apichaang.psi.co.th/';
                break;

            default:
                # code...
                break;
        }
    }
    /**
     * change lang system
     */
    public function change_lang()
    {

        if ($this->input->post(NULL, FALSE)) {
            $this->session->set_userdata('language', $this->input->post('language'));
        }
    }

    /**
     *
     * @param <type> $file_view  ชื่อ file view
     * @param <type> $return true = return view �?ลับมา�?สดงเรย / false = ไม่ return view �?ลับ
     */
    public function setView($file_view = null, $return = false)
    {
        if ($return)
            $this->load->view($file_view, $this->getData());
        else
            $this->setData('the_content', $this->load->view($file_view, $this->getData(), true));
    }

    /**
     * �?สดง view
     */
    public function getView()
    {
        $this->load->view($this->getTheme(), $this->getData());
    }

    /**
     *
     * @return them template
     */
    public function getTheme()
    {
        return $this->theme;
    }

    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    public function getMeta_title()
    {
        return $this->meta_title;
    }

    public function setMeta_title($meta_title)
    {
        $this->setData('meta_title', $meta_title);
        $this->meta_title = $meta_title;
    }

    public function getMeta_keywords()
    {
        return $this->meta_keywords;
    }

    public function setMeta_keywords($meta_keywords)
    {
        $this->setData('meta_keywords', $meta_keywords);
        $this->meta_keywords = $meta_keywords;
    }

    public function getMeta_description()
    {
        return $this->meta_description;
    }

    public function setMeta_description($meta_description)
    {
        $this->setData('meta_description', $meta_description);
        $this->meta_description = $meta_description;
    }

    public function getData($key = null)
    {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data)
    {
        $this->data[$key] = $data;
    }

    public function getPage_num()
    {
        return $this->page_num;
    }

    public function setPage_num($page_num)
    {
        $this->page_num = $page_num;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function setController($controller)
    {
        $this->setData('controller', $controller);
        $this->controller = $controller;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method)
    {
        $this->setData('method', $method);
        $this->method = $method;
    }

    /**
     * จัด�?ารเรื่อง pagination
     * @param <type> $total จำนวน�?ถวทั้งหมด
     * @param <type> $cur_page หน้าปัจจุบัน
     */
    public function config_page($total, $cur_page)
    {
        $config['base_url'] = base_url();
        $config['total_rows'] = $total;
        $config['per_page'] = $this->page_num;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="links">';
        $config['num_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li class="paging_btn">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = __('Next', 'b2c_default') . ' »';
        $config['next_tag_open'] = '<li class="paging_btn">';
        $config['next_tag_close'] = '</li>';
        $config['num_links'] = 5;

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = __('First', 'b2c_default');
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['last_link'] = __('Last', 'b2c_default');
        $config['cur_page'] = $cur_page;
        $this->data['config_page'] = $config;
        $this->setData('config_page', $config);
    }

    public function onaction_set_returnstationitem($decode_result, $unit_price, $timeType)
    {
        $stationDataItems = $decode_result->stationDataItems;
        $ii = 0;

        while ($ii < count($stationDataItems)) {

            $y = date('Y', $stationDataItems[$ii]->dateTime);
            $m = date('m', $stationDataItems[$ii]->dateTime);
            $d = date('d', $stationDataItems[$ii]->dateTime);

            $h = date('H', $stationDataItems[$ii]->dateTime);
            $i = date('i', $stationDataItems[$ii]->dateTime);
            $s = date('s', $stationDataItems[$ii]->dateTime);
            //echo $stationDataItems[$ii]->dateTime;exit();
            $stationDataItems[$ii]->y = intval($y);
            $stationDataItems[$ii]->m = intval($m);
            $stationDataItems[$ii]->d = intval($d);

            $stationDataItems[$ii]->h = intval($h);
            $stationDataItems[$ii]->i = intval($i);
            $stationDataItems[$ii]->s = intval($s);

            if ($timeType == 1) {
                $stationDataItems[$ii]->dateTime =  date('Y-m-d H:i:s', $stationDataItems[$ii]->dateTime) . '';
            } else {
                $stationDataItems[$ii]->dateTime =  $stationDataItems[$ii]->year . '-' . $stationDataItems[$ii]->month . '-' . $stationDataItems[$ii]->day;
            }

            $generationPower = $timeType == 1 ? $stationDataItems[$ii]->generationPower : $stationDataItems[$ii]->generationValue;

            # case : if time type = 1 
            if ($timeType == 1) {
                $eco_price =  floatval($unit_price)  * (floatval($generationPower) / 10000);
            } else {
                $eco_price =  floatval($unit_price)  * (floatval($generationPower));
            }
            $stationDataItems[$ii]->eco_price = number_format((float)$eco_price, 2, '.', '');
            //$stationDataItems[$ii]->eco_price = $eco_price;
            $stationDataItems[$ii]->timeType = $timeType;

            ++$ii;
        }

        return $stationDataItems;
    }
    public function oauth2_url($case)
    {
        switch ($case) {
            case 'auth_user':
                return 'https://api.solarmanpv.com/account/v1.0/token?language=en&appId=202011259123122';

            case 'plan_list':
                return 'https://api.solarmanpv.com/station/v1.0/list';
            case 'devices':
                return 'https://api.solarmanpv.com/station/v1.0/device';
            case 'history':
                return 'https://api.solarmanpv.com/station/v1.0/history?language=en';
            case 'realTime':
                return 'https://api.solarmanpv.com/station/v1.0/realTime';

            default:
                return;
        }
    }




    public function get_customeraccesstoken($auth_account_id = null, $customer_id = null)
    {
        if (!empty($customer_id)) {
            $query_getcustomer = $this->db->select('customers.* , oauth_account.id as auth_account_id')->from('customers')
                ->join('oauth_account', 'customers.email =  oauth_account.email')
                ->where('customers.id', $customer_id)->get();

            $customers_obj  = $query_getcustomer->row();
            $auth_account_id = $customers_obj->auth_account_id;
        }

        $query_gettoken = $this->db->select('*')->from('access_token')->where('auth_account_id', $auth_account_id)
            ->order_by('id', 'desc')->limit(1)->get();

        return $query_gettoken->row();
    }

    public function fetch_stationrelplaninput($station_id = null)
    {

        $query_result = $this->db->select('station.* , , (select top 1 unitprice from setting_electricmeter where station.electric_meter_type = setting_electricmeter.electric_meter_type) as unit_price')->from('plan_list')
            ->join('station', 'plan_list.station_id = station.stationSolarId')
            ->where('plan_list.station_id', $station_id)
            ->order_by('id', 'desc')->limit(1)->get();
        if ($query_result->num_rows() > 0) {
            return $query_result->result();
        } else {
            return false;
        }
    }

    public function fetch_stationdevicesn($station_id = null, $devices_id = null, $deviceType = null, $station_arr = array(), $deviceSn = null)
    {

        $query = $this->db->select('devices.* , devices_rel_sn.*')->from('devices')
            ->join('devices_rel_sn', 'devices_rel_sn.deviceId = devices.deviceId');

        if (!empty($station_id)) {
            $query = $query->where('devices_rel_sn.stationId', $station_id);
        }
        if (!empty($deviceType)) {
            $query = $query->where('devices.deviceType', $deviceType);
        }

        if (!empty($station_arr)) {
            $query = $query->where_in('devices_rel_sn.stationId', $station_arr);
        }

        $query = $query->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function dashboard_groupbyinverter($limit = null, $start = null, $queryString = null, $id = null, $relation = null, $station_id  = null, $station_arr = array(), $deviceSn = null)
    {
        if (!empty($queryString)) {
            $start = 0;
        }

        $query = $this->db->select('count(*) as count_inverter , model_SolarApp as model ')
            ->from('devices')
            ->join('devices_rel_sn ', 'devices.deviceId = devices_rel_sn.deviceId')
            ->join('plan_list', 'devices_rel_sn.stationId = plan_list.station_id')
            ->join('station', 'plan_list.station_id = station.stationSolarId')
            ->group_by("model_SolarApp");

        $query = $query->where('deviceType',  'COLLECTOR');
        $query = $query->get();
        // echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }


            return $data;
        }

        return false;
    }

    public function fetch_planstationoauth($limit = null, $start = null, $queryString = null, $id = null, $relation = null, $station_id  = null, $station_arr = array(), $deviceSn = null)
    {
        if (!empty($queryString)) {
            $start = 0;
        }

        $query = $this->db->select('plan_list.* , plan_list.generationPower as plan_generationPower ,station.allPowerGeneration_SolarApp as allPowerGeneration, station.unit_priceSolarApp as unit_price , model_SolarApp as model , model_power_SolarApp as model_pw , allPowerGeneration_SolarApp as allPowerGeneration_SolarApp ')
            ->from('plan_list')
            ->join('station', 'plan_list.station_id = station.stationSolarId')
            ->join('oauth_account_rel_planlist', 'plan_list.station_id = oauth_account_rel_planlist.station_id');
        $query = $query->get();
        // echo $this->db->last_query();exit();


        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $row->datestart = date("Y-m-d", $row->createdDate);
                $row->cv_lastUpdateTime = date("Y/m/d H:i:s", $row->lastUpdateTime);

                $data[] = $row;
            }


            return $data;
        }

        return false;
    }


    public function fetch_report_summarypwhr($year, $month, $day)
    {
        $subquery = "( select hr,station_id,MAX(generationPower) as generationpw  from report_summaryhr_planlist
                where year = {$year} and month = {$month}  and day = {$day}
                group by station_id,hr
                )r1";
        $query = $this->db->select("sum(CAST(r1.generationpw as bigint)) as watt,hr ")
            ->from("  {$subquery} ")
            ->group_by('r1.hr');

        $query = $query->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function fetch_gridinvertermodel($id = null)
    {
        $query = $this->db->select('grid_tile_inverter_model.*')
            ->from('grid_tile_inverter_model');

        if (!empty($id)) {
            $query = $query->where('grid_tile_inverter_model.id', $id);
        }
        $query = $query->order_by("no", "asc");
        $query = $query->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }
    public function fetch_stationrelplaninputoauth($limit = null, $start = null, $queryString = null, $id = null, $relation = null, $station_id  = null, $station_arr = array(), $deviceSn = null)
    {
        if (!empty($queryString)) {
            $start = 0;
        }

        $query = $this->db->select('station.* , plan_list.* , oauth_account_rel_planlist.auth_account_id , devices_rel_sn.deviceSn')
            ->from('plan_list')
            ->join('station', 'plan_list.station_id = station.stationSolarId')
            ->join('devices_rel_sn', 'devices_rel_sn.stationId = station.stationSolarId')
            ->join('devices', 'devices.deviceId = devices_rel_sn.deviceId')
            ->join('oauth_account_rel_planlist', 'plan_list.station_id = oauth_account_rel_planlist.station_id');

        $query = $query->where('devices.deviceType', "COLLECTOR");


        if (!empty($station_id)) {
            $query = $query->where('plan_list.station_id', $station_id);
        }

        if (!empty($id)) {
            $query = $query->where('plan_list.id', $id);
        }
        // case : ค้นหาทั้งชื่อไซต์อย่างเดียว
        if (!empty($queryString) && empty($deviceSn)) {
            $query = $query->like('plan_list.name', $queryString);
        }

        // case : ค้นหาทั้งชื่อไซต์และ sn
        if (!empty($deviceSn) && !empty($queryString)) {
            $query = $query->like('plan_list.name', $queryString);
            $query = $query->or_like('devices_rel_sn.deviceSn', $deviceSn);
        }
        if (!empty($station_arr)) {
            $query = $query->where_in('plan_list.station_id', $station_arr);
        }
        $query = $query->get();
        // echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }


            return $data;
        }

        return false;
    }

    public function fetch_planlist($limit, $start, $queryString = null, $id = null, $relation = null)
    {
        if (!empty($queryString)) {
            $start = 0;
        }
        if ($limit != 999 && $start != 999) {
            $this->db->limit($limit, $start);
        }
        //$query = $this->db->get("masterproduct_serialnumber");

        $query = $this->db->select('plan_list.*')
            ->from('plan_list');



        if (!empty($queryString)) {
            $query = $query->like('plan_list.name', $queryString);
        }
        if (!empty($id)) {
            $query = $query->where('plan_list.id', $id);
        }
        $query = $query->get();
        //echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function fetch_report_summaryyear_planlist($limit = null, $start = null, $queryString = null, $id = null, $relation = null, $station_id  = null, $station_arr = array())
    {
        if (!empty($queryString)) {
            $start = 0;
        }

        $query = $this->db->select('report_summaryyear_planlist.*  , plan_list.* , plan_list.generationPower as plan_generationPower')
            ->from('report_summaryyear_planlist')
            ->join('plan_list', 'plan_list.station_id = report_summaryyear_planlist.station_id');

        if (!empty($queryString)) {
            $query = $query->like('report_summaryyear_planlist.name', $queryString);
        }
        if (!empty($station_id)) {
            $query = $query->where('report_summaryyear_planlist.station_id', $station_id);
        }

        if (!empty($id)) {
            $query = $query->where('report_summaryyear_planlist.id', $id);
        }
        if (!empty($station_arr)) {
            $query = $query->where_in('report_summaryyear_planlist.station_id', $station_arr);
        }
        $query = $query->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function fetch_report_summaryday_planlist(
        $limit = null,
        $start = null,
        $queryString = null,
        $id = null,
        $relation = null,
        $station_id  = null,
        $station_arr = array(),
        $year = null,
        $month  = null,
        $day = null
    ) {
        if (!empty($queryString)) {
            $start = 0;
        }

        $query = $this->db->select('report_summaryday_planlist.*  , plan_list.* 
                , plan_list.generationPower as plan_generationPower
                , station.unit_priceSolarApp as unit_price')
            ->from('report_summaryday_planlist')
            ->join('plan_list', 'plan_list.station_id = report_summaryday_planlist.station_id')
            ->join('station', 'station.stationSolarId = report_summaryday_planlist.station_id')
            ->where("year", $year)
            ->where("month", $month)
            ->where("day", $day);

        if (!empty($queryString)) {
            $query = $query->like('report_summaryday_planlist.name', $queryString);
        }
        if (!empty($station_id)) {
            $query = $query->where('report_summaryday_planlist.station_id', $station_id);
        }

        if (!empty($id)) {
            $query = $query->where('report_summaryday_planlist.id', $id);
        }
        if (!empty($station_arr)) {
            $query = $query->where_in('report_summaryday_planlist.station_id', $station_arr);
        }
        $query = $query->get();


        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function fetch_planlistreloauth($limit = null, $start = null, $queryString = null, $id = null, $relation = null, $station_id  = null)
    {
        if (!empty($queryString)) {
            $start = 0;
        }

        $query = $this->db->select('plan_list.* , oauth_account_rel_planlist.auth_account_id')
            ->from('plan_list')
            ->join('oauth_account_rel_planlist', 'plan_list.station_id = oauth_account_rel_planlist.station_id');

        if (!empty($queryString)) {
            $query = $query->like('plan_list.name', $queryString);
        }
        if (!empty($station_id)) {
            $query = $query->where('plan_list.station_id', $station_id);
        }

        if (!empty($id)) {
            $query = $query->where('plan_list.id', $id);
        }
        $query = $query->get();
        //echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function onaction_set_removeduplicateplankey($arr_planlist, $param = null)
    {

        $strparameter = empty($param) ? "station_id" : $param;
        $temparr = array_unique(array_column($arr_planlist, "{$strparameter}"));
        $arr_planlist = array_intersect_key($arr_planlist, $temparr); # remove duplicate key

        return $arr_planlist;
    }


    public function fetch_devices($station_id = null, $not_empty_status = null, $id = null, $relation_with_stationtbl = null)
    {
        $query = $this->db->select('devices_rel_sn.* , plan_list.lastUpdateTime')
            ->from('devices')
            ->join('devices_rel_sn ', 'devices.deviceId = devices_rel_sn.deviceId')
            ->join('plan_list', 'devices_rel_sn.stationId = plan_list.station_id');

        if (!empty($relation_with_stationtbl)) {
            $query = $query->select('station.model_SolarApp as model');
            $query =  $query->join('station ', 'plan_list.station_id = station.stationSolarId');
        }
        if (!empty($not_empty_status)) {
            $query = $query->where('plan_list.lastUpdateTime is NOT NULL', NULL, FALSE);
            $query = $query->where('plan_list.lastUpdateTime !=', "");
        }


        if (!empty($id)) {
            $query = $query->where('devices_rel_sn.deviceId', $id);
        }

        $query = $query->where('deviceType',  'COLLECTOR');
        $query = $query->get();
        //  echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function count_devicesregister($station_id = null, $queryString = null, $id = null)
    {
        $query = $this->db->select('devices_rel_sn.*')
            ->from('devices')
            ->join('devices_rel_sn ', 'devices.deviceId = devices_rel_sn.deviceId')
            ->join('plan_list', 'devices_rel_sn.stationId = plan_list.station_id');

        if (!empty($queryString)) {
            // $query = $query->like('plan_list.name', $queryString);

        }
        if (!empty($station_id)) {
            //$query = $query->where('plan_list.station_id', $station_id);
        }

        if (!empty($id)) {
            $query = $query->where('devices_rel_sn.deviceId', $id);
        }

        $query = $query->where('plan_list.lastUpdateTime is NOT NULL', NULL, FALSE);
        $query = $query->where('plan_list.lastUpdateTime !=', "");

        $query = $query->where('deviceType',  'COLLECTOR');


        $query = $query->get();
        //echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }


    public function fetch_devicesmodel($station_id = null, $queryString = null, $id = null)
    {
        $query = $this->db->select('devices_rel_sn.*')
            ->from('devices')
            ->join('devices_rel_sn ', 'devices.deviceId = devices_rel_sn.deviceId')
            ->join('plan_list', 'devices_rel_sn.stationId = plan_list.station_id');

        if (!empty($queryString)) {
            // $query = $query->like('plan_list.name', $queryString);

        }
        if (!empty($station_id)) {
            //$query = $query->where('plan_list.station_id', $station_id);
        }

        if (!empty($id)) {
            $query = $query->where('devices_rel_sn.deviceId', $id);
        }

        $query = $query->where('deviceType',  'COLLECTOR');
        $query = $query->where('plan_list.lastUpdateTime is NOT NULL', NULL, FALSE);
        $query = $query->where('plan_list.lastUpdateTime !=', "");

        $query = $query->get();
        // echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }


    public function fetch_customerrelplan($station_id = null, $queryString = null, $customer_id = null)
    {
        $query = $this->db->select('customers_rel_station.* , plan_list.name_solarapp , plan_list.locationLat , plan_list.locationLng  , plan_list.createdDate , plan_list.intallation_date,station.model_SolarApp as model , station.allPowerGeneration_SolarApp as allPowerGeneration,station.model_power_solarApp as model_pw,station.electric_meter_type , (select top 1 unitprice from setting_electricmeter where station.electric_meter_type = setting_electricmeter.electric_meter_type) as unit_price ')
            ->from('customers_rel_station')
            ->join('plan_list', 'customers_rel_station.stationId = plan_list.station_id')
            ->join('station',   'customers_rel_station.stationId = station.stationSolarId');

        if (!empty($queryString)) {
            // $query = $query->like('plan_list.name', $queryString);

        }
        if (!empty($station_id)) {
            $query = $query->where('customers_rel_station.stationId', $station_id);
        }

        if (!empty($customer_id)) {
            $query = $query->where('customers_rel_station.customer_id', $customer_id);
        }


        $query = $query->get();
        // echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }



    public function oauth2_list_history($timeType, $startDate, $endDate, $stationId, $url, $access_token)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/json",
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        # json response time type 1 = show every hour on date , time type 2 = date range
        $data = '{"stationId":"' . $stationId . '","startTime":"' . $startDate . '","endTime":"' . $endDate . '","timeType":"' . $timeType . '"}';
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
       
        return $resp;
    }

    public function oauth2_calcurateplanlistpagesize($access_token = null)
    {

        $url = $this->oauth2_url("plan_list");
        # count all result for calcurate page size
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/json",
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"size":"1","page":1}';
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        # เก็บ log
        $this->onaction_updateorinsertsolarmanlog("list", $url);

        $gettotaldecode_result = json_decode($resp);
        if (!empty($gettotaldecode_result)) {
            $total = empty($gettotaldecode_result->total) ? 1 : intval($gettotaldecode_result->total);
            $total_planlistpage =  $total;
            $ctotal_planlistpage = $total_planlistpage / 200;

            $xtotal_planlistpage = ceil($ctotal_planlistpage);

            if ($xtotal_planlistpage < 1) {
                $xtotal_planlistpage = 1;
            }
            //echo $xtotal_planlistpage.'||';
            return intval($xtotal_planlistpage);
        } else {
            return 0;
        }
    }


    public function oauth2_plan_list($access_token)
    {

        $decode_result = "";
        if (isset($access_token)) {
            # case : calcurate page size
            $total_planlistpage =  $this->oauth2_calcurateplanlistpagesize($access_token);

            $kk = 1;
            while ($kk <= $total_planlistpage) {
                # insert new token 


                $url = $this->oauth2_url("plan_list");

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $headers = array(
                    "Accept: application/json",
                    "Authorization: Bearer {$access_token}",
                    "Content-Type: application/json",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

                $data = '{"size":"200","page":"' . $kk . '"}';

                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                //for debug only!
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $resp = curl_exec($curl);
                curl_close($curl);

                # เก็บ log
                $this->onaction_updateorinsertsolarmanlog("list", $url);

                $decode_tokenresult = json_decode($resp);
                if ($kk == 1) {
                    $decode_result = $decode_tokenresult;
                } else {
                    if (isset($decode_result->stationList)) {
                        foreach ($decode_tokenresult->stationList as $key => $value) {
                            array_push($decode_result->stationList, $value);
                        }
                    }
                }

                if ($kk == $total_planlistpage) {
                    return $decode_result;
                }

                ++$kk;
            }
        }

        return $decode_result;
    }


    # oauth2 list history data
    public function oauth2_realtime($stationId, $url, $access_token)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/json",
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        # json response time type 1 = show every hour on date , time type 2 = date range
        $data = '{"stationId":"' . $stationId . '"}';
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        # เก็บ log
        $this->onaction_updateorinsertsolarmanlog("realTime", $url);

        return $resp;
    }


    public function onaction_updateorinsertsolarmanlog($api_name = null, $api_url = null)
    {
        $query = $this->db->select("*")->from('solarman_log')
            ->where('api_name', $api_name)
            ->where("date", DATE("Y-m-d"))
            ->where("type", "backend")->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $api_coutingcallurl = $row->api_coutingcallurl;
            if ($api_coutingcallurl < 0) {
                $api_coutingcallurl = 1;
            } else {
                $api_coutingcallurl = intval($api_coutingcallurl) + 1;
            }
            $this->db->update('solarman_log', array(
                'api_name' => $api_name,
                'api_url' => $api_url,
                'api_coutingcallurl' => $api_coutingcallurl,
                "updated" => DATE("Y-m-d H:i:s")
            ), array("id" => $row->id));
        } else {
            $this->db->insert(
                'solarman_log',
                array(
                    "api_name" => $api_name,
                    "api_url" => $api_url,
                    "api_coutingcallurl" => 1,
                    "type" => "backend",
                    'date' => DATE("Y-m-d"),
                    "created" => DATE("Y-m-d H:i:s")
                )
            ); // insert device serialnumber
        }
    }

    public function get_thaimonthname($monthNum = null, $monthname_type = null)
	{

		$monthNum = intval($monthNum);
		switch ($monthNum) {

			case 1:
				$month = "มกราคม";
				if ($monthname_type == "short") {
					return "ม.ค.";
				}
				return $month;
				break;
			case 2:
				$month = "กุมภาพันธ์";
				if ($monthname_type == "short") {
					return "ก.พ.";
				}
				return $month;
				break;
			case 3:
				$month = "มีนาคม";
				if ($monthname_type == "short") {
					return "มี.ค.";
				}
				return $month;
				break;
			case 4:
				$month = "เมษายน";
				if ($monthname_type == "short") {
					return "เม.ย.";
				}
				return $month;
				break;
			case 5:
				$month = "พฤษภาคม";
				if ($monthname_type == "short") {
					return "พ.ค.";
				}
				return $month;
				break;
			case 6:
				$month = "มิถุนายน";
				if ($monthname_type == "short") {
					return "มิ.ย.";
				}
				return $month;
				break;
			case 7:
				$month = "กรกฎาคม";
				if ($monthname_type == "short") {
					return "ก.ค.";
				}
				return $month;
				break;
			case 8:
				$month = "สิงหาคม";
				if ($monthname_type == "short") {
					return "ส.ค.";
				}
				return $month;
				break;
			case 9:
				$month = "กันยายน";
				if ($monthname_type == "short") {
					return "ก.ย.";
				}
				return $month;
				break;
			case 10:
				$month = "ตุลาคม";
				if ($monthname_type == "short") {
					return "ต.ค.";
				}
				return $month;
				break;
			case 11:
				$month = "พฤศจิกายน";
				if ($monthname_type == "short") {
					return "พ.ย.";
				}
				return $month;
				break;
			case 12:
				$month = "ธันวาคม";
				if ($monthname_type == "short") {
					return "ธ.ค.";
				}
				return $month;
				break;
		}
	}

    public function fetch_electric_meter()
	{
		$query  = $this->db->select('*')->from('setting_electricmeter')->get();
		$result = $query->result_array();
		$data = array();
		foreach ($result as $key => $value) {
			$data[$value['electric_meter_type']] = $value['unitprice'];
		}
		return $data;
	}

    public function ajax_call_history()
	{
		$url = $this->oauth2_url("history"); # get url 


		$stationId = $_POST['stationId'];
		$access_token  = $_POST['access_token'];
		$unit_price  = empty($_POST['unit_price']) ? 0 : $_POST['unit_price'];

		# case : get history data today generation power 
		$timeType_day = 1;
		$startDate_day = DATE("Y-m-d");
		$endDate_day   = DATE("Y-m-d");
		# case : get history data month generation power
		$month_report = $this->list_everydate_of_month(DATE("Y"),  DATE("m"));
		$timeType_month = 2;
		$startDate_month = $month_report[key($month_report)];
		$endDate_month   = end($month_report);
		# case : get history data year generation power

		$timeType_year = 4;
		$startDate_year = 2020;
		$endDate_year   = DATE("Y");

		$data = array();
		/** begin : call api to get history of this day */
		$resp = $this->oauth2_list_history($timeType_day, $startDate_day, $endDate_day, $stationId, $url, $access_token);
		# case : convert json to array
		$resp_historyday = json_decode($resp);
		if (isset($resp_historyday->stationDataItems)) {
			$stationDataItems = $resp_historyday->stationDataItems;
			$ii = 0;

			$generationPower       = 0;
			$get_generationpower   = $this->get_generationpower($stationId, $access_token); # get : generation power on realtime 
			$generationPower       = $get_generationpower["generationPower"];

			$today_generationpower = 0;
			$today_generationpower = $this->calcurate_todaygenerationpw($ii, $stationDataItems, $today_generationpower); # get : today generation power

			// ผลตอบแทนวันนี้
			$today_revenue = $today_generationpower * $unit_price;
			$data['today_revenue']  =  number_format($today_revenue, 2, '.', ''); # get : today revenue

			// พลังงานที่ผลิตได้วันนี้
			$today_generationpower = number_format($today_generationpower, 2, '.', '');
			$data['today_generationpower'] = $today_generationpower;

			// generationPower
			$data["generationPower"] =  $generationPower;
			$data["lastUpdateTime"]  =  $get_generationpower["lastUpdateTime"];


			// ผลตอบแทนเดือนนี้
			$resp = $this->oauth2_list_history($timeType_month, $startDate_month, $endDate_month, $stationId, $url, $access_token);
			# case : convert json to array
			$resp_historymonth = json_decode($resp);
			if (isset($resp_historymonth->stationDataItems)) {
				$month_revenue = $this->calcurate_monthavenue($resp_historymonth, $unit_price);
				$data['month_revenue'] = number_format($month_revenue, 2, '.', '');
			} else {
				$data['month_revenue'] = 0;
			}

			// kgco
			$resp = $this->oauth2_list_history($timeType_year, $startDate_year, $endDate_year, $stationId, $url, $access_token);
			# case : convert json to array
			$resp_historyyear = json_decode($resp);
			if (isset($resp_historyyear->stationDataItems)) {
				$kgco = $this->calcurate_kgco($resp_historyyear, $unit_price);
				$data['kgco'] = number_format($kgco, 2, '.', '');
			} else {
				$data['kgco'] = 0;
			}
		} else {
			$data['today_generationpower'] = 0;
			$data['today_revenue']  = 0;
			$data["generationPower"] = 0;
			$data['month_revenue'] = 0;
		}

		echo json_encode(array("data" => $data, "status" => true));

		/** eof : call api to get history of this day */
	}
	public function get_generationpower($stationId, $access_token)
	{
		# case : get real time data
		$realtime_url = $this->oauth2_url("realTime"); # get url 
		$response_api_obj = $this->oauth2_realtime($stationId, $realtime_url, $access_token);
		$response = json_decode($response_api_obj);

		if (!empty($response)) {
			$generationPower = $response->generationPower;
			$lastUpdateTime  = $response->lastUpdateTime;
		}
		return array("generationPower" => $generationPower, "lastUpdateTime" => $lastUpdateTime);
	}
	public function calcurate_todaygenerationpw($ii, $stationDataItems, $today_generationpower)
	{
		while ($ii < count($stationDataItems)) {
			$today_generationpower += empty($stationDataItems[$ii]->generationPower) ? 0 : $stationDataItems[$ii]->generationPower;
			++$ii;
		}
		$today_generationpower = $today_generationpower > 0 ? $today_generationpower / 10000 : 0;
		return $today_generationpower;
	}
	public function calcurate_monthavenue($resp_historymonth, $unit_price)
	{
		$stationDataItems = $resp_historymonth->stationDataItems;
		$jj = 0;
		$month_generationpw = 0;
		while ($jj < count($stationDataItems)) {
			$month_generationpw += empty($stationDataItems[$jj]->generationValue) ? 0 : $stationDataItems[$jj]->generationValue;
			++$jj;
		}
		$month_revenue = $month_generationpw * $unit_price;
		return $month_revenue;
	}
	public function calcurate_kgco($resp_historyyear, $unit_price)
	{
		$stationDataItems = $resp_historyyear->stationDataItems;
		$jj = 0;
		$year_generationpw = 0;
		while ($jj < count($stationDataItems)) {
			$year_generationpw += empty($stationDataItems[$jj]->generationValue) ? 0 : $stationDataItems[$jj]->generationValue;
			++$jj;
		}
		$kgco = $year_generationpw * 0.5113;
		return $kgco;
	}
	public function get_stationdataitem($search_planlist, $startDate, $endDate, $period)
	{
		$returndata = array();
		$get_planlist = $this->fetch_planlistreloauth(null, null, null, null, null, $search_planlist);

		if (!empty($get_planlist)) {

			$auth_account_id = $get_planlist[key($get_planlist)]->auth_account_id;
			$stationId = $get_planlist[key($get_planlist)]->station_id;

			$get_accountdata = $this->get_customeraccesstoken($auth_account_id);
			if (!empty($get_accountdata)) {
				$access_token =  $get_accountdata->access_token; # get access token

				$url = $this->oauth2_url("history"); # get url 
				# json param : time type 
				$timeType = ($startDate == $endDate) ? 1 : 2;
				# case : ถ้าเป็นรายเดือนให้ default เป็น time type 2
				if ($period == "month") {
					$timeType = 2;
				}
				if ($period == "year") {
					$timeType = 3;
				}

				$returndata["timeType"] = $timeType;

				# case : get history data from solarman api 
				$resp = $this->oauth2_list_history($timeType, $startDate, $endDate, $stationId, $url, $access_token);
				# case : convert json to array
				$decode_result = json_decode($resp);

				$get_stationinput = $this->fetch_stationrelplaninput($stationId);
				if (empty($get_stationinput)) {
					$returndata["unit_price"] = 0;
				} else {
					$returndata["unit_price"] = isset($get_stationinput[key($get_stationinput)]->unit_price) ? $get_stationinput[key($get_stationinput)]->unit_price : 0;
				}

				if (!empty($decode_result)) {
					if (isset($decode_result->total)) {
						if ($decode_result->total > 0) {
							# case : set return station data
							$returndata["stationDataItems"]  = $this->onaction_set_returnstationitem($decode_result, $returndata["unit_price"], $timeType);

							return $returndata;
						} else {
							# not found data
							return $returndata;
						}
					}
				}
			}
		}

		return false;
	}




	public function list_everydate_of_month($y, $m, $lastdate_filter = null)
	{
		$month = $m;
		$year = $y;

		$start_date = "01-" . $month . "-" . $year;
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		for ($i = $start_time; $i < $end_time; $i += 86400) {
			if (empty($lastdate_filter)) {
				$list[] = date('Y-m-d', $i);
			} else {
				if (date('Y-m-d', $i) <= $lastdate_filter) {
					$list[] = date('Y-m-d', $i);
				}
			}
		}

		//return $list;
		return $list;
	}

    public function ajax_curlgetsolarreport($post_data = array())
	{
        if(!empty($post_data)){
            $_POST = $post_data;
        }
		$stationId = $_POST['stationId'];
		$period    = $_POST['period'];
		$startDate =  "";
		$endDate   =  "";

		if ($period == "month") {
			$duration = explode("/", $_POST['duration']);
			$month = $duration[0];
			$year = $duration[1];
			$datereport = $this->list_everydate_of_month($year,  $month);
			$startDate  = $datereport[key($datereport)];
			$endDate    = end($datereport);
		} else {
			$startDate = $_POST['duration'] . '-01';
			$endDate   = $_POST['duration'] . '-12';
		}
    
		$response = $this->get_stationdataitem($stationId, $startDate, $endDate, $period);

	

		if (isset($response['stationDataItems'])) {
			$sort_array = array();
			if ($period == "month") {
				$response['stationDataItems'] = $this->append_null_array($period, $response, $datereport, $startDate, $endDate);
			} else {
				$response['stationDataItems'] = $this->append_null_array($period, $response, array(), $startDate, $endDate);
			}


			foreach ($response['stationDataItems'] as $key => $row) {
				if ($period == "year") {
					$sort_array[$key] = $row->month;
				} else {
					$sort_array[$key] = $row->day;
				}
			}
			array_multisort($sort_array, SORT_ASC, $response['stationDataItems']);

			// echo '<PRE>';print_r($response['stationDataItems']);exit();
			if (!empty($response['stationDataItems'])) {
                if(!empty($post_data)){// case : send by compare data
                    return $response;
                }
                else{
                    echo json_encode(array("status" => true, "data" => json_encode($response)));
                }
				
			} else {
                if(!empty($post_data)){// case : send by compare data
                    return false;
                }
                else{
                    echo json_encode(array("status" => false));
                }

			
			}
		} else {
            if(!empty($post_data)){// case : send by compare data
                return false;
            }
            else{
                echo json_encode(array("status" => false));
            }
			
		}
	}

	public function append_null_array($period, $response, $datereport, $startDate, $endDate)
	{
		if ($period == "month") {

			if(!isset($response['stationDataItems'])){
				$stationDataItems = array();
			}else{
				$stationDataItems = (array)$response['stationDataItems'];
			}
			foreach ($datereport as $datekey => $datevalue) {
				$month = DATE("m", strtotime($datevalue));
				$year  = DATE("Y", strtotime($datevalue));
				$day = DATE("d", strtotime($datevalue));
				$day = intval($day);
				$find_key =  array_search($day, array_column($stationDataItems, 'day'));
				
				if ($datevalue <= DATE("Y-m-d")) {
					if (is_numeric($find_key)) {
						# case : found
					} else {

						$data_push = array(
							"generationPower" => 0,
							"generationValue" => 0,
							"d" => intval($day),
							"month" => intval($month),
							"year" => intval($year),
							"day" => intval($day),
							"eco_price" => 0,
							"timeType" => 2,
						);
						array_push($response['stationDataItems'], (object)$data_push);
					}
				}
			}
		} else {
			if(!isset($response['stationDataItems'])){
				
				$stationDataItems = array();
			}else{
				$stationDataItems = (array)$response['stationDataItems'];
			}

			for ($i = 1; $i <= 12; $i++) {


				$year = DATE("Y", strtotime($startDate));

				$find_key =  array_search($i, array_column($stationDataItems, 'month'));
				if (is_numeric($find_key)) {
					# case : found
				} else {

					$data_push = array(
						"generationPower" => 0,
						"generationValue" => 0,
						"day" => "",
						"year" => $year,
						"month" => $i,

						"eco_price" => 0,
						"timeType" => 3,
					);
					array_push($response['stationDataItems'], (object)$data_push);
				}
			}
		}
	
		return $response['stationDataItems'];
	}

}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
